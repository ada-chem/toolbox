import os
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from ada_toolbox.slack_api.slack_api import Slack
import logging
from ada_toolbox.data_analysis_lib.validation_thresholds import blur_threshold, brightness_threshold, \
    r2_score_threshold, number_valid_observations_lm_threshold, lm_slope_threshold
from ada_toolbox.data_analysis_lib import spectroscopy_analysis
from ada_toolbox.data_analysis_lib import validation_thresholds
import cv2
from typing import Union

# import ada_toolbox.data_analysis_lib.spectroscopy_analysis as spect
import ada_toolbox.data_analysis_lib.image_analysis as img
import ada_toolbox.data_analysis_lib.conductivity_analysis as cond

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Validation(Slack):
    """
    The Validation class tests the raw data analysis scripts against a predetermined threshold.
    The validation class imports the the slack api super class to communicate with ada-chem channels using the
    validator bot.
    """

    def __init__(self, channel: Union[str, None] = 'test', **kwargs):

        # Use token key word argument to use a different bot if desired.
        super(Validation, self).__init__(**kwargs)

        # The desired channel to post to
        self.channel = channel

    def save_and_post_figure(self, figure, data_type: str, campaign, sample, position):

        if figure is not None:
            image_directory = 'tmp'
            image_name = f'{campaign}_{sample}_{position}_{data_type}.png'
            image_path = os.path.join(os.path.basename(image_directory), image_name)
            if not os.path.isdir(os.path.basename(image_directory)):
                os.mkdir(os.path.basename(image_directory))
            plt.savefig(image_path)
            self.post_file(channel=self.channel,
                           title=f"Campaign: {campaign}, "
                           f"Sample: {sample}, "
                           f"Image: {image_name}",
                           file=image_path)

    def spectra_data_raw(self,
                         spectroscopy_df: pd.DataFrame,
                         bound_lo_dark=validation_thresholds.bound_lo_dark,
                         bound_up_dark=validation_thresholds.bound_up_dark,
                         tol_lo_dark=validation_thresholds.tol_lo_dark,
                         tol_up_dark=validation_thresholds.tol_up_dark,
                         # bright baseline
                         bound_lo_bright=validation_thresholds.bound_lo_bright,
                         bound_up_bright=validation_thresholds.bound_up_bright,
                         tol_lo_bright=validation_thresholds.tol_lo_bright,
                         tol_up_bright=validation_thresholds.tol_up_bright,
                         # sample signal
                         tol_sample=validation_thresholds.tolerance,
                         campaign: str = None,
                         sample: str = None,
                         position: str = None,
                         ):
        # validate dark baseline
        pass_baseline_dark, figure_dark = spectroscopy_analysis.validate_dark_baseline(df=spectroscopy_df,
                                                                          bound_lo=bound_lo_dark,
                                                                          bound_up=bound_up_dark,
                                                                          tol_lo=tol_lo_dark,
                                                                          tol_up=tol_up_dark,
                                                                          campaign=campaign,
                                                                          sample=sample,
                                                                          position=position,
                                                                          logger=logger,
                                                                          validartor_slack=self)
        self.save_and_post_figure(figure_dark, 'DARK_BL', campaign, sample, position)

        # validate bright baseline
        pass_baseline_bright, figure_bright = spectroscopy_analysis.validate_bright_baseline(df=spectroscopy_df,
                                                                              bound_lo=bound_lo_bright,
                                                                              bound_up=bound_up_bright,
                                                                              tol_lo=tol_lo_bright,
                                                                              tol_up=tol_up_bright,
                                                                              campaign=campaign,
                                                                              sample=sample,
                                                                              position=position,
                                                                              logger=logger,
                                                                              validartor_slack=self)
        self.save_and_post_figure(figure_bright, 'BRIGHT_BL', campaign, sample, position)

        pass_signal, figure_signal = spectroscopy_analysis.validate_sample_signal(df=spectroscopy_df,
                                                                           tolerance=tol_sample,
                                                                           campaign=campaign,
                                                                           sample=sample,
                                                                           position=position,
                                                                           logger=logger,
                                                                           validartor_slack=self)

        self.save_and_post_figure(figure_signal, 'SIGNAL', campaign, sample, position)

    def calibrated(self,
                   spectroscopy_df: pd.DataFrame,
                   bound_lo=validation_thresholds.bound_lo_calibrated,
                   bound_up=validation_thresholds.bound_up_calibrated,
                   tol_lo=validation_thresholds.tolerance_cal_t_r_lo,
                   tol_up=validation_thresholds.tolerance_cal_t_r_up,
                   campaign: str = None,
                   sample: str = None,
                   position: str = None,
                   data_type: str = None,
                   ignore=validation_thresholds.ignore_range_t_r,
                   ):

        passed, figure = spectroscopy_analysis.validate_calibrated(df=spectroscopy_df,
                                                           what_signal=data_type,
                                                           bound_lower=bound_lo,
                                                           bound_upper=bound_up,
                                                           tolerance_up=tol_up,
                                                           tolerance_lo=tol_lo,
                                                           logger=logger,
                                                           validartor_slack=self,
                                                           campaign=campaign,
                                                           sample=sample,
                                                           position=position,
                                                           ignore=ignore)
        # self.save_and_post_figure(figure, data_type, campaign, sample, position)
        return passed

    def absorbance(self,
                   spectroscopy_df: pd.DataFrame,
                   bound_lo=validation_thresholds.bound_absorp_lo,
                   bound_up=validation_thresholds.bound_absorp_up,
                   tol_lo=validation_thresholds.tolerance_cal_t_r_lo,
                   tol_up=0,
                   campaign: str = None,
                   sample: str = None,
                   position: str = None,
                   data_type: str = None,
                   ignore=validation_thresholds.ignore_range_absorp,
                   data_key="absorbance"
                   ):

        passed, figure = spectroscopy_analysis.validate_calibrated(df=spectroscopy_df,
                                                           what_signal=data_type,
                                                           bound_lower=bound_lo,
                                                           bound_upper=bound_up,
                                                           tolerance_up=tol_up,
                                                           tolerance_lo=tol_lo,
                                                           logger=logger,
                                                           validartor_slack=self,
                                                           campaign=campaign,
                                                           sample=sample,
                                                           position=position,
                                                           ignore=ignore,
                                                           raw_data_key=data_key)
        # self.save_and_post_figure(figure, data_type, campaign, sample, position)
        return passed

    def conductivity_data(self,
                          conductivity_file: str = None,
                          conductivity_df: pd.DataFrame = None,
                          current_col: str = "Current",
                          voltage_col: str = "Voltage",
                          r2_score_threshold: float = r2_score_threshold,
                          number_valid_observations_lm_threshold: int = number_valid_observations_lm_threshold,
                          **kwargs):
        """
        Validate the conductivity data analysis against the provided threshold for a given file.
        :param conductivity_file: File directory of raw conductivity csv
        :param conductivity_df: conductivity dataframe
        :return: Dictionary if values are valid
        """
        if conductivity_df is None:

            try:
                # Read in the raw data csv
                df = pd.read_csv(conductivity_file)

            except Exception as e:
                logger.error(e)
                return None

        else:
            df = conductivity_df

        # Clean the raw data
        df_clean = cond.remove_saturated_points(df)

        # Number of valid measurements present
        num_valid = cond.valid_measurements(data=df_clean)

        metadata = ""
        length_kwargs = len(kwargs)
        i = 1
        for key, value in kwargs.items():
            if i != length_kwargs:
                metadata = metadata + f"{key.upper()}: {value}, "
                i += 1
            else:
                metadata = metadata + f"{key.upper()}: {value} -- "
                i += 1

        # Linear model needs atleast 2 observations to draw a line
        if num_valid >= number_valid_observations_lm_threshold:

            r2, slope, intercept = cond.linear_regression(data=df_clean,
                                                          x_axis=current_col,
                                                          y_axis=voltage_col)

            if r2 > r2_score_threshold:
                logger.info(f"{metadata}"
                            "Conductance R2 measurement is valid.")

                if slope > lm_slope_threshold:
                    logger.info(f"{metadata}"
                                "Conductance slope measurement is valid.")

                    return {"valid_points": True, "r2": True, "slope": True}

                else:
                    logger.warning(f"{metadata}"
                                   f"Slope of {slope} < {lm_slope_threshold} threshold,"
                                   f"calculated from {num_valid} valid points.")

                    if self.channel is not None:
                        self.post(channel=self.channel,
                                  text=f"{metadata}"
                                  f"Slope of {slope} < {lm_slope_threshold} threshold,"
                                  f"calculated from {num_valid} valid points.")

                    return {"valid_points": True, "r2": True, "slope": False}

            else:
                logger.warning(f"{metadata}"
                               f"R-squared score of {r2} < {r2_score_threshold} "
                               f"calculated from {num_valid} valid points.")

                if self.channel is not None:
                    self.post(channel=self.channel,
                              text=f"{metadata}"
                              f"R-squared score of {r2} < {r2_score_threshold} threshold,"
                              f"calculated from {num_valid} valid points.")

                return {"valid_points": True, "r2": False, "slope": False}

        else:
            logger.warning(f"{metadata}"
                           f"Not enough valid measurements to calculate conductance. There are {num_valid} "
                           f"valid observations, {number_valid_observations_lm_threshold} are needed.")

            if self.channel is not None:
                self.post(channel=self.channel,
                          text=f"{metadata}"
                          f"Not enough valid measurements to calculate conductance. There are {num_valid} "
                          f"valid observations, {number_valid_observations_lm_threshold} are needed.")

            return {"valid_points": False, "r2": False, "slope": False}

    def photograph(self,
                   image_directory: str = None,
                   image: np.ndarray = None,
                   brightness_threshold: float = brightness_threshold,
                   blur_threshold: float = blur_threshold,
                   **kwargs):
        """
        Validate the image data analysis against the provided thresholds for a given image.
        :param image_directory: File path of an image to test
        :param image: nd array of an image in memory
        :param brightness_threshold: Brightness threshold to exceed. Ranges from 0 to 1.
        :param blur_threshold: Blurriness threshold to be under. Ranges from 0 to 1.
        :return: Dictionary if values are valid
        """

        if image is None:
            try:
                brightness = img.brightness_detection(image_directory=image_directory)
                blur = img.blur_detection(image_directory=image_directory)

            except Exception as e:
                logger.error(e)
                return None

        else:
            try:
                brightness = img.brightness_detection(image=image)
                blur = img.blur_detection(image=image)

            except Exception as e:
                logger.error(e)
                return None

        # Set conditions
        if blur < blur_threshold:
            blur_condition = True
        else:
            blur_condition = False

        if brightness > brightness_threshold:
            brightness_condition = True
        else:
            brightness_condition = False

        metadata = ""
        length_kwargs = len(kwargs)
        i = 1
        for key, value in kwargs.items():
            if i != length_kwargs:
                metadata = metadata + f"{key.upper()}: {value}, "
                i += 1
            else:
                metadata = metadata + f"{key.upper()}: {value} -- "
                i += 1

        if brightness > brightness_threshold:

            logger.info(f"Image is bright. Brightness of {brightness} > threshold of {brightness_threshold}")

        else:

            if image is None:

                logger.info(f"{metadata}"
                            f"Image: '{os.path.basename(image_directory)}' -- "
                            f"Brightness of {brightness} < {brightness_threshold} threshold.")

                if self.channel is not None:
                    self.post(channel=self.channel,
                              text=f"{metadata}"
                              f"Image: '{os.path.basename(image_directory)}' -- "
                              f"Brightness of {brightness} < {brightness_threshold} threshold.")

                    self.post_file(channel=self.channel,
                                   title=f"{metadata}"
                                   f"Image: '{os.path.basename(image_directory)}'",
                                   file=image_directory)

                return {"brightness": brightness_condition, "blurriness": blur_condition}

            else:
                logger.info(f"{metadata}"
                            f"Brightness of {brightness} < {brightness_threshold} threshold.")

                if self.channel is not None:
                    self.post(channel=self.channel,
                              text=f"{metadata}"
                              f"Brightness of {brightness} < {brightness_threshold} threshold.")

                return {"brightness": brightness_condition, "blurriness": blur_condition}

        if blur < blur_threshold:

            logger.info(f"Image is not blurry. Blur of {blur} < threshold of {blur_threshold}")

            return {"brightness": brightness_condition, "blurriness": blur_condition}

        else:

            if image is None:

                logger.info(f"{metadata}"
                            f"Image: '{os.path.basename(image_directory)}' -- "
                            f"Blur of {blur} > {blur_threshold} threshold.")

                if self.channel is not None:
                    self.post(channel=self.channel,
                              text=f"{metadata} "
                              f"Image: '{os.path.basename(image_directory)}' -- "
                              f"Blur of {blur} > {blur_threshold} threshold.")

                    self.post_file(channel=self.channel,
                                   title=f"{metadata} "
                                   f"Image: '{os.path.basename(image_directory)}'",
                                   file=image_directory)

                return {"brightness": brightness_condition, "blurriness": blur_condition}

            else:

                logger.info(f"{metadata}"
                            f"Blur of {blur} > {blur_threshold} threshold.")

                if self.channel is not None:
                    self.post(channel=self.channel,
                              text=f"{metadata}"
                              f"Blur of {blur} > {blur_threshold} threshold.")

                return {"brightness": brightness_condition, "blurriness": blur_condition}


if __name__ == "__main__":
    # Slack(bot="Validator").post(channel='test', text="hello")
    # Validation(bot='Validator').post(channel='test', text="validator class")

    # blurry = cv2.imread('/Users/teddyhaley/PycharmProjects/toolbox/sample_data/image_data/blurry.jpg')
    # dark = cv2.imread('/Users/teddyhaley/PycharmProjects/toolbox/sample_data/image_data/dark.jpg')
    #
    # Validation(channel='test', bot="Validator").photograph(
    #     image_directory='/Users/teddyhaley/PycharmProjects/toolbox/sample_data/image_data/blurry.jpg',
    #     campaign="campaign_1",
    #     sample="sample_1",
    #     step='step_1')
    #
    # Validation(channel='test', bot="Validator").photograph(
    #     image_directory='/Users/teddyhaley/PycharmProjects/toolbox/sample_data/image_data/dark.jpg',
    #     campaign="campaign_1",
    #     sample="sample_1",
    #     step='step_1')
    #
    # Validation(channel='test', bot="Validator").photograph(image=dark,
    #                                                        campaign="campaign_1",
    #                                                        sample="sample_1",
    #                                                        step='step_1')
    #
    # Validation(channel='test', bot="Validator").photograph(image=blurry,
    #                                                        campaign="campaign_1",
    #                                                        sample="sample_1",
    #                                                        step='step_1')

    Validation(channel='test', bot="Validator").conductivity_data(
        conductivity_file='/Users/teddyhaley/PycharmProjects/toolbox/sample_data/conductivity_data/sample_000_pos_000_conductivity.csv',
        campaign="campaign_1",
        sample="sample_1",
        step='step_1'
    )

    # Validation(channel=None, bot="Validator").photograph(
    #     image_directory='/Users/teddyhaley/PycharmProjects/toolbox/sample_data/image_data/blurry.jpg',
    #     campaign="campaign_1",
    #     sample="sample_1",
    #     step='step_1')
    #
    # Validation(channel=None, bot="Validator").photograph(
    #     image_directory='/Users/teddyhaley/PycharmProjects/toolbox/sample_data/image_data/dark.jpg',
    #     campaign="campaign_1",
    #     sample="sample_1",
    #     step='step_1')
    #
    # Validation(channel=None, bot="Validator").photograph(image=dark,
    #                                                      campaign="campaign_1",
    #                                                      sample="sample_1",
    #                                                      step='step_1')
    #
    # Validation(channel=None, bot="Validator").photograph(image=blurry,
    #                                                      campaign="campaign_1",
    #                                                      sample="sample_1",
    #                                                      step='step_1')

    Validation(channel=None, bot="Validator").conductivity_data(
        conductivity_file='/Users/teddyhaley/PycharmProjects/toolbox/sample_data/conductivity_data/sample_000_pos_000_conductivity.csv',
        campaign="campaign_1",
        sample="sample_1",
        step='step_1'
    )

    # spectroscopy validation
    # file_good = 'sample_data/spectroscopy_data/_uvvis-blank_t-data_good.csv'
    # file_bad = 'sample_data/spectroscopy_data/_uvvis-blank_t-data_bad.csv'
    #
    # spectra_good = pd.read_csv(file_good)
    # spectra_bad = pd.read_csv(file_bad)
    #
    # Validation(channel='test', bot="Validator").spectra_data(spectroscopy_df=spectra_good,
    #                                                          bound_lo_dark=validation_thresholds.bound_lo_dark,
    #                                                          bound_up_dark=validation_thresholds.bound_up_dark,
    #                                                          tol_lo_dark=validation_thresholds.tol_lo_dark,
    #                                                          tol_up_dark=validation_thresholds.tol_up_dark,
    #                                                          bound_lo_bright=validation_thresholds.bound_lo_bright,
    #                                                          bound_up_bright=validation_thresholds.bound_up_bright,
    #                                                          tol_lo_bright=validation_thresholds.tol_lo_bright,
    #                                                          tol_up_bright=validation_thresholds.tol_up_bright,
    #                                                          tol_sample=validation_thresholds.tolerance)
    #
    # Validation(channel='test', bot="Validator").spectra_data(spectroscopy_df=spectra_bad,
    #                                                          bound_lo_dark=validation_thresholds.bound_lo_dark,
    #                                                          bound_up_dark=validation_thresholds.bound_up_dark,
    #                                                          tol_lo_dark=validation_thresholds.tol_lo_dark,
    #                                                          tol_up_dark=validation_thresholds.tol_up_dark,
    #                                                          bound_lo_bright=validation_thresholds.bound_lo_bright,
    #                                                          bound_up_bright=validation_thresholds.bound_up_bright,
    #                                                          tol_lo_bright=validation_thresholds.tol_lo_bright,
    #                                                          tol_up_bright=validation_thresholds.tol_up_bright,
    #                                                          tol_sample=validation_thresholds.tolerance)

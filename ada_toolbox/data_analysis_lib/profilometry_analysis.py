import pandas as pd
import csv
import plotly.graph_objects as go
from pathlib import Path
import numpy as np
import logging
import struct
import math
from glob import glob

from scipy import optimize, signal
from collections import OrderedDict
from sklearn import linear_model
from sklearn.metrics import r2_score
from sklearn.cluster import KMeans

import warnings

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

pd.options.display.max_rows = 999
pd.options.display.max_columns = 999


def convert_profile_binary_to_csv(filename, total_points=0):
    """
    opens up a dektak binary file and extracts the profile data
    :param filename: the name of the file
    :param total_points: total number of points desired - not currently used
    :return: profile dictionary containing lateral, profile and neg_profile
    """
    y_scale_factor = 1.32  # determined experimentally - but it is contained somewhere in the datafile

    with open(filename, 'rb') as binary_file:
        data = binary_file.read()

    bytedata = bytearray(data)  # convert data to a byte array
    numpoint_pos = bytedata.find(b'NumPoints') + 10  # offset of number points in file
    numpoints = struct.unpack('<H', data[numpoint_pos:numpoint_pos + 2])[0] - 1  # drop last point to avoid some error - little-endian, unsigned short

    # reduce number of points stored to match total_points if possible
    if total_points > numpoints or total_points == 0:
        total_points = numpoints
    total_points = numpoints  # load the whole file regardless of requested points - this behavior overrides the preceding code. currently, downsampling is not supported

    ith = math.ceil(numpoints / total_points)  # used when downsampling is supported

    # initialize x and y data
    x_data = np.zeros(shape=(total_points,), dtype=float)
    y_data = np.zeros(shape=(total_points,), dtype=float)

    # locate X data
    start_x = bytedata.find(b'Micro') + 45  # landmark for x data
    for x in range(0, numpoints, ith):
        read_bytes = data[start_x + (x * 8):start_x + (x * 8) + 8]
        myfloat = struct.unpack('<d', read_bytes)[0]  # little-endian, double
        x_data[math.floor(x / ith)] = myfloat

    # locate Y data
    start_y = bytedata.find(b'ValueTypeName') + 186  # landmark for y data

    adjust_offset = False  # some files need this offset adjusted by two - reason not certain
    for i in range(0, 4):
        testbytes = data[start_y + (i * 8):start_y + (i * 8) + 8]
        testfloat = struct.unpack('<d', testbytes)[0]  # little-endian, double
        if abs(testfloat) > 10000000 or abs(testfloat) < 0.00000000000001:  # read test data without offset, if it is a garbage value, we need to adjust the position
            adjust_offset = True

    if adjust_offset:
        start_y = start_y + 2

    for y in range(0, numpoints, ith):
        read_bytes = data[start_y + (y * 8):start_y + (y * 8) + 8]
        myfloat = struct.unpack('<d', read_bytes)[0]  # little-endian, double
        y_data[math.floor(y / ith)] = myfloat * y_scale_factor  # scale factor accounts for the instrument's calibration

    return pd.DataFrame({'distance': x_data, 'height_raw': y_data})  # return a Pandas dataframe


def crop_data(df, start_x, end_x):
    cropped_df = df[df.distance >= start_x]
    cropped_df = cropped_df[cropped_df['distance'] <= end_x]
    cropped_df = cropped_df.reset_index(drop=True)
    return cropped_df


def fit_profile_data(x_data, y_data, func):
    """
    fits a segment of profile data to a provided function
    fit is line equation, merror is error in slope, berror is error in intercept - this is only meaningful for the linear fit - cubic error not implemented
    :return: the line fit
    """
    fits = {'fit': None, 'merror': None, 'berror': None}

    popt, pcov = optimize.curve_fit(func, x_data, y_data)
    perr = np.sqrt(np.diag(pcov))
    fits['fit'] = popt
    fits['merror'] = perr[0]
    fits['berror'] = perr[1]

    return fits


def lin(x, m, b):
    return (m * x) + b


def cubic(x, a, b, c, d):
    return (a * x**3) + (b * x**2) + (c * x**1) + d


def get_ranges(mapping_data, value):
    """
    returns a list of ranges of a particular value
    :param mapping_data: 1D profile to analyze
    :param value: the value to search for
    :return: list of tuples (start, end)
    """

    in_range = False
    counter = 0
    start = None
    end = None
    ret_list = []
    while counter < len(mapping_data):
        if in_range:
            if mapping_data[counter] != value:
                end = counter - 1
                ret_list.append((start, end))
                in_range = False
        else:
            if mapping_data[counter] == value:
                start = counter
                in_range = True
        counter += 1
    if in_range:
        end = counter - 1
        ret_list.append((start, end))

    return ret_list


def consolidate_ranges(data_list, range_list):
    """
    given a data set and a list of ranges, return a dataset only consisting of the provided ranges
    :param data_list:
    :param range_list:
    :return:
    """
    ret_list = []
    for ranged in range_list:
        for data_count in range(ranged[0], ranged[1]):
            ret_list.append(data_list[data_count])
    return np.array(ret_list)


def analyze_region(xdata, ydata, region, film_type):
    return OrderedDict({
        'type': film_type,
        'start': region[0],
        'end': region[1],
        'width (um)': xdata[region[1]] - xdata[region[0]],
        'mean_height (nm)': np.mean(ydata[region[0]:region[1]]),
        'median_height (nm)': np.median(ydata[region[0]:region[1]]),
        'roughness (nm': np.std(ydata[region[0]:region[1]]),
        'max_height (nm)': np.max(ydata[region[0]:region[1]]),
        'min_height (nm)': np.min(ydata[region[0]:region[1]])
    })


def analyze_rolling_regions(profile, window_width=100):
    rolling = profile['height_level'].rolling(window_width, center=True)
    profile = pd.concat((profile, rolling.mean().rename('rolling_mean')), axis=1)
    profile = pd.concat((profile, rolling.median().rename('rolling_median')), axis=1)
    profile = pd.concat((profile, rolling.std().rename('rolling_roughness')), axis=1)
    profile = pd.concat((profile, rolling.max().rename('rolling_max')), axis=1)
    profile = pd.concat((profile, rolling.min().rename('rolling_min')), axis=1)
    return profile


def level_profile(profile, filename, min_height=100, gradient_threshold=5, min_feature_width=20, plot=False):
    """
    levels the raw profile data by calculating best fit lines
    :param min_feature_width: (microns) features narrower than this are ignored
    :param gradient_threshold: gradients below this are considered bare glass
    :param min_height: flat sections below this height are treated as glass (nm)
    :param profile: the profile dictionary to level
    :return: the leveled profile
    """

    # create a gradient of the profile (x values are fixed, so we can get away with working with the height array only
    grad = np.gradient(profile['height_raw'])
    grad = np.abs(grad)

    # 0 = glass, 1 = film - catagorized based around a gradient level
    film_map = np.copy(grad)
    for count, point in enumerate(film_map):
        if point <= gradient_threshold:
            film_map[count] = 0
        else:
            film_map[count] = 1

    glass_positions = get_ranges(film_map, 0)

    glass_x_data = consolidate_ranges(profile['distance'], glass_positions)
    glass_y_data = consolidate_ranges(profile['height_raw'], glass_positions)

    fit = fit_profile_data(glass_x_data, glass_y_data, cubic)  # we only want to fit a curve to the bare glass

    leveled = profile['height_raw'] - ((fit['fit'][0] * profile['distance'] ** 3) + (fit['fit'][1] * profile['distance'] ** 2) + (fit['fit'][2] * profile['distance'] ** 1) + (fit['fit'][3]))

    # reevaluated film map to account for raised flat areas of film
    change_counter = 0
    for count, point in enumerate(film_map):
        if film_map[count] == 0 and leveled[count] >= min_height:
            film_map[count] = 1
            change_counter += 1

    if change_counter > 20:
        glass_positions = get_ranges(film_map, 0)
        glass_x_data = consolidate_ranges(profile['distance'], glass_positions)
        glass_y_data = consolidate_ranges(profile['height_raw'], glass_positions)

        fit = fit_profile_data(glass_x_data, glass_y_data, cubic)  # we only want to fit a curve to the bare glass

        leveled = profile['height_raw'] - ((fit['fit'][0] * profile['distance'] ** 3) + (fit['fit'][1] * profile['distance'] ** 2) + (fit['fit'][2] * profile['distance'] ** 1) + (fit['fit'][3]))

    # apply a custom filter to the film map. every feature with a width of less than 20 microns is grouped with it's neighbors, essentially eliminating features less than 20 microns in width
    filtered_film_map = np.copy(film_map)
    start_map = 0
    end_map = len(filtered_film_map)-1
    start_feature = start_map
    starting_value = filtered_film_map[start_feature]
    current_point = start_feature
    while True:
        while True:  # find the end of the current feature
            current_value = film_map[current_point]
            if current_value != starting_value or current_point >= end_map:
                end_feature = current_point - 1
                break
            else:
                current_point += 1
        width = end_feature - start_feature
        if current_point >= end_map:
            break
        if width < min_feature_width and starting_value == 1:  # erase the feature if it is a film
            for point in range(start_feature, end_feature+1):
                filtered_film_map[point] = current_value
            starting_value = current_value
        else:  # start scanning the next feature
            start_feature = current_point
            starting_value = filtered_film_map[start_feature]
    film_positions = get_ranges(filtered_film_map, 1)

    profile = pd.concat((profile, leveled.rename('height_level')), axis=1)
    profile = pd.concat((profile, pd.DataFrame(grad, columns=['gradient'])), axis=1)
    profile = pd.concat((profile, pd.DataFrame(film_map, columns=['film_map'])), axis=1)
    profile = pd.concat((profile, pd.DataFrame(filtered_film_map, columns=['filtered_film_map'])), axis=1)

    profile = analyze_rolling_regions(profile, window_width=100)

    rough_profile = profile[profile.rolling_roughness > 200]
    rough_profile = rough_profile[(rough_profile.rolling_roughness.shift(1) < rough_profile.rolling_roughness) & (rough_profile.rolling_roughness.shift(-1) < rough_profile.rolling_roughness)]
    rough_profile = rough_profile.reset_index(drop=True)

    smooth_profile = profile[profile.rolling_roughness <= 80]
    smooth_profile = smooth_profile.reset_index(drop=True)

    thickness = []
    heights_df = pd.DataFrame(columns=['distance', 'height_raw', 'height_level', 'gradient', 'film_map', 'filtered_film_map', 'rolling_mean', 'rolling_median', 'rolling_roughness', 'rolling_max', 'rolling_min'])

    for peak in rough_profile.distance:
        zone_before = (smooth_profile[(smooth_profile.distance < peak) & (smooth_profile.distance > peak - 50)])
        zone_after = (smooth_profile[(smooth_profile.distance > peak) & (smooth_profile.distance < peak + 50)])
        if len(zone_before) > 0 and len(zone_after) > 0:
            heights_df = pd.concat([heights_df, zone_before, zone_after])
            thickness.append(zone_before.height_level.mean() - zone_after.height_level.mean())
            # plot_stuff(pd.concat([zone_before, zone_after]))
    print(thickness)
    csvname = Path(filename).with_suffix('.csv')
    basename = csvname.stem
    csvname = csvname.with_name(f'{basename}_processed.csv')
    datacsvname = csvname.with_name(f'{basename}_analytics.csv')
    profile.to_csv(csvname, index=False)

    regions = []
    for region in film_positions:
        region_data = analyze_region(profile['distance'], profile['height_level'], region, film_type='film')
        regions.append(region_data)

    film_x_data = consolidate_ranges(profile['distance'], film_positions)
    film_y_data = consolidate_ranges(profile['height_level'], film_positions)

    # overall_data = analyze_region(film_x_data, film_y_data, (0, len(film_x_data)-1), film_type='overall')

    # overall_data = analyze_region(profile['distance'], profile['height_level'], region=(0, -1), film_type='overall')
    # regions.append(overall_data)

    # keys = regions[0].keys()
    # with open(datacsvname, 'w', newline='') as output_file:
    #     dict_writer = csv.DictWriter(output_file, keys)
    #     dict_writer.writeheader()
    #     dict_writer.writerows(regions)

    if plot:
        plot_stuff(profile)
        plot_stuff(rough_profile)
        plot_stuff(smooth_profile)
        # plot_stuff(heights_df)
    return profile


def plot_stuff(df):

    x = df['distance']
    y = df['height_raw']
    y1 = df['height_level']
    y2 = df['gradient']
    y3 = df['film_map'] * 1000
    y4 = df['filtered_film_map'] * 1000
    y5 = df['rolling_mean']
    y6 = df['rolling_median']
    y7 = df['rolling_roughness']
    y8 = df['rolling_max']
    y9 = df['rolling_min']

    fig = go.Figure()
    # fig.add_trace(go.Scatter(x=x, y=y, mode='markers', name='raw_data', marker_color='maroon'))
    fig.add_trace(go.Scatter(x=x, y=y1, mode='markers', name='leveled_data', marker_color='purple'))
    # fig.add_trace(go.Scatter(x=x, y=y2, mode='markers', name='gradient', marker_color='red'))
    # fig.add_trace(go.Scatter(x=x, y=y3, mode='markers', name='film_map', marker_color='green'))
    # fig.add_trace(go.Scatter(x=x, y=y4, mode='markers', name='filter_map', marker_color='orange'))
    fig.add_trace(go.Scatter(x=x, y=y5, mode='markers', name='rolling_mean', marker_color='blue'))
    # fig.add_trace(go.Scatter(x=x, y=y6, mode='markers', name='rolling_median', marker_color='yellow'))
    fig.add_trace(go.Scatter(x=x, y=y7, mode='markers', name='rolling_roughness', marker_color='black'))
    # fig.add_trace(go.Scatter(x=x, y=y8, mode='markers', name='rolling_max', marker_color='grey'))
    # fig.add_trace(go.Scatter(x=x, y=y9, mode='markers', name='rolling_min', marker_color='cyan'))
    fig.show()


if __name__ == "__main__":
    # filenames = [r'D:\test_data\profilometer_data_pos_5.OPDx']
    filenames = [r'D:\test_data\zb_profile_raw_04-24-2019_16_14_05.OPDx']
    for filename in filenames:
        logger.info(f'processing {filename}')
        raw_profile = convert_profile_binary_to_csv(filename)
        cropped_data = crop_data(raw_profile, 0, 1000)
        cropped_data = raw_profile
        leveled_profile = level_profile(cropped_data, filename, plot=True)
        break

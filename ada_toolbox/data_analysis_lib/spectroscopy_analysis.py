import numpy as np
import pandas
import logging
from typing import Union
import copy
from ada_toolbox.data_analysis_lib.exceptions import DataOutOfBounds
from matplotlib import pyplot as plt
from ada_toolbox.matplotlib_lib.matplotlib_tools import generate_spectra_plot_validation

# Raw plot for transmission
spectra_config = {
        "data": None,
        "x_col_name": "x_(nm)",
        "y_col_name": "raw_signal",
        "baseline_col_names": ['baseline_light', 'baseline_dark'],
        "title": "Raw Transmission",
        "x_label": "x_(nm)",
        "y_label": "Raw Intensity",
        "ax_args": {
            'linewidth': 1.5,
        },
        "ax_settings": [
            {'grid': {'kwargs': {'linestyle': ':'}}},
            {'axhline': {'kwargs': {'y': 0, 'c': 'grey'}}},
            {'axhline': {'kwargs': {'y': 2 ** 16, 'c': 'grey'}}},
            {'axvline': {'kwargs': {'x': 900,'c':  'grey', 'label': 'Spect.\ncrossover', 'linestyle': ':'}}},
        ],
}
# ----------------------------------------------General helper functions---------------------------------------------- #
def data_within_range(dat: list,
                      bound_lower: Union[int, float],
                      bound_upper: Union[int, float],
                      tolerance_lo=0,
                      tolerance_up=0):
    """
    This function checks if the values in an array are within range up to some tolerance
    :param dat: an array of data
    :param bound_lower: lower bound of the range
    :param bound_upper: upper bound of the range
    :param tolerance_lo: the allowed uncertainty on lower bound - should be a positive value
    :param tolerance_up: the allowed uncertainty on upper bound - should be positive value
    :return: boolean True if value within range; False if value outside the range
    """
    data = np.array(dat)
    # check if any value in data is larger then max allowed+tolerance
    if np.max(data) > bound_upper+tolerance_up:
        ind_greater = np.concatenate(np.where(data > (bound_upper+tolerance_up)))
        pass_up = False
    else:
        pass_up = True
        ind_greater = []
    # check if any value in data is smaller then min minus tolerance
    if np.min(data) < bound_lower-tolerance_lo:
        ind_less = np.concatenate(np.where(data < (bound_lower - tolerance_lo)))
        pass_lo = False
    else:
        pass_lo = True
        ind_less = []

    if len(ind_less) != 0 or len(ind_greater) != 0:
        comb_ind = np.append(np.array(ind_greater), np.array(ind_less))
        comb_ind = find_continious_seq(list(set(comb_ind)))
    else:
        comb_ind = []

    return (pass_up and pass_lo), comb_ind

def data_within_curves(data: list,
                       curve_lo: list,
                       curve_up: list,
                       tol_up=0,
                       tol_lo=0):
    """
    Helper functio used to check whether the data is within 2 curves, outputs true or false for each boundary curve, as
    as well as a range of indecies on which the function failed
    :param data: dataframe of a position data
    :param curve_lo: lower boundary curve
    :param curve_up: upper boundary curve
    :param tol_up: tolerance for upper boundary
    :param tol_lo: tolerance for lower boundary
    :return: returns a tuple:
             lo_pass: boolean for whether data is above lower curve
             up_pass: boolean for whether data is below upper curve
             lo_fail_indices: failed indeces for lower curve
             up_fail_indeces: failed indeces for upper curve
    """
    diff_up = np.subtract(np.array(curve_up), np.array(data))

    # check if it is below curve_up
    if np.min(diff_up) >= 0:
        up_pass = True
        up_fail_indices = []
    elif np.min(diff_up) <= 0 and np.abs(np.min(diff_up)) <= tol_up:
        up_pass = True
        up_fail_indices = []
    else:
        up_pass = False
        up_fail_indices = np.concatenate(np.where(diff_up<0))

    diff_lo = np.array(data) - np.array(curve_lo)

    # check if it is above curve_lo
    if np.min(diff_lo) >= 0:
        lo_pass = True
        lo_fail_indices = []
    elif np.min(diff_lo) <= 0 and np.abs(np.min(diff_lo)) <= tol_lo:
        lo_pass = True
        lo_fail_indices = []
    else:
        lo_pass = False
        lo_fail_indices = np.concatenate(np.where(diff_lo < 0))
    if lo_fail_indices != []:
        lo_fail_indices = find_continious_seq(lo_fail_indices)
    if up_fail_indices != []:
        up_fail_indices = find_continious_seq(up_fail_indices)

    return lo_pass, up_pass, lo_fail_indices, up_fail_indices


def column_from_df_with_key(df: pandas.DataFrame,
                            key: str) -> list:
    """
    Helper function that retrieves a column from csv via a key
    :param df: dataframe
    :param key: heading of the column
    :return: x is the nm value, y is the count number
    """
    x = df[key]
    x = pandas.Series.tolist(x)
    return x


def curve_length(x: list, y: list) -> float:
    """
    Find the length of the curve
    :param x: list of x values
    :param y: list of y values
    :return: length of the path as float
    """
    # get length of arrays
    len_x = x.__len__()
    len_y = y.__len__()

    # the arrays must be the same size
    if len_x != len_y:
        raise ValueError('The length of x and y arrays must be the same, check input into path_length')

    # find path length
    length = 0.
    for i in range(0, len_x-1):
        length = length + np.sqrt((x[i+1]-x[i])**2+(y[i+1]-y[i])**2)
    return length


def quantify_noise(raw_data: list,
                   smoothed_data: list,
                   uv_end_index=1537,
                   dead_band=30) -> tuple:
    """
    Returns average ir noise val,
    :param raw_data: raw data from spectrometer
    :param smoothed_data: smoothed data from csv
    :param uv_end_index: index at which the uv spectrum ends
    :param dead_band: the range of indecies to ignore - to avoid discontinuity between 2 jumps
    :return: noise_ir_avg - average ir noise
             noise_ir_max - max ir noise
             noise_uv_avg - average uv noise
             noise_uv_max - max uv noise
    """
    # find difference between smoothed and raw data
    noise = np.array(raw_data)-np.array(smoothed_data)

    # take absolute value of the noise
    noise = np.abs(noise)

    noise_ir = noise[0:uv_end_index-dead_band]
    noise_uv = noise[uv_end_index+dead_band:-1]

    # find the max
    noise_uv_avg = np.average(noise_uv)
    noise_ir_avg = np.average(noise_ir)

    noise_uv_max = np.max(noise_uv)
    noise_ir_max = np.max(noise_ir)

    return noise_ir_avg, noise_ir_max, noise_uv_avg, noise_uv_max


def find_continious_seq(array: np.ndarray):
    # sort the array
    arr = np.sort(array)
    if arr.max() == arr.min():
        return [(int(arr[0]), int(arr[0]))]
    previous_num = 0
    start = 0
    stop = None

    array_tuples = list()

    for i, num in enumerate(arr):
        if i == 0:
            start = arr[i]
            previous_num = arr[i]
        else:
            if num-previous_num == 1:
                previous_num = num
                if i == len(arr) - 1:
                    array_tuples.append((start, num))
                continue
            else:
                stop = previous_num
                array_tuples.append((start, stop))
                start = num
                previous_num = num

    return array_tuples


# -----------------------------------------------Dark baseline functions---------------------------------------------- #
def validate_dark_baseline(df: pandas.DataFrame,
                           bound_lo: Union[int, float],
                           bound_up: Union[int, float],
                           tol_lo: Union[int, float],
                           tol_up: Union[int, float],
                           logger: logging.Logger = None,
                           validartor_slack=None,
                           campaign: str = None,
                           sample: str = None,
                           position: str = None,
                           data_key='baseline_dark',
                           t_r: str=None):
    """
    Validates raw baseline_dark data
    :param df: data frame of raw data
    :param bound_lo: lower bound of the range
    :param bound_up: upper bound of the range
    :param tol_lo: the allowed uncertainty on lower bound - should be a positive value
    :param tol_up: the allowed uncertainty on upper bound - should be positive value
    :param logger: logger got logging
    :param wavelength_key: key for the x-axis in data plot
    :param data_key: key for the dark baseline heading in DataFrame
    :return: boolean True if passed False if failed
    """
    data = column_from_df_with_key(df, data_key)

    # STEP 1: Validate that the value is between bound_lo and bound_up
    passed, failed_ind = data_within_range(data, bound_lo, bound_up, tolerance_lo=tol_lo, tolerance_up=tol_up)

    if passed:
        logger.info(f'Dark baseline ({t_r}) is within {bound_lo} and {bound_up} with tolerance of {tol_lo} '
                    f'for lower bound and {tol_up} for upper bound -- VALIDATION PASSED')
        return True, None

    else:
        logger.warning(f'Dark baseline ({t_r}) is outside of {bound_lo} and {bound_up} with tolerance of {tol_lo} '
                       f'for lower bound and {tol_up} for upper bound -- VALIDATION FAILED')
        plot_config = copy.copy(spectra_config)
        plot_config["data"] = df
        plot_config["y_col_name"] = data_key
        plot_config["baseline_col_names"] = None
        figure = generate_spectra_plot_validation(**plot_config,
                                                  fail_indecies=failed_ind)

        if validartor_slack is not None and validartor_slack.channel is not None:
            validartor_slack.post(channel=validartor_slack.channel,
                                  text=f'Campaign: {campaign}, '
                                  f'Sample: {sample}, '
                                  f'Position: {position} -- '
                                  f'Dark baseline is outside of {bound_lo} and {bound_up} with tolerance of {tol_lo} '
                                  f'for lower bound and {tol_up} for upper bound -- VALIDATION FAILED')
        # TODO: Decide how to act on the failed validation
        return False, figure

    # TODO: Determine a way to validate the shape of the curve
    # STEP 2: Validate the shape via some method
    # Could use the following features: path length, area under the curve

    # TODO: Implement
    # STEP 3: Check if the noise isn't too large
    # quantified_data < threshhold_noise


# ---------------------------------------------Bright baseline functions---------------------------------------------- #
def validate_bright_baseline(df: pandas.DataFrame,
                             bound_lo: Union[int, float],
                             bound_up: Union[int, float],
                             tol_lo: Union[int, float],
                             tol_up: Union[int, float],
                             logger: logging.Logger = None,
                             validartor_slack = None,
                             campaign: str = None,
                             sample: str = None,
                             position: str = None,
                             wavelength_key='x_(nm)',
                             data_key='baseline_light',
                             t_r: str=None):
    """
    Validates raw baseline_bright data
    :param df: data frame object of raw data
    :param bound_lo: lower bound of the range
    :param bound_up: upper bound of the range
    :param tol_lo: the allowed uncertainty on lower bound - should be a positive value
    :param tol_up: the allowed uncertainty on upper bound - should be positive value
    :param logger: logger for logging
    :param wavelength_key: key for the x-axis in data plot
    :param data_key: key for the light baseline heading in DataFrame
    :return: boolean True if passed False if failed
    """
    wavelength = column_from_df_with_key(df, wavelength_key)
    data = column_from_df_with_key(df, data_key)

    passed, failed_ind = data_within_range(data, bound_lo, bound_up, tolerance_lo=tol_lo, tolerance_up=tol_up)

    # STEP 1: Validate that the value is between bound_lo and bound_up
    if passed:
        logger.info(
            f'Bright baseline is within {bound_lo} and {bound_up} with tolerance of {tol_lo} '
            f'for lower bound and {tol_up} for upper bound -- VALIDATION PASSED')
        return True, None
    else:
        logger.warning(
            f'Dark baseline is outside of {bound_lo} and {bound_up} with tolerance of {tol_lo} '
            f'for lower bound and {tol_up} for upper bound -- VALIDATION FAILED')
        if validartor_slack is not None and validartor_slack.channel is not None:
            validartor_slack.post(channel=validartor_slack.channel,
                                  text=f'Campaign: {campaign}, '
                                  f'Sample: {sample}, '
                                  f'Position: {position} -- '
                                  f'Bright baseline is outside of {bound_lo} and {bound_up} with tolerance of {tol_lo} '
                                  f'for lower bound and {tol_up} for upper bound -- VALIDATION FAILED')

        plot_config = copy.copy(spectra_config)
        plot_config["data"] = df
        plot_config["y_col_name"] = data_key
        plot_config["baseline_col_names"] = None
        figure = generate_spectra_plot_validation(**plot_config,
                                                  fail_indecies=failed_ind)
        # TODO: Decide how to act on the failed validation
        # raise DataOutOfBounds
        return False, figure


# ----------------------------------------------Sample signal functions----------------------------------------------- #
def validate_sample_signal(df: pandas.DataFrame,
                           tolerance,
                           logger: logging.Logger = None,
                           validartor_slack=None,
                           campaign: str = None,
                           sample: str = None,
                           position: str = None,
                           uv_end_index=1537,
                           dead_band=10,
                           raw_data_key='raw_signal',
                           baseline_dark='baseline_dark',
                           baseline_light='baseline_light',
                           t_r: str=None
                           ):
    """
        Validates raw baseline_bright data
        :param df: DataFrame of raw data
        :param tolerance: tolerance for exceeding data limits
        :param logger: logger for logging
        :param uv_end_index: the array index at which we stitch uv and ir data
        :param dead_band: number of data points to ignore around the stitch index due to discontinuity in data
        :param wavelength_key: data frame key for wavelength
        :param raw_data_key: data frame key for raw signal data
        :param raw_smoothed_key: data frame key for smoothed signal
        :param baseline_dark: data frame key for dark baseline
        :param baseline_light: dara frame key for light baseline
        :return: boolean True if passed False if failed
        """

    data = column_from_df_with_key(df, raw_data_key)
    baseline_light = column_from_df_with_key(df, baseline_light)
    baseline_dark = column_from_df_with_key(df, baseline_dark)

    # STEP 1: Validate that the UV signal is between dark and bright baselines with a certain tolerance
    data_measured_uv = data[0:uv_end_index-dead_band]
    data_baseline_dark_uv = baseline_dark[0:uv_end_index-dead_band]
    data_baseline_bright_uv = baseline_light[0:uv_end_index-dead_band]

    pass_uv_lo, pass_uv_up, lo_fail_indices_uv, up_fail_indices_uv = data_within_curves(data_measured_uv,
                                                                                        data_baseline_dark_uv,
                                                                                        data_baseline_bright_uv,
                                                                                        tol_up=tolerance,
                                                                                        tol_lo=tolerance)

    # STEP 2: Validate that the IR signal is between dark and bright baselines with a certain tolerance
    data_measured_ir = data[uv_end_index + dead_band:-1]
    data_baseline_dark_ir = baseline_dark[uv_end_index + dead_band:-1]
    data_baseline_bright_ir = baseline_light[uv_end_index + dead_band:-1]

    pass_ir_lo, pass_ir_up, lo_fail_indices_ir, up_fail_indices_ir = data_within_curves(data_measured_ir,
                                                                                        data_baseline_dark_ir,
                                                                                        data_baseline_bright_ir,
                                                                                        tol_up=tolerance,
                                                                                        tol_lo=tolerance)

    _, _, lo_failed_indices, up_fail_indices = data_within_curves(data,
                                                                  baseline_dark,
                                                                  baseline_light,
                                                                  tol_up=tolerance,
                                                                  tol_lo=tolerance)

    failed_indices = lo_failed_indices + up_fail_indices

    if pass_uv_lo:
        logger.info(
            f'The UV part of signal is above the dark baseline with tolerance of +/-{tolerance} '
            f'-- VALIDATION PASSED')
    else:
        logger.warning(
            f'The UV part of signal is NOT above the dark baseline with tolerance of +/-{tolerance} '
            f'-- VALIDATION FAILED')
        if validartor_slack is not None and validartor_slack.channel is not None:
            validartor_slack.post(channel=validartor_slack.channel,
                                  text=f'Campaign: {campaign}, '
                                  f'Sample: {sample}, '
                                  f'Position: {position} -- '
                                  f'The UV part of signal is NOT above the dark baseline with tolerance of +/-{tolerance} '
                                  f'-- VALIDATION FAILED')
    if pass_uv_up:
        logger.info(
            f'The UV part of signal is below the bright baseline with tolerance of +/-{tolerance} '
            f'-- VALIDATION PASSED')
    else:
        logger.warning(
            f'The UV part of signal is NOT below the bright baseline with tolerance of +/-{tolerance} '
            f'-- VALIDATION FAILED')
        if validartor_slack is not None and validartor_slack.channel is not None:
            validartor_slack.post(channel=validartor_slack.channel,
                                  text=f'Campaign: {campaign}, '
                                  f'Sample: {sample}, '
                                  f'Position: {position} -- '
                                  f'The UV part of signal is NOT below the bright baseline with tolerance of +/-{tolerance} '
                                  f'-- VALIDATION FAILED')
    if pass_ir_lo:
        logger.info(
            f'The IR part of signal is above the dark baseline with tolerance of +/-{tolerance} '
            f'-- VALIDATION PASSED')
    else:
        logger.warning(
            f'The IR part of signal is NOT above the dark baseline with tolerance of +/-{tolerance} '
            f'-- VALIDATION FAILED')
        if validartor_slack is not None and validartor_slack.channel is not None:
            validartor_slack.post(channel=validartor_slack.channel,
                                  text=f'Campaign: {campaign}, '
                                  f'Sample: {sample}, '
                                  f'Position: {position} -- '
                                  f'The IR part of signal is NOT above the dark baseline with tolerance of +/-{tolerance} '
                                  f'-- VALIDATION FAILED')

    if pass_ir_up:
        logger.info(
            f'The IR part of signal is below the bright baseline with tolerance of +/-{tolerance} '
            f'-- VALIDATION PASSED')
    else:
        logger.warning(
            f'The IR part of signal is NOT below the bright baseline with tolerance of +/-{tolerance} '
            f'-- VALIDATION FAILED')
        if validartor_slack is not None and validartor_slack.channel is not None:
            validartor_slack.post(channel=validartor_slack.channel,
                                  text=f'Campaign: {campaign}, '
                                  f'Sample: {sample}, '
                                  f'Position: {position} -- '
                                  f'The IR part of signal is NOT below the bright baseline with tolerance of +/-{tolerance} '
                                  f'-- VALIDATION FAILED')

    if pass_uv_lo and pass_ir_lo and pass_ir_up and pass_uv_up:
        return True, None
    else:
        plot_config = copy.copy(spectra_config)
        plot_config["data"] = df
        figure = generate_spectra_plot_validation(**plot_config,
                                                  fail_indecies=failed_indices)
        return False, figure


# -------------------------------------------- processed data validation --------------------------------------------- #


def validate_calibrated(df: pandas.DataFrame,
                        what_signal: str,
                        bound_lower,
                        bound_upper,
                        tolerance_up,
                        tolerance_lo,
                        logger: logging.Logger = None,
                        validartor_slack=None,
                        campaign: str = None,
                        sample: str = None,
                        position: str = None,
                        ignore: list = None,
                        raw_data_key='calibrated_signal',
                        wavelength_key='x_(nm)'):

    data = column_from_df_with_key(df, raw_data_key)

    useful_range = []

    if ignore is not None:
        ignore.sort()
        for range in ignore:
            useful_range_upper = list(np.where(df[wavelength_key]>range[1]))
            useful_range_lower = list(np.where(df[wavelength_key]<range[0]))
            # convert to set so then we get rid of duplicates
            useful_range.append(set(np.concatenate(useful_range_lower + useful_range_upper)))
            logger.info(f'NOTE: Excluding wavelength in {range[0]} nm and {range[1]} nm range for validation')

        # find intersection of sets
        range = useful_range[0]
        for s in useful_range:
            range = set.intersection(range, s)

        # convert back to list
        range = list(range)
        ind = find_continious_seq(range)
        mod_data = []
        for i in ind:
            mod_data += data[i[0]:i[-1]]

    passed, _ = data_within_range(mod_data, bound_lower=bound_lower,
                               bound_upper=bound_upper,
                               tolerance_lo=tolerance_lo,
                               tolerance_up=tolerance_up)

    _, failed_ind = data_within_range(data, bound_lower=bound_lower,
                                  bound_upper=bound_upper,
                                  tolerance_lo=tolerance_lo,
                                  tolerance_up=tolerance_up)

    if passed:
        logger.info(
            f'The calibrated {what_signal} is within {bound_lower} and {bound_upper} with a tolerance of {tolerance_lo} '
            f'for lower bound and {tolerance_up} for upper bound'
            f'-- VALIDATION PASSED')
        return passed, None
    else:
        logger.warning(
            f'The calibrated {what_signal} is NOT within {bound_lower} and {bound_upper} with a tolerance of {tolerance_lo} '
            f'for lower bound and {tolerance_up} for upper bound'
            f'-- VALIDATION FAILED')

        if validartor_slack is not None and validartor_slack.channel is not None:
            validartor_slack.post(channel=validartor_slack.channel,
                                  text=f'Campaign: {campaign}, '
                                  f'Sample: {sample}, '
                                  f'Position: {position} -- '
                                  f'The calibrated {what_signal} is NOT within {bound_lower} and {bound_upper} with a tolerance of {tolerance_lo} '
                                  f'for lower bound and {tolerance_up} for upper bound'
                                  f'-- VALIDATION FAILED')
        plot_config = copy.copy(spectra_config)
        plot_config["data"] = df
        figure = generate_spectra_plot_validation(**plot_config,
                                                  fail_indecies=failed_ind)
        return False, figure

if __name__ == "__main__":
    logger = logging.getLogger()
    file_good = 'sample_data/spectroscopy_data/_uvvis-blank_t-data_good.csv'
    file_bad = 'sample_data/spectroscopy_data/_uvvis-blank_t-data_bad.csv'

    file_good = pandas.read_csv(file_good)
    file_bad = pandas.read_csv(file_bad)

    result, _ = validate_dark_baseline(file_bad, 0, 0.1*2 ** 16, 0, 0, logger=logger)
    plt.show()
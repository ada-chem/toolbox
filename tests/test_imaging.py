import os

print("CWD: ", os.getcwd())
print("CWD Files: ", os.listdir(os.getcwd()))
print("File Directory: ", os.path.dirname(os.path.realpath(__file__)))

from ada_toolbox.data_analysis_lib.validation_thresholds import brightness_threshold, blur_threshold
from ada_toolbox.data_analysis_lib import image_analysis as img
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

os.chdir(os.path.dirname(os.path.realpath(__file__)))
os.chdir('..')

blank_image = os.path.join(os.getcwd(), "sample_data", "image_data", "blank.jpg")
blurry_image = os.path.join(os.getcwd(), "sample_data", "image_data", "blurry.jpg")
dark_image = os.path.join(os.getcwd(), "sample_data", "image_data", "dark.jpg")

assert (os.path.exists(blank_image))
assert (os.path.exists(blurry_image))
assert (os.path.exists(dark_image))

# Blur detection testing
assert (img.blur_detection(blank_image) < blur_threshold)
assert (img.blur_detection(blurry_image) > blur_threshold)

# Light detection testing
assert (img.brightness_detection(dark_image) < brightness_threshold)
assert (img.brightness_detection(blurry_image) > brightness_threshold)

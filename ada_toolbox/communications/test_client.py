# from ada_toolbox.communications.tcpip import TCPIPServer
from tcpip import TCPIPClient
import json

print("Starting client...")
client = TCPIPClient(address=('localhost', 1026), header_length=35, timeout=30)
with client.open_connection():
    print("send message")
    command = json.dumps({"Testing": "asdf"})
    # client.send_message_await_reply({"message": "Testing"}, silent=False)
    client.send_message_await_reply(message=command, silent=False)

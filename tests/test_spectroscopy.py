from ada_toolbox.data_analysis_lib.spectroscopy_analysis import *
import pandas
import logging
import matplotlib.pyplot as plt

logger = logging.getLogger()

# # Tests for curve_length
# x = [0, 1, 2, 3, 4]
# y = [0, 1, 2, 3, 4]
# # should pass
# assert curve_length(x, y) == np.sqrt(32)
#
# # should fail
# assert curve_length(x, y) != 0
# x = [0, 1, 2, 3, 4]
# y = [0, 1, 2, 3]
# # curve_length(x, y)
#
# # Tests for data_within_range
# max = 2**16
# min = 0
# uncert = 5
# data = [2**16, -1, 0, 10000]
# result, _ = data_within_range(data, bound_lower=min,
#                          bound_upper=max,
#                          tolerance_up=uncert,
#                          tolerance_lo=uncert)
# assert result is True
#
# data = [2**17, -1, 0, 10000]
# result, _ = data_within_range(data, bound_lower=min,
#                          bound_upper=max,
#                          tolerance_up=uncert,
#                          tolerance_lo=uncert)
# assert result is False
#
# data = [2**16, -100000, 0, 10000]
# result, _ = data_within_range(data, bound_lower=min,
#                          bound_upper=max,
#                          tolerance_up=uncert,
#                          tolerance_lo=uncert)
# assert result is False

# test dark baseline validation
# GOOD and BAD files
# TODO: Move test data to sample data folder.
file_good = 'sample_data/spectroscopy_data/_uvvis-blank_t-data_good.csv'
file_bad = 'sample_data/spectroscopy_data/_uvvis-blank_t-data_bad.csv'

file_good = pandas.read_csv(file_good)
file_bad = pandas.read_csv(file_bad)

x1 = column_from_df_with_key(file_good, 'x_(nm)')
y1 = column_from_df_with_key(file_good, 'baseline_dark')

x2 = column_from_df_with_key(file_bad, 'x_(nm)')
y2 = column_from_df_with_key(file_bad, 'baseline_dark')

result, _ = validate_dark_baseline(file_good, 0, 0.1*2**16, 0, 0, logger=logger)
assert result is True
result, _ = validate_dark_baseline(file_bad, 0, 0.1*2**16, 0, 0, logger=logger)
assert result is False
# test sample data validation
result, _ = validate_sample_signal(file_good, tolerance=10000, logger=logger)
assert result is True
result, _ = validate_sample_signal(file_bad, tolerance=100, logger=logger)
assert result is False
result, _ = validate_bright_baseline(file_good, 0, 2**16, 0, 0, logger=logger)
assert result is True
result, _ = validate_bright_baseline(file_bad, 2000, 2**16, 0, 0, logger=logger)
assert result is False

passed, _ = validate_calibrated(df=file_good,
                    what_signal='transmision',
                    bound_lower=0,
                    bound_upper=1,
                    tolerance_up=0.1,
                    tolerance_lo=0.1,
                    logger=logger,
                    ignore=[[0, 250],[500, 600]])
assert passed is True

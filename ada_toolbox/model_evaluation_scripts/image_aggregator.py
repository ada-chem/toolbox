import os
import shutil

desktop = os.path.join(os.path.join(os.path.expanduser('~')), 'Desktop')
desktop_files = os.listdir(desktop)
zb_data = os.path.join(desktop, 'zb-data')
campaigns = os.listdir(zb_data)

rootdir = zb_data

for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith(".jpg"):
            directories = subdir.split('/')
            campaign = directories[-2]
            sample = directories[-1]
            new_name = f"{campaign}_{sample}_{file}"
            shutil.copy(os.path.join(subdir, file), os.path.join(desktop, 'images', new_name))

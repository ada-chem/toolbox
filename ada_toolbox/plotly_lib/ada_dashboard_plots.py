import plotly.graph_objects as go


def spectroscopy_plot(df):
    # Initialize figure
    fig = go.Figure()

    unique_samples = df['sample'].unique()
    unique_samples.sort()

    for sample in unique_samples:
        sample_df = df[df['sample'] == sample]

        # Add Traces
        fig.add_trace(
            go.Scattergl(x=list(sample_df['x_(nm)']),
                         y=list(sample_df['A_f']),
                         name=f"Sample {sample}",
                         )
        )

        fig.add_trace(
            go.Scattergl(x=list(sample_df['x_(nm)']),
                         y=list(sample_df['log_A_f']),
                         name=f"Sample {sample}",
                         visible=False,
                         )
        )

    fig.update_layout(
        updatemenus=[
            go.layout.Updatemenu(
                active=0,
                buttons=list([
                    dict(label="Absorbance",
                         method="update",
                         args=[{"visible": [True, False]}]),
                    dict(label="Log Absorbance",
                         method="update",
                         args=[{"visible": [False, True]}]),
                ]),
                direction="down",
                pad={"r": 5, "t": 5},
                showactive=True,
                x=0.1,
                xanchor="left",
                y=1.06,
                yanchor="top"
            )
        ])

    fig.update_xaxes(title_text='x (nm)')
    fig.update_yaxes(title_text='Absorbance (f)')

    fig.update_layout(
        template='plotly_white',
        height=900,
        annotations=[
            go.layout.Annotation(text="Data:", x=0, xref="paper", y=1.05, yref="paper",
                                 align="left", showarrow=False),
        ]
    )

    return fig

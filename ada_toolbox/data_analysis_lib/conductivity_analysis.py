import pandas as pd
import numpy as np
import logging

from sklearn import linear_model
from sklearn.metrics import r2_score

import warnings
from ada_toolbox.data_analysis_lib.validation_thresholds import max_voltage, min_voltage, outlier_threshold_sd, \
    number_valid_observations_lm_threshold, r2_score_threshold, min_current_pA, max_current, min_current

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

warnings.filterwarnings("ignore")


def flag_outliers(data: pd.DataFrame,
                  current_col: str = "Current",
                  voltage_col: str = "Voltage",
                  max_voltage: float = max_voltage,
                  min_voltage: float = min_voltage):
    """
    This function reads in the raw data and flags observations with invalid voltage.
    :param data: Raw conductivity dataframe
    :param current_col: The name of the current column
    :param voltage_col: The name of the voltage column
    :param max_voltage: The maximum possible voltage
    :param min_voltage: The minimum possible voltage
    :return: A clean dataframe that only contains valid data
    """

    data['outlier'] = np.where(
        np.logical_or(np.greater(data[voltage_col], max_voltage), np.less(data[voltage_col], min_voltage)), 1, 0)

    return data


def remove_saturated_points(data: pd.DataFrame,
                            current_col: str = "Current",
                            voltage_col: str = "Voltage",
                            max_voltage: float = max_voltage,
                            min_voltage: float = min_voltage,
                            max_current: float = max_current,
                            min_current: float = min_current, ):
    """
    This function reads in the raw data and removes observations with voltage invalid voltage.
    :param data: Raw conductivity dataframe
    :param current_col: The name of the current column
    :param voltage_col: The name of the voltage column
    :param max_voltage: The maximum possible voltage
    :param min_voltage: The minimum possible voltage
    :param max_current: The maximum possible current
    :param min_current: The minimum possible current
    :return: A clean dataframe that only contains valid data
    """

    # Make mask
    mask = (data[voltage_col] < max_voltage) & \
           (data[voltage_col] > min_voltage) & \
           (data[current_col] < max_current) & \
           (data[current_col] > min_current)

    # Filter data
    mdf = data[mask]

    # Return
    return mdf, mask


def valid_measurements(data: pd.DataFrame):
    """
    This function counts the number of valid measurements. If there are less than 2 measurements, we cannot calculate
    conductance.
    :param data: Conductivity dataframe
    :return: Number of valid observations
    """
    df = remove_saturated_points(data=data)
    len_df = len(df)

    logger.info(f"Conductivity has {len_df} valid measurements.")

    return len_df


def linear_regression_RANSAC(data: pd.DataFrame,
                             x_axis: str = "Voltage",
                             y_axis: str = "Current",
                             min_obs: int = number_valid_observations_lm_threshold):
    """
    https://en.wikipedia.org/wiki/Coefficient_of_determination
    R - squared is one minus the square of the observed residuals over the total residual. A R2 score of 1 suggests that
    the linear model fits the data perfectly (there are no observed residual). Scores closer to 0 suggest that the
    linear model does not
    :param data: The cleaned conductivity dataframe
    :param x_axis: The name of the current column
    :param y_axis: The name of the voltage column
    :param min_obs: The minimum number of observations required to create the linear model
    :return: R2 score, slope, intercept
    """

    # Get the number of negative current measurements
    num_neg = data[data[y_axis] < 0].shape[0]

    # Create the response if any fitting step fails
    fail = [0] * 4

    # If multiple negative values, return fail
    if num_neg > 1:
        logger.warning(f"Dataframe has too many ({num_neg}) negative currents")
        return fail

    # If too few values, return fail
    if len(data) <= min_obs:
        logger.warning(f"Dataframe needs at least {min_obs} observations to create a linear model.")
        return fail

    else:

        try:
            x = data[x_axis].values.reshape(-1, 1)
            y = data[y_axis].values.reshape(-1, 1)

            lm = linear_model.RANSACRegressor()
            lm.fit(x, y)
            y_predicted = lm.predict(x)

            r2 = float(r2_score(y[lm.inlier_mask_], y_predicted[lm.inlier_mask_]))
            slope = float(lm.estimator_.coef_[0])
            intercept = float(lm.estimator_.intercept_[0])
            inliers = np.where(lm.inlier_mask_)[0]

            # If the slope is negative, fail
            if slope < 0:
                return fail

            return r2, slope, intercept, inliers

        except Exception as e:
            print(e)
            return fail


def linear_regression(data: pd.DataFrame,
                      x_axis: str = "Voltage",
                      y_axis: str = "Current",
                      min_obs: int = number_valid_observations_lm_threshold):
    """
    https://en.wikipedia.org/wiki/Coefficient_of_determination
    R - squared is one minus the square of the observed residuals over the total residual. A R2 score of 1 suggests that
    the linear model fits the data perfectly (there are no observed residual). Scores closer to 0 suggest that the
    linear model does not
    :param data: The cleaned conductivity dataframe
    :param x_axis: The name of the current column
    :param y_axis: The name of the voltage column
    :param min_obs: The minimum number of observations required to create the linear model
    :return: R2 score, slope, intercept
    """

    if len(data) < min_obs:
        logger.warning(f"Dataframe needs at least {min_obs} observations to create a linear model.")
        r2 = 0
        slope = 0
        intercept = 0
        return r2, slope, intercept

    else:
        x = data[x_axis].values.reshape(-1, 1)
        y = data[y_axis].values.reshape(-1, 1)

        lm = linear_model.LinearRegression()
        lm.fit(x, y)
        y_predicted = lm.predict(x)

        r2 = float(r2_score(y, y_predicted))
        slope = float(lm.coef_[0])
        intercept = float(lm.intercept_[0])

        if slope < 0:
            r2 = 0
            slope = 0
            intercept = 0
            return r2, slope, intercept

        else:
            return r2, slope, intercept


def remove_outliers(slope, intercept, clean_df, x_axis, y_axis):
    # Create the line for the predicted
    clean_df['y_i'] = clean_df[x_axis] * slope + intercept

    # Residual
    clean_df['residual'] = abs(clean_df['y_i'] - clean_df[y_axis])

    # Calculate the standard deviation from the predicted
    std_pop = np.sqrt(np.mean(abs(clean_df[y_axis] - clean_df['y_i']) ** 2))

    # standard deviations observed from predicted
    clean_df['z_score'] = clean_df['residual'] / std_pop

    # note outliers
    clean_df['outlier'] = np.where(clean_df['z_score'] > outlier_threshold_sd, 1, 0)

    # keep valid rows
    df = clean_df[clean_df['outlier'] == 0]

    return df


def conductance_by_position(clean_df: pd.DataFrame,
                            x_axis: str = "Voltage",
                            y_axis: str = "Current",
                            min_obs: int = 2,
                            outlier_threshold_sd=outlier_threshold_sd):
    r2, slope, intercept = linear_regression(data=clean_df,
                                             x_axis=x_axis,
                                             y_axis=y_axis,
                                             min_obs=min_obs)

    if slope <= 0:
        conductance = {
            'slope': 0,
            'intercept': 0,
            'r2': 0,
        }
        return conductance

    # Create the line for the predicted
    clean_df['y_i'] = clean_df[x_axis] * slope + intercept

    # Residual
    clean_df['residual'] = abs(clean_df['y_i'] - clean_df[y_axis])

    # Calculate the standard deviation from the predicted
    std_pop = np.sqrt(np.mean(abs(clean_df[y_axis] - clean_df['y_i']) ** 2))

    # standard deviations observed from predicted
    clean_df['z_score'] = clean_df['residual'] / std_pop

    # note outliers
    clean_df['outlier'] = np.where(clean_df['z_score'] > outlier_threshold_sd, 1, 0)

    # keep valid rows
    df = clean_df[clean_df['outlier'] == 0]

    r2, slope, intercept = linear_regression(data=df,
                                             x_axis=x_axis,
                                             y_axis=y_axis,
                                             min_obs=min_obs)

    if slope <= 0:
        conductance = {
            'slope': 0,
            'intercept': 0,
            'r2': 0,
        }
        return conductance

    else:
        conductance = {
            'slope': slope,
            'intercept': intercept,
            'r2': r2,
        }
        return conductance


if __name__ == "__main__":
    data = {
        'Voltage': [-400, 1, 2, 3, 20, 5, 6, 7, 8, 9, 10, 400],
        'Current': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    }

    # Creates pandas DataFrame.
    df = pd.DataFrame(data)

    clean_df = remove_saturated_points(data=df)

    print(conductance_by_position(clean_df=clean_df))

    # # Clean the raw data
    # df_clean = remove_saturated_points(df)
    #
    # # Number of valid measurements present
    # num_valid = valid_measurements(data=df_clean)
    #
    # # Linear model needs atleast 2 observations to draw a line
    # if num_valid >= number_valid_observations_lm_threshold:
    #     r2, slope, intercept = linear_regression(data=df_clean)
    #
    #     #
    #     if r2 > r2_score_threshold:
    #         logger.info("Conductance measurement is valid.")
    #
    #     else:
    #         logger.warning("Conductance measurement is not valid.")
    #
    # else:
    #     logger.warning("Not enough valid measurements to calculate conductance.")

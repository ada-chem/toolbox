import numpy as np
import cv2
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def brightness_detection(image_directory: str = None, image: np.ndarray = None):
    """
    Detect the level of brightness of an image.
    :param image_directory: Image path directory.
    :param image: Image nd array
    :return: Brightness with range 0 to 1. 0 being dark, 1 being bright.
    """

    if image is None:

        try:
            # Load the image
            image = cv2.imread(image_directory)
            logger.info(f"Found image: {image_directory}")

        except Exception as e:
            logger.error(e)
            return None

    # Convert to grayscale using pillow
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Convert to absolute scale
    abs_gray = abs(gray)

    # Normalize the array by the max sharpness possible. This should be 255 from gray scale intensity.
    norm_gray = abs_gray / 255

    brightness = np.amax(norm_gray)

    logger.info(f"Brightness calculated to be: {brightness}")

    return brightness


def blur_detection(image_directory: str = None, image: np.ndarray = None):
    """
    Detect the level of blur of an image.
    :param image_directory: Image path directory.
    :param image: Image nd array
    :return: blurriness, range of 0 to 1. 0 being not blurry, 1 being blurry.
    """

    if image is None:

        try:
            # Load the image
            image = cv2.imread(image_directory)
            logger.info(f"Found image: {image_directory}")

        except Exception as e:
            logger.error(e)
            return None

    # Convert the image into gray scale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Conduct sobel convolution in both x and y direction.
    # Sobel convolution takes the derivative between 'ksize' neighbouring pixels in x and y directions.
    # The new matrix tells us how fast pixels change intensity, which is useful for identifying if something
    # is in focus or not. An image that is blurred will have neighbouring pixels of similar intensity.
    # An image that is in focus should have more distinct edges, and thus higher derivatives.
    sobelx = cv2.Sobel(src=gray, ddepth=cv2.CV_64F, dx=1, dy=0, ksize=1)
    sobely = cv2.Sobel(src=gray, ddepth=cv2.CV_64F, dx=0, dy=1, ksize=1)

    # Combine the x and y edge detection into a single matrix
    max_sobel = np.maximum(sobelx, sobely)

    # Convert to absolute scale
    abs_sobel = abs(max_sobel)

    # Normalize the array by the max sharpness possible. This should be 255 from gray scale intensity.
    norm_sobel = abs_sobel / 255

    # Get histogram of normalized sobel matrix. Histogram is bin count array and bin intensity array.
    histogram = np.histogram(norm_sobel)

    # number of bins
    scale = len(histogram[0])

    # total number of pixels
    pixels = sum(histogram[0])

    # Blurriness of 0 means its clear, aka not blurry
    blurriness = 0

    for index in range(0, scale):
        # Pixels per bin / total pixels
        ratio = histogram[0][index] / pixels

        # Ratio of pixels in bin by bin intensity
        blurriness += ratio * histogram[1][index]

    logger.info(f"Blurriness calculated to be: {blurriness}")

    return blurriness


if __name__ == "__main__":

    blurry_image = "/Users/teddyhaley/PycharmProjects/toolbox/sample_data/image_data/blurry.jpg"
    dark_image = "/Users/teddyhaley/PycharmProjects/toolbox/sample_data/image_data/dark.jpg"
    dark_image2 = "/Users/teddyhaley/Desktop/spincoated_slide_before_annealing1.jpg"

    blur_detection(image_directory=blurry_image)
    blur_detection(image_directory=dark_image)
    brightness_detection(image_directory=blurry_image)
    brightness_detection(image_directory=dark_image)
    brightness_detection(image_directory=dark_image2)


import socket
import threading
import time

def create_client(message="asdf", sleep_time=5):
    ClientSocket = socket.socket()
    host = '127.0.0.1'
    port = 1235

    print('Waiting for connection')
    try:
        ClientSocket.connect((host, port))
    except socket.error as e:
        print(str(e))

    Response = ClientSocket.recv(1024)
    while True:
        # Ping the server every sleep_time seconds until the end of time.
        ClientSocket.send(str.encode(message))
        Response = ClientSocket.recv(1024)
        print(Response.decode('utf-8'))
        time.sleep(sleep_time)

    ClientSocket.close()

if __name__ == "__main__":
    t0 = threading.Thread(target=create_client, args=("message from client 0",), daemon=True)
    t0.start()
    t1 = threading.Thread(target=create_client, args=("message from client 1",), daemon=True)
    t1.start()
    while True:
        pass

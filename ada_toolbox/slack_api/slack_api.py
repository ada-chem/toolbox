import time
import logging
import slack
import ssl as ssl_lib
import certifi
import requests

logger = logging.getLogger(__name__)

bots = {
    "Johnny5": "xoxb-276362573270-708342361911-vlySqklAAZ5tt2DN1pkDYFUK",
    "Validator": "xoxb-276362573270-676795193922-uSwBVGLCQksuCSlCyj8CRm4Z",
    "Bendruino": "xoxb-276362573270-696354499138-FIaipxTsVTtgc3pXpxnHmQbD"
}


class Slack:
    """
    The slack class acts as a wrapper around the Slack Client API.
    By default, the Validator slack bot is used to post to channels in ada-chem.
    """

    def __init__(self, token=None, bot=None):

        # if both token and bot are used
        if token is not None and bot is not None:
            logger.info(f"Both token and bot are specified. Trying the bot first.")

            # Choose the bot by default
            try:
                token_bot = bots[bot]
                self.token = token_bot
                client = slack.WebClient(token=token_bot,
                                         ssl=ssl_lib.create_default_context(cafile=certifi.where()))

            # Try the token if the bot is invalid
            except Exception as e:
                logger.warning(f"{e}. Invalid bot. Trying token.")

                logger.warning(f"Defaulting to use Johnny 5.")
                token_bot = bots["Johnny5"]
                self.token = token_bot
                client = slack.WebClient(token=token_bot,
                                         ssl=ssl_lib.create_default_context(cafile=certifi.where()))

        elif token is not None:
            try:
                self.token = token
                client = slack.WebClient(token=token,
                                         ssl=ssl_lib.create_default_context(cafile=certifi.where()))

            except Exception as e:
                logger.warning(f"{e}. Defaulting to use Johnny 5.")
                token_bot = bots["Johnny5"]
                self.token = token_bot
                client = slack.WebClient(token=token_bot,
                                         ssl=ssl_lib.create_default_context(cafile=certifi.where()))

        elif bot is not None:
            try:
                token_bot = bots[bot]
                self.token = token_bot
                client = slack.WebClient(token=token_bot,
                                         ssl=ssl_lib.create_default_context(cafile=certifi.where()))
            except Exception as e:
                logger.warning(f"{e}. Invalid bot. Using Johnny 5 instead.")
                token_bot = bots["Johnny5"]
                self.token = token_bot
                client = slack.WebClient(token=token_bot,
                                         ssl=ssl_lib.create_default_context(cafile=certifi.where()))

        # Use Johnny 5 if none specified
        else:
            logger.info("No token or bot specified. Using Johnny 5 by default.")
            token_bot = bots["Johnny5"]
            self.token = token_bot
            client = slack.WebClient(token=token_bot,
                                     ssl=ssl_lib.create_default_context(cafile=certifi.where()))

        self.client = client

    def post(self, channel, text):
        """
        Text post to channel where the bot with specified token lives.
        :param channel: Name of channel
        :param text: Text to go in post
        :return: None
        """
        try:
            r = self.client.chat_postMessage(
                channel=channel,
                text=text
            )

            return r

        except Exception as e:
            logger.warning(e)

    def post_file(self, channel, title=None, file=None):
        """
        Post a file to a channel.
        :param channel: The name of the channel to post to
        :param title: Caption of file uploaded
        :param file: File path of file desired to upload to slack post
        :return: None
        """

        try:
            r = requests.post('https://slack.com/api/files.upload',
                              data={'token': self.token,
                                    'channels': [channel],
                                    'title': title},
                              files={'file': open(file, 'rb')})

            return r

        except Exception as e:
            logger.warning(e)

    def reset_chemos(self, experiment='emulation', channel='chemos_interaction'):

        """
        Reset the ChemOS experiment.
        :return: None
        """

        self.post(channel=channel, text=f'Reset {experiment}')

    def reset_and_start_chemos(self, experiment='emulation', channel='chemos_interaction', iterations=100):

        """
        Start the emulator ChemOS environment for n iterations.
        :param continuation: whether this experiment should be continued in the currently running environment
        :return: None
        """

        # Check the type
        if type(iterations) is not int:
            raise ValueError('iter is not an integer.')

        # Resets the ChemOS experiment over slack.
        self.reset_chemos()

        time.sleep(3)

        # Start the experiment
        self.post(channel=channel, text=f'Start {experiment} for {iterations} iterations')


if __name__ == "__main__":
    # setup logging
    logging.basicConfig(
        level=logging.DEBUG
    )

    Slack(bot="Validator").post(channel='test', text='validator')
    Slack(bot="Johnny5").post(channel='test', text='johnny 5')

    Slack().post(channel='test', text='no bot')
    Slack(bot="Validator",
          token="xoxb-276362573270-708342361911-vlySqklAAZ5tt2DN1pkDYFUK").post(channel='test',
                                                                                text='both valid.')
    Slack(bot="Johnny5",
          token="xoxb-276362573270-708342361911-vlySqklAAZ5tt2DN1pkDYFU").post(channel='test',
                                                                               text='Valid bot')
    Slack(bot="Johnny",
          token="xoxb-276362573270-708342361911-vlySqklAAZ5tt2DN1pkDYFUK").post(channel='test',
                                                                                text='valid token')

    Slack(token="xoxb-276362573270-708342361911-vlySqklAAZ5tt2DN1pkDYFUK").post(channel='test',
                                                                                text='token')

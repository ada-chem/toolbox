# from ada_toolbox.communications.tcpip import TCPIPServer
from tcpip import TCPIPServer
import pickle
import json
import time


print("Starting server...")
server = TCPIPServer(address=('localhost', 1026))
with server.get_message_return_reply():
    print(server.buffer)
    command = json.loads(server.buffer.decode("utf-8"))
    print(command)
    ret = {"command": command}
    time.sleep(1)
    server.reply = pickle.dumps(ret)

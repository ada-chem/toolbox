#!/usr/bin/env bash

pip install -U setuptools
pip install git+https://gitlab.com/ada-chem/toolbox@master#egg=ada_toolbox
python3 tests/test_imaging.py

# Check exit status
if [[ $? = 0 ]]; then
    echo "Imaging Test Passed"
else
    echo "Imaging Test Failed: $?"
    exit 1
fi

python3 tests/test_spectroscopy.py

# Check exit status
if [[ $? = 0 ]]; then
    echo "Spectroscopy Test Passed"
else
    echo "Spectroscopy Test Failed: $?"
    exit 1
fi

python3 tests/test_conductivity.py

# Check exit status
if [[ $? = 0 ]]; then
    echo "Conductivity Test Passed"
else
    echo "Conductivity Test Failed: $?"
    exit 1
fi
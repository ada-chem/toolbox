"""
A library of helpful plotting functions based around the matplotlib library.
"""

# Global
import logging
from pprint import pprint
from functools import reduce
from matplotlib import colorbar
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.tri as mtri
from matplotlib import cm
from matplotlib.path import Path as MPLPath
import matplotlib.patches as patches
import pandas as pd
import numpy as np
from typing import List, Dict, Tuple
from scipy.ndimage import gaussian_filter, label as ndimage_label
from bayes_opt import UtilityFunction
import json
import os
import copy
from decimal import Decimal
import scipy.stats as stats
import matplotlib.patches as mpatches

from ada_toolbox.matplotlib_lib import matplotlib_tools as mp
from ada_toolbox.analytical_lib import analytical_tools as at
from ada_toolbox.data_analysis_lib import conductivity_analysis as cond

logger = logging.getLogger(__name__)

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def format_e(n):
    # convert n to scientific notation with 3 decimal places
    a = '%.3E' % n
    # remove extra 0s, format, and return
    return a.split('E')[0].rstrip('0').rstrip('.') + 'E' + a.split('E')[1]


def proc_passed_kwargs(
        kwargs
):
    """
    Converts the state of passed kwargs from None to an empty dict if needed
    :param kwargs: The kwargs to convert
    :return: kwargs as a dict
    """

    # If none, return empty dict
    if kwargs is None:
        return {}
    else:
        return kwargs


def two_points_to_parallel_beizer(
        x0: Tuple[float, float],
        x1: Tuple[float, float],
        shift: float = 0.5,
):
    """
    Creates a matplotlib Beizer path from two points.  The Beizer node handles
    are parallel to the x axis, to create the required curves for the parallel
    coordinate plot.
    :param x0: The coordinates for point 0.
    :param x1: The coordinates for point 1.
    :param shift: The shift to be used for the node handles.
    :return: The matplotlib path object.
    """

    # The parallel coordinate plot is composed of many small line segments
    # spanning between the vertical y axes.  These Beizers are composed of four
    # nodes.  The outer two rest on the y axes where the curve and the axis
    # intersect.  The other two are within, and give the curve it's shape.
    # These beizer_codes define the type of nodes to use.
    beizer_codes = [
        MPLPath.MOVETO,
        MPLPath.CURVE4,
        MPLPath.CURVE4,
        MPLPath.CURVE4,
    ]

    # Create the verticies (or node positions) for the path
    verticies = [
        x0,
        (x0[0] + shift, x0[1]),
        (x1[0] - shift, x1[1]),
        x1,
    ]

    # Create the path
    path = MPLPath(verticies, beizer_codes)

    return path


def format_labels(
        keys: List[str],
        labels: Dict[str, str] = None,
        label_units: Dict[str, str] = None,
        dictionary: bool = False,
) -> List[str] or Dict:
    """
    Generate the formatted strings to use for matplotlib axis labels.
    :param keys: A list of keys
    :param labels: The strings to use for the labels, keyed by the keys
    :param label_units: The units to use
    :return:
    """

    # If the labels weren't passed, use the keys
    if labels is None:
        if dictionary:
            label_str = {}
            for key in keys:
                label_str[key] = key
        else:
            label_str = keys
    else:
        if dictionary:
            label_str = {}
            for key in keys:
                label_str[key] = labels.get(key, key)
        else:
            label_str = [labels[key] for key in keys]

    # If units were included, append in parentheses.
    if label_units is not None:
        if dictionary:
            label_str_units = {}
            for label, key in zip(label_str, keys):
                label_str_units[key] = (label + ' $\it{(' + label_units[key] + ')}$')
        else:
            label_str_units = []
            for label, key in zip(label_str, keys):
                label_str_units.append(label + ' $\it{(' + label_units[key] + ')}$')

        return label_str_units
    return label_str


def values_to_color_list(
        values,
        color_map_name: str = 'viridis',
) -> List[float]:
    """
    Converts a list of values to a list of RGB values for a given color map.
    :param values: List of values to generate colors with
    :param color_map_name: Name of matplotlib colormap to use.
    :return: List of RGBA values.
    """

    # Get the color map
    color_map = cm.get_cmap(color_map_name)

    # Normalize the array
    values_array = np.array(values)
    values_array_norm = at.normalize_array(values_array)

    # Get color map
    color_array = color_map(values_array_norm)

    return color_array


def create_corner_plot(
        data: pd.DataFrame,
        varied_col_names: List[str],
        measured_name: str,
        labels: Dict[str, str] = None,
        units: Dict[str, str] = None,
        title: str = 'corner_plot',
        figsize: Tuple[float, float] = (11, 8.5),
        fig_args: Dict = None,
        ax_args: Dict = None,
        scatter_args: Dict = None,
        hist_args: Dict = None,
        line_args: Dict = None,
):
    """
    Create a corner plot, or the non-redundant half of a matrix scatter plot
    with cross-hairs for max value, and histograms of distributions.

    :param data: A dataframe containing the data.
    :param varied_col_names: The column names of the dataframe to use to create
    the relevant axis.
    :param measured_name: The column name to use to create the color values.
    :param labels: A dictionary keyed by the values from varied_col_names.  The
    values from this dict will be used to create the axis labels.
    :param units: A dictionary keyed by the values from varied_col_names.
    The values from this dict will be used to create the axis labels.
    :param title: title of the plot.
    :param figsize: the size of the figure in inches.
    :param fig_args: A dict of kwargs to pass to the figure.
    :param ax_args: A dict of kwargs to pass to every axes.
    :param scatter_args: A dict of kwargs to pass to the scatter axes.
    :param hist_args: A dict of kwargs to pass to the hist axes.
    :param line_args: A dict of kwargs to pass to the vert and horizontal lines.
    :return: The figure.
    """

    # Convert the kwargs
    fig_args = proc_passed_kwargs(fig_args)
    ax_args = proc_passed_kwargs(ax_args)
    scatter_args = proc_passed_kwargs(scatter_args)
    hist_args = proc_passed_kwargs(hist_args)
    line_args = proc_passed_kwargs(line_args)

    # Create the figure
    figure: plt.Figure = plt.figure(
        figsize=figsize,
        **fig_args
    )

    # add title
    figure.suptitle(title)

    # Remove spacing between subplots
    figure.subplots_adjust(
        wspace=0,
        hspace=0,
        top=0.95,
        bottom=0.05,
    )

    # Get the number of varied parameters
    varied_len = len(varied_col_names)

    # Get the color values
    colors = values_to_color_list(
        values=data[measured_name],
    )

    # Generate the formatted labels
    formatted_labels = format_labels(
        keys=varied_col_names,
        labels=labels,
        label_units=units
    )

    # Determine the row value for the best sample
    best_measured_loc = data[measured_name].idxmax()

    # Begin creating each axis by looping through the cross of the varied_params
    # The y-loop is reversed.
    count = 0
    for x_pos, x_var_name in enumerate(varied_col_names):
        for y_pos, y_var_name in enumerate(varied_col_names[::-1]):

            # Calculate the subplot position to use, counting as you read
            subplot_pos = (varied_len - y_pos - 1) * varied_len + x_pos + 1

            # Calculate the plot bounds
            plot_x_min, plot_x_max = at.get_plotting_bounds(data[x_var_name])
            plot_y_min, plot_y_max = at.get_plotting_bounds(data[y_var_name])

            # If this current combination of the x_pos and y_pos values results
            # in a position that's a matrix comparision position, create the
            # scatter plot
            if y_pos + 1 < varied_len - x_pos:

                # Create the axis
                ax: plt.Axes = figure.add_subplot(
                    varied_len,
                    varied_len,
                    subplot_pos,
                    **ax_args,
                )

                # if this is the first axis, add title
                if count == 0:
                    add_title_to_axes(ax, title)
                    count = 1

                # Plot the scatter data
                ax.scatter(
                    data[x_var_name],
                    data[y_var_name],
                    color=colors,
                    s=10,
                    **scatter_args
                )

                # If this axis is on the bottom-most or left-most positions, add
                # the legend labels.  Else, remove the ticks.
                if y_pos == 0:
                    ax.set_xlabel(formatted_labels[x_pos], rotation=20)
                else:
                    ax.axes.get_xaxis().set_ticks([])

                if x_pos == 0:
                    ax.set_ylabel(formatted_labels[::-1][y_pos], rotation=70)
                else:
                    ax.axes.get_yaxis().set_ticks([])

                # Set axis ranges
                ax.set_xlim(plot_x_min, plot_x_max)
                ax.set_ylim(plot_y_min, plot_y_max)

                # Add crosshairs for best value
                ax.axvline(
                    x=data[x_var_name][best_measured_loc],
                    zorder=0,
                    color='lightgrey',
                    **line_args
                )

                ax.axhline(
                    y=data[y_var_name][best_measured_loc],
                    zorder=0,
                    color='lightgrey',
                    **line_args
                )

            # If a histogram position
            if varied_len - x_pos - 1 == y_pos:

                # Create the axis
                ax: plt.Axes = figure.add_subplot(
                    varied_len,
                    varied_len,
                    subplot_pos,
                    **ax_args,
                )

                # Create histogram
                ax.hist(
                    data[x_var_name],
                    range=(plot_x_min, plot_x_max),
                    bins=10,
                    facecolor='whitesmoke',
                    edgecolor='grey',
                    linewidth=1,
                    **hist_args
                )

                # Plot cross line
                ax.axvline(
                    x=data[x_var_name][best_measured_loc],
                    color='lightgrey',
                    **line_args
                )

                # Format
                ax.set_xlim(plot_x_min, plot_x_max)

                # Set x axis labelling
                if y_pos == 0:
                    ax.set_xlabel(formatted_labels[::-1][y_pos])
                else:
                    ax.axes.get_xaxis().set_ticks([])

                # Set y axis labelling
                ax.axes.get_yaxis().set_ticks([])

    formatted_labels_dict = format_labels(
        keys=[measured_name],
        labels=labels,
        label_units=units,
        dictionary=True,
    )

    add_colorbar_to_figure(figure=figure,
                           num_rows=varied_len,
                           num_cols=varied_len,
                           position=varied_len,
                           column_name=measured_name,
                           label=formatted_labels_dict[measured_name],
                           dataframe=data,
                           y_pos_inner=0.05,
                           span=True)

    return figure


def create_parameter_progress_plot(
        data: pd.DataFrame,
        col_names: List[str],
        x_axis_column: str,
        labels: Dict[str, str] = None,
        units: Dict[str, str] = None,
        title: str = 'progress_plot',
        figsize: Tuple[float, float] = (11, 8.5),
        fig_args: Dict = None,
        ax_args: Dict = None,
        scatter_args: Dict = None,
        bar_args: Dict = None,
        y_label_rotation=75,
):
    """
    :param data: A dataframe containing the data.
    :param col_names: The column names of the dataframe to use to create
    the relevant axis.
    :param measured_name: The column name to use to create the color values.
    :param labels: A dictionary keyed by the values from col_names.  The
    values from this dict will be used to create the axis labels.
    :param units: A dictionary keyed by the values from col_names.
    The values from this dict will be used to create the axis labels.
    :param title: title of the plot.
    :param x_axis_label: The label for the x-axis.
    :param figsize: the size of the figure in inches.
    :param fig_args: A dict of kwargs to pass to the figure.
    :param ax_args: A dict of kwargs to pass to every axes.
    :param scatter_args: A dict of kwargs to pass to the scatter axes.
    :param bar_args: A dict of kwargs to pass to the bar.
    :return: The figure
    """

    # Convert the kwargs
    fig_args = proc_passed_kwargs(fig_args)
    ax_args = proc_passed_kwargs(ax_args)
    scatter_args = proc_passed_kwargs(scatter_args)
    bar_args = proc_passed_kwargs(bar_args)

    # Create the figure
    figure: plt.Figure = plt.figure(
        figsize=figsize,
        **fig_args,
    )

    # Remove spacing between subplots
    figure.subplots_adjust(
        wspace=0,
        hspace=0,
        top=0.95,
        bottom=0.05,
    )

    figure.suptitle(title)

    # Get the number of varied parameters
    param_count = len(col_names)
    to_label = copy.deepcopy(col_names)

    # Generate the formatted labels
    formatted_labels = format_labels(
        keys=to_label,
        labels=labels,
        label_units=units
    )

    # get the values to use for the x axis; should be a list of unique sample numbers
    x_values = data[x_axis_column]

    color_list = ['steelblue', 'lightblue']
    new_color_list = []
    for i in range(len(x_values) // len(color_list)):
        new_color_list = new_color_list + color_list

    new_color_list = new_color_list[:len(x_values)]

    # see if there's a label for the x axis, if not, use the column header
    if labels:
        if x_axis_column in labels.keys():
            x_axis_label = labels[x_axis_column]
        else:
            x_axis_label = x_axis_column
    else:
        x_axis_label = x_axis_column

    # For each varied param
    for count in range(param_count):

        # Create the axis
        ax: plt.Axes = figure.add_subplot(param_count, 1, count + 1)

        # add title to first axis
        if count == 0:
            add_title_to_axes(ax, title)

        # Plot the varied parameter
        ax.bar(
            x_values,
            data[col_names[count]],
            **scatter_args,
            color=new_color_list
        )

        # Label x-axis
        ax.set_ylabel(f"{formatted_labels[count]}", rotation=y_label_rotation, labelpad=20)
        ax.get_yaxis().set_label_coords(-0.1, 0.5)

        # If not the last axis
        if count + 1 != param_count:

            # Remove x-axis
            ax.axes.get_xaxis().set_ticks([])

        # Else, if last row
        else:

            # Label x-axis
            ax.set_xlabel(x_axis_label)

    return figure


def create_parallel_coordinate_plot(
        data: pd.DataFrame,
        varied_col_names: List[str],
        measured_name: str,
        labels: Dict[str, str] = None,
        units: Dict[str, str] = None,
        title: str = 'parallel_coordinate_plot',
        figsize: Tuple[float, float] = (11, 8.5),
        fig_args: Dict = None,
        ax_args: Dict = None,
        beizer_patch_args: Dict = None,

):
    """
    Create a parallel coordinate plot.

    :param data: A dataframe containing the data.
    :param varied_col_names: The column names of the dataframe to use to create
    the relevant axis.
    :param measured_name: The column name to use to create the color values.
    :param labels: A dictionary keyed by the values from varied_col_names.  The
    values from this dict will be used to create the axis labels.
    :param units: A dictionary keyed by the values from varied_col_names.
    The values from this dict will be used to create the axis labels.
    :param title: title of the plot.
    :param figsize: the size of the figure in inches.
    :param fig_args: A dict of kwargs to pass to the figure.
    :param ax_args: A dict of kwargs to pass to every axes.
    :param beizer_patch_args: A dict of kwargs to pass to the beizer patches.
    :return: The figure.
    """

    # Convert the kwargs
    fig_args = proc_passed_kwargs(fig_args)
    ax_args = proc_passed_kwargs(ax_args)
    beizer_patch_args = proc_passed_kwargs(beizer_patch_args)

    # Create the figure and axes
    figure: plt.Figure = plt.figure(
        figsize=figsize,
        **fig_args,
    )

    # add title
    figure.suptitle(title)

    ax: plt.Axes = plt.subplot2grid((1, 20), (0, 0), **ax_args, colspan=19, fig=figure)

    # add title in axis
    add_title_to_axes(ax, title)
    plt.box(False)

    # Create a dataframe that only contains the columns that are varied values
    varied_values_df = data[varied_col_names]

    # Get the color values
    colors = values_to_color_list(
        values=data[measured_name],
        color_map_name='viridis'
    )

    # Generate the formatted labels
    formatted_labels = format_labels(
        keys=varied_col_names,
        labels=labels,
        label_units=units
    )

    # Normalize this dataframe to be from 0 to 1.
    varied_values_df_norm = at.normalize_dataframe(varied_values_df)

    # Get the number of rows passed in the dataframe
    row_count = len(varied_values_df_norm)

    # Get the number of varied parameters
    varied_param_count = len(varied_col_names)

    # Get a list of increasing integers to use for the x axis positions
    x_values = list(range(varied_param_count))

    # For every row in the dataframe
    for row_pos in range(row_count):

        # Get a list of the normalized varied values for plotting
        y_values = varied_values_df_norm.iloc[row_pos].values

        # For all the varied params, save the last one
        for x_value in x_values[:-1]:
            # Get a Beizer curve connecting this point and the next.
            beizer_path = two_points_to_parallel_beizer(
                x0=(x_value, y_values[x_value]),
                x1=(x_value + 1, y_values[x_value + 1]),
            )

            # Convert the path into a patch for plotting
            beizer_patch = patches.PathPatch(
                path=beizer_path,
                facecolor='none',
                linewidth=1.5,
                edgecolor=colors[row_pos],
                **beizer_patch_args
            )

            # Add to axes
            ax.add_patch(beizer_patch)

    # Formatting
    # For each varied param
    for count, varied_param in enumerate(varied_col_names):
        # Add vertical line
        ax.plot(
            [varied_param, varied_param],
            [0, 1],
            c='grey',
            linewidth=2,
        )

        # Add min max value to vertical lines
        column = data[varied_param]

        # Get the min and max values as strings
        min_val = min(column)
        max_val = max(column)
        min_str = str(round(min_val, 3))
        max_str = str(round(max_val, 3))

        # Add the min and max values to the plot
        ax.text(
            count,
            -0.10,
            min_str,
            horizontalalignment='center',
            color='grey',
        )

        # Add the min and max values to the plot
        ax.text(
            count,
            1.05,
            max_str,
            horizontalalignment='center',
            color='grey',
        )

        # Add the x axis labels
        ax.set_xticks(x_values)
        ax.set_xticklabels(formatted_labels, rotation=20)

    # Format x-axis
    ax.set_xlim(0, varied_param_count - 1)

    # Set yrange
    ax.set_ylim(-0.2, 1.2)

    # Remove axis marks
    ax.tick_params(
        top=False,
        bottom=False,
        left=False,
        right=False,
        labelleft=False,
        labelbottom=True
    )

    formatted_labels_dict = format_labels(
        keys=[measured_name],
        labels=labels,
        label_units=units,
        dictionary=True
    )

    add_colorbar_to_figure(figure=figure,
                           num_cols=20,
                           num_rows=1,
                           position=20,
                           dataframe=data,
                           column_name=measured_name,
                           label=formatted_labels_dict[measured_name],
                           x_pos_inner=0.25,
                           y_pos_inner=0.13,
                           width=0.5,
                           height=0.73,
                           label_position='right',
                           )

    plt.gcf().subplots_adjust(bottom=0.15)

    return figure


def create_voltage_current_plot(
        data: pd.DataFrame,
        varied_params: dict,
        varied_parameter_column_name: str,
        figsize: tuple = (11, 8.5),
        labels=None,
        units=None,
        title=None,
        dpi: int = 100):
    formatted_labels = format_labels(
        keys=[varied_parameter_column_name],
        labels=labels,
        label_units=units,
        dictionary=True
    )

    if title is None:
        title = f"Current vs. Potential by Position: {formatted_labels[varied_parameter_column_name]}"

    # Create a figure object
    figure = plt.figure(figsize=figsize, dpi=dpi)  # figsize, dpi args

    # Create figure axes
    axes: plt.Axes = plt.subplot2grid((1, 20), (0, 0), colspan=19, fig=figure)

    # add title in axis
    add_title_to_axes(axes, title)

    # Get the number of unique samples
    samples = data['sample'].unique()

    # Get the total number of possible mol ratios for the colormap
    viridis = cm.get_cmap('viridis', len(data[varied_parameter_column_name].unique()))

    for sample in samples:

        # Subset dataframe by sample
        sample_df = data[data['sample'] == sample]

        # Get the unique positions from the dataframe. Linear model will be fit by position.
        positions = sample_df['position'].unique()

        for position in positions:

            df_positions = sample_df[sample_df['position'] == position]

            if len(df_positions) < 2:

                axes.scatter(
                    df_positions['Current'],
                    df_positions['Voltage'],
                    color="black",
                )

            else:

                color = viridis(df_positions[varied_parameter_column_name])

                m, b = np.polyfit(df_positions['Current'], df_positions['Voltage'], 1)

                df_positions['y_i'] = df_positions['Current'] * m + b

                axes.scatter(
                    df_positions['Current'],
                    df_positions['Voltage'],
                    color=color,
                )

                axes.plot(
                    df_positions['Current'],
                    df_positions['y_i'],
                    color=color[0],
                )

    # Set axes labels
    axes.set_ylabel("Potential (Voltage)")
    axes.set_xlabel("Current (Amps)")

    # Get x limits
    xmin, xmax = at.calc_plot_min_max_from_range(data['Current'], buffer=0.1)
    axes.set_xlim(xmin, xmax)

    # Get y limits
    ymin, ymax = at.calc_plot_min_max_from_range(data['Voltage'], buffer=0.1)
    axes.set_ylim(ymin, ymax)

    # Set title
    plt.title(title)

    add_colorbar_to_figure(figure=figure,
                           num_cols=20,
                           num_rows=1,
                           position=20,
                           dataframe=data,
                           column_name=varied_parameter_column_name,
                           label=formatted_labels[varied_parameter_column_name],
                           x_pos_inner=0.25,
                           y_pos_inner=0.0,
                           width=0.5,
                           height=1.0,
                           label_position='right',
                           )

    return figure


def create_conductance_ratio_position_plot(
        data: pd.DataFrame,
        slope: str,
        varied_params: dict,
        varied_parameter_column_name: str,
        labels=None,
        units=None,
        figsize: tuple = (11, 8.5),
        title=None,
        dpi: int = 100):
    formatted_labels = format_labels(
        keys=[varied_parameter_column_name],
        labels=labels,
        label_units=units,
        dictionary=True
    )

    if title is None:
        title = f"Conductance vs. {formatted_labels[varied_parameter_column_name]} by Position."

    # Create a figure object
    figure = plt.figure(figsize=figsize, dpi=dpi)  # figsize, dpi args

    # Create figure axes
    axes: plt.Axes = plt.subplot2grid((1, 20), (0, 0), colspan=19, fig=figure)

    # add title to axis
    add_title_to_axes(axes, title)

    # Get the total number of possible mol ratios for the colormap
    viridis = cm.get_cmap('viridis', len(data['position'].unique()))

    # Colour by position
    color = viridis(data['position'])

    axes.scatter(
        data[varied_parameter_column_name],
        data[slope],
        color=color,
    )

    # Set axes labels
    axes.set_ylabel("Conductance (Siemens)")
    axes.set_xlabel(formatted_labels[varied_parameter_column_name])

    # Set title
    plt.title(title)

    # Get x limits
    xmin, xmax = at.calc_plot_min_max_from_range(data[varied_parameter_column_name], buffer=0.1)
    axes.set_xlim(xmin, xmax)

    # Get y limits
    ymin, ymax = at.calc_plot_min_max_from_range(data[slope], buffer=0.1)
    axes.set_ylim(ymin, ymax)

    add_colorbar_to_figure(figure=figure,
                           num_cols=20,
                           num_rows=1,
                           position=20,
                           dataframe=data,
                           column_name='position',
                           label='\nPosition',
                           x_pos_inner=0.25,
                           y_pos_inner=0.0,
                           width=0.5,
                           height=1.0,
                           label_position='right',
                           )

    return figure


def create_conductance_ratio_sample_plot(
        data: pd.DataFrame,
        mean_conductance_col_name: str,
        std_conductance_col_name: str,
        varied_params: dict,
        varied_parameter_column_name: str,
        standard_error: str,
        figsize: tuple = (11, 8.5),
        labels=None,
        units=None,
        title=None,
        dpi: int = 100):
    # Create a figure object
    figure = plt.figure(figsize=figsize, dpi=dpi)  # figsize, dpi args

    # Create figure axes
    axes: plt.Axes = plt.subplot2grid((1, 20), (0, 0), colspan=20, fig=figure)

    formatted_labels = format_labels(
        keys=[varied_parameter_column_name],
        labels=labels,
        label_units=units,
        dictionary=True
    )

    if title is None:
        title = f"Conductance vs. {formatted_labels[varied_parameter_column_name]} by Sample."

    # add title to axis
    add_title_to_axes(axes, title)

    axes.scatter(
        data[varied_parameter_column_name],
        data[mean_conductance_col_name],
    )

    axes.errorbar(
        data[varied_parameter_column_name],
        data[mean_conductance_col_name],
        yerr=data[std_conductance_col_name],
        linestyle="None",
    )

    # Set axes labels
    axes.set_ylabel("Conductance (Siemens)")

    axes.set_xlabel(formatted_labels[varied_parameter_column_name])

    # Set title
    plt.title(title)

    # Get x limits
    xmin, xmax = at.calc_plot_min_max_from_range(data[varied_parameter_column_name], buffer=0.1)
    axes.set_xlim(xmin, xmax)

    # Get y limits
    ymin, ymax = at.calc_plot_min_max_from_range(data[mean_conductance_col_name], buffer=0.1)
    axes.set_ylim(ymin, ymax)

    return figure


def plot_styled_line(
        ax,
        x,
        y,
        color='#ff2465',
        bg_thickness=5,
        fg_thickness=1.5,
        bg_kwargs=None,
        fg_kwargs=None,
):
    """
    Plot an array as a color-styled line with an inlay.
    :param ax: The matplotlib axis to plot the line on.
    :param x: The x data to plot.
    :param y: The y data to plot.
    :param color: Color for the line.
    :param bg_thickness: Thickness of the background line.
    :param fg_thickness: Thickness of the foreground line.
    :param bg_kwargs: Kwargs for the rear thick lines.
    :param fg_kwargs: Kwargs for the forward thin lines.
    :return: axes
    """

    # Convert the kwargs
    bg_kwargs = proc_passed_kwargs(bg_kwargs)
    fg_kwargs = proc_passed_kwargs(fg_kwargs)

    # Plot the rear, color line
    ax.plot(
        x,
        y,
        color=color,
        linewidth=bg_thickness,
        **bg_kwargs
    )

    # Plot the rear shading
    ax.plot(
        x,
        y,
        color='black',
        linewidth=bg_thickness,
        alpha=0.3,
        **bg_kwargs,
    )

    # Plot the inner line
    ax.plot(
        x,
        y,
        color=color,
        linewidth=fg_thickness,
        **fg_kwargs
    )


def plot_styled_scatter(
        ax,
        x,
        y,
        color='#ff2465',
        bg_size=25,
        fg_size=5,
        alpha=0.3,
        bg_kwargs=None,
        fg_kwargs=None,
):
    """
    Scatter an array as a color-styled line with an inlay.
    :param ax: The matplotlib axis to plot the scatter on.
    :param x: The x data to plot.
    :param y: The y data to plot.
    :param color: Color for the scatter.
    :param bg_thickness: Thickness of the background scatter.
    :param fg_thickness: Thickness of the foreground scatter.
    :param bg_kwargs: Kwargs for the rear thick scatters.
    :param fg_kwargs: Kwargs for the forward thin scatters.
    :return: axes
    """

    # Convert the kwargs
    bg_kwargs = proc_passed_kwargs(bg_kwargs)
    fg_kwargs = proc_passed_kwargs(fg_kwargs)

    # Plot the rear, color scatter
    ax.scatter(
        x,
        y,
        c=color,
        s=bg_size,
        **bg_kwargs,

    )

    # Plot the rear shading
    ax.scatter(
        x,
        y,
        c='black',
        s=bg_size,
        alpha=alpha,
        **bg_kwargs,

    )

    # Plot the inner scatter
    ax.scatter(
        x,
        y,
        color=color,
        s=fg_size,
        **fg_kwargs
    )

    pass


def create_voroni_plot(
        data: pd.DataFrame,
        ax_0: str,
        ax_1: str,
        measured_name: str,
        labels: Dict[str, str] = None,
        units: Dict[str, str] = None,
        title: str = 'voroni_plot',
        figsize: Tuple[float, float] = (11, 8.5),
        color_list: List = None,
        ax=None,
        fig_args: Dict = None,
        ax_args: Dict = None,
        color_args: Dict = None,
        poly_args: Dict = None,
        poly_line_args: Dict = None,
        scatter_args: Dict = None,
):
    """
    Create a Voroni figure.

    :param data: Dataframe to use.
    :param ax_0: Name of column for axis 0.
    :param ax_1: Name of column for axis 1.
    :param measured_name: Value to use for color shade.
    :param labels: Corresponding dict of labels.
    :param units: Corresponding dict of units.
    :param title: title of the plot.
    :param figsize: Figure size.
    :param color_list: List of colors to use for plotting.
    :param ax: Axis to create the figure on
    :param fig_args: Args to pass to figure.
    :param ax_args: Args to pass to axis.
    :param poly_args: Args to pass to polygons.
    :param poly_line_args: Args to pass to the polygon lines.
    :param scatter_args: Args to ass to scatter.
    :return: Matplotlib figure.
    """

    # Convert the kwargs
    fig_args = proc_passed_kwargs(fig_args)
    ax_args = proc_passed_kwargs(ax_args)
    color_args = proc_passed_kwargs(color_args)
    poly_args = proc_passed_kwargs(poly_args)
    poly_line_args = proc_passed_kwargs(poly_line_args)
    scatter_args = proc_passed_kwargs(scatter_args)

    # Generate the formatted labels
    formatted_labels = format_labels(
        keys=[ax_0, ax_1],
        labels=labels,
        label_units=units
    )

    # Generate polygons
    polygons = at.generate_voroni_regions(
        data=data,
        ax_0=ax_0,
        ax_1=ax_1,
    )

    # If the data_ax does not exist, make one
    if ax is None:
        # Create the figure and axis.
        figure: plt.Figure = plt.figure(
            figsize=figsize,
            **fig_args,
        )
        figure.suptitle(title)
        ax: plt.Axes = figure.add_subplot(1, 1, 1, **ax_args)

    # Generate the color mapping
    if color_list is None:
        colors = values_to_color_list(
            values=data[measured_name],
            **color_args
        )
    else:
        colors = color_list

    for polygon, color in zip(
            polygons,
            colors
    ):
        # Create the region
        ax.fill(
            *zip(*polygon),
            c=color,
            **poly_args
        )

    # Add the lines
    for polygon in polygons:
        # Plot the outlines
        ax.fill(
            *zip(*polygon),
            fill=False,
            **poly_line_args
        )

    # Plot white scatter points
    ax.scatter(
        data[ax_0],
        data[ax_1],
        zorder=2,
        **scatter_args
    )

    # Scale the axes
    ax_0_min, ax_0_max = at.get_plotting_bounds(data[ax_0])
    ax_1_min, ax_1_max = at.get_plotting_bounds(data[ax_1])
    ax.set_xlim(ax_0_min, ax_0_max)
    ax.set_ylim(ax_1_min, ax_1_max)

    # Label axes
    ax.set_xlabel(formatted_labels[0])
    ax.set_ylabel(formatted_labels[1])


def add_colorbar_to_figure(figure,
                           num_rows,
                           num_cols,
                           position,
                           dataframe,
                           column_name,
                           label=None,
                           x_pos_inner=1.0,
                           y_pos_inner=0.0,
                           width=0.05,
                           height=1,
                           label_position='left',
                           rounding=2,
                           span=False
                           ):
    """
    creates an axis at the designated index, then an inset axis with the proper proportions. then adds a colorbar to that axis.
    :param figure: figure to add colorbar to
    :param num_rows: number of rows for subplot
    :param num_cols: number of columns for subplot
    :param position: index for subplot
    :param dataframe: dataframe containing the data for the colorbar
    :param column_name: column name in dataframe
    :param label: label of colorbar
    :param x_pos_inner: x position of inset axis
    :param y_pos_inner: y position of inset axis
    :param width: width of inset axis
    :param height: height of inset axis
    :param label_position: position of the colorbar label, either 'left' or 'right'
    :param rounding: number of decimal places to round to
    :return: none
    """
    if label is None:
        label = column_name
    if span:
        ax: plt.Axes = plt.subplot2grid((num_rows, num_cols), (0, num_cols - 1), colspan=1, rowspan=num_rows - 1,
                                        fig=figure)
        ax2 = ax.inset_axes([x_pos_inner, y_pos_inner, width, height])
    else:
        ax = figure.add_subplot(num_rows, num_cols, position, frameon=False)
        ax2 = ax.inset_axes([x_pos_inner, y_pos_inner, width, height])

    ax.tick_params(top=False, bottom=False, left=False, right=False, labelleft=False, labelbottom=False)
    ax2.tick_params(top=False, bottom=False, left=False, right=False, labelleft=False, labelbottom=False)
    plt.box(False)

    minimum, maximum = at.calc_plot_min_max_from_range(dataframe[column_name], buffer=0)
    add_colorbar_to_axis(ax2, minimum=minimum, maximum=maximum, label=label, label_position=label_position)


def add_colorbar_to_axis(
        ax,
        colorscale='viridis',
        minimum=0,
        maximum=1,
        label='',
        label_position='left',
):
    """
    Converts the passes axis into a color bar.

    :param ax: The axis to add the color bar to.
    :param colorscale: The matplotlib color scale to use.
    :param min: The min label to put on the color bar.
    :param max: The max label to put on the color bar.
    :param label: The vertical label to put on the color bar.
    :return: The axis with the color bar added.
    """

    # Add the color scale
    bar = colorbar.ColorbarBase(
        ax,
        cmap=plt.get_cmap(colorscale),
        orientation='vertical',
        ticks=[0, 1],
    )

    if type(minimum) is int or type(minimum) is float:
        # format the numbers if they are very "small" or very "large"
        if (abs(minimum) > 1000 or abs(minimum) < 0.0001) and minimum != 0:
            minimum = format_e(Decimal(str(minimum)))
        else:
            minimum = round(minimum, 3)

    if type(maximum) is int or type(maximum) is float:
        if (abs(maximum) > 1000 or abs(maximum) < 0.0001) and maximum != 0:
            maximum = format_e(Decimal(str(maximum)))
        else:
            maximum = round(maximum, 3)

    # Relabel ticks
    bar.set_ticklabels([minimum, maximum])

    # Add label
    bar.set_label(label, labelpad=15)

    # Set the label position
    bar.ax.yaxis.set_label_position(label_position)
    if label_position == 'left':
        bar.ax.yaxis.tick_left()
    else:
        bar.ax.yaxis.tick_right()

    # Reduce the spacing between the label and the bar
    bar.ax.yaxis.labelpad = -15


def create_contour_plot(
        data: pd.DataFrame,
        ax_x0: str,
        ax_x1: str,
        ax_z: str,
        ax,
        levels: int = 20,
        cmap: str = 'viridis_r',
        labels: Dict[str, str] = None,
        units: Dict[str, str] = None,
        interp_mode: str = 'linear',
        density: int = 1000,
        buffer: float = 0.1,
):
    """
    Create a contour plot on an axis.
    :param data: The dataframe which contains the needed data.
    :param ax_x0: Name of the datafame column with x0 data.
    :param ax_x1: Name of the datafame column with x1 data.
    :param ax_z: Name of the datafame column with z data.
    :param ax: A matplotlib axis to plot the data on.
    :param levels: The number of contour levels to plot.
    :param cmap: The matplotlib colormap to use.
    :param labels: Dict of labels.
    :param units: Dict of units.
    :param interp_mode: 'linear' or 'cubic'.
    :param density: Density of interpolation.
    :param buffer: Buffer
    :return:
    """

    # Format the labels
    formatted_labels = format_labels(
        keys=[ax_x0, ax_x1],
        labels=labels,
        label_units=units
    )

    # Interpolate the data
    x0_data, x1_data, z_data = at.interpoloate_3d_data(
        data=data,
        x0=ax_x0,
        x1=ax_x1,
        z=ax_z,
        interp_mode=interp_mode,
        density=density,
        buffer=buffer,
    )

    # Smooth the z data
    # z_data = gaussian_filter(z_data, sigma=10, mode='constant', cval=1000)

    # Plot the outlines
    # ax.contour(
    #     x0_data,
    #     x1_data,
    #     z_data,
    #     levels=levels,
    #     colors='white',
    #     linewidths=0.5,
    #     zorder=2,
    # )

    conts = ax.contourf(
        x0_data,
        x1_data,
        z_data,
        levels=levels,
        cmap=cmap,
        # alpha=0.75,
        zorder=1
    )

    # Remove edge colors
    for cont in conts.collections:
        cont.set_edgecolor("face")

    # Label axes
    ax.set_xlabel(formatted_labels[0])
    ax.set_ylabel(formatted_labels[1])


def plot_optimization_results(
        emulator,
        optimizer,
        parameter,
        requested_params,
        optimizer_kwargs,
        domain_res=10000,
):
    """
    Given an emulator object and an optimization result, plot the results.

    :param emulator:
    :param optimization:
    :param parameter:
    :return:
    """

    reality_color = 'cornflowerblue'
    surrogate_color = 'orange'
    utility_color = 'mediumseagreen'

    # Get the parameter that should be plotted
    param = emulator.get_parameter(name=parameter)

    # Get the domain over which to sample
    domain = np.linspace(param.pmin, param.pmax, domain_res)

    # Sample the emulator to get reality
    reality = emulator.evaluate(deviation=False, precision=False, **{parameter: domain})

    # Sample the emulator to get the deviation
    reality_dev = emulator._evaluate_deviation(**{parameter: domain})

    # Get the utility function.  Rename the kwargs for UtilityFunciton
    util = UtilityFunction(
        kind=optimizer_kwargs['acq'],
        kappa=optimizer_kwargs['kappa'],
        xi=optimizer_kwargs['xi']
    )
    utility = util.utility(domain.reshape(-1, 1), optimizer._gp, 0)

    # Get next best util values
    best_util_domain = domain[np.argmax(utility)]
    best_util_utility = max(utility)

    # Get the observation domains and values
    observation_domain = optimizer.space.params.flatten()[:-1]
    observation_value = optimizer.space.target[:-1]

    # Sample the optimization for mean and deviation
    surrogate, surrogate_dev = optimizer._gp.predict(
        domain.reshape(-1, 1),
        return_std=True
    )

    # Reformat the requested params into a list
    requested_domain = np.array([params[parameter] for params in requested_params])

    # Begin plotting
    figure: plt.Figure = plt.figure(figsize=(9, 9), dpi=200)
    sampling_ax = plt.subplot2grid((10, 1), (0, 0), rowspan=7)
    utility_ax = plt.subplot2grid((10, 1), (7, 0), rowspan=3)

    # Plot reality
    sampling_ax.plot(
        domain,
        reality,
        c=reality_color,
        label='Reality'
    )

    sampling_ax.fill_between(
        domain,
        reality + reality_dev,
        reality - reality_dev,
        color=reality_color,
        zorder=0,
        alpha=0.3
    )

    # Plot the surrogate
    sampling_ax.plot(
        domain,
        surrogate,
        c=surrogate_color,
        label='Surrogate'
    )

    sampling_ax.fill_between(
        domain,
        surrogate + surrogate_dev,
        surrogate - surrogate_dev,
        color=surrogate_color,
        zorder=1,
        alpha=0.3
    )

    # Plot the samplings
    sampling_ax.scatter(
        observation_domain,
        observation_value,
        edgecolors=surrogate_color,
        zorder=10,
        c='white',
        label='Observation',
        linewidths=2,
    )

    # Plot the displacement lines
    for requested_domain_val, observation_domain_val, observation_value_val in zip(
            requested_domain, observation_domain, observation_value
    ):
        sampling_ax.plot(
            [requested_domain_val, observation_domain_val],
            [observation_value_val] * 2,
            zorder=9,
            color='grey',
            alpha=0.5,
        )

    # Plot vertical line / dot for last sampling
    sampling_ax.axvline(
        observation_domain[-1],
        color='lightgrey',
        zorder=0,
    )

    utility_ax.axvline(
        observation_domain[-1],
        color='lightgrey',
        zorder=0,
    )

    sampling_ax.scatter(
        observation_domain[-1],
        observation_value[-1],
        color=surrogate_color,
        zorder=10,
    )

    # Plot the utility function
    utility_ax.plot(
        domain,
        utility,
        c=utility_color,
    )

    # Plot next best guess lines
    utility_ax.axvline(
        x=best_util_domain,
        color='lightgrey',
        linestyle='--',

    )

    sampling_ax.axvline(
        x=best_util_domain,
        color='lightgrey',
        zorder=0,
        linestyle='--',
    )

    # Plot next best guess point
    utility_ax.scatter(
        best_util_domain,
        best_util_utility,
        zorder=10,
        c=utility_color,
        label='Next obs',
    )

    # Plot samplings on the util axis
    observations_idx = np.abs(domain - np.vstack(observation_domain)).argmin(axis=1)
    utility_ax.scatter(
        domain[observations_idx],
        utility[observations_idx],
        edgecolors=utility_color,
        zorder=10,
        c='white',
        label='Obs',
        linewidths=2,
    )
    #

    # Set the bounds based upon reality
    real_max = max(reality + reality_dev)
    real_min = min(reality - reality_dev)
    sampling_ax.set_xlim(at.get_plotting_bounds(domain))
    sampling_ax.set_ylim(at.get_plotting_bounds([real_min, real_max]))
    utility_ax.set_xlim(at.get_plotting_bounds(domain))

    # Format
    sampling_ax.legend(loc='upper right')
    utility_ax.legend(loc='upper right')
    sampling_ax.set_xlabel('')
    sampling_ax.set_xticks([])
    sampling_ax.set_ylabel('Response')
    utility_ax.set_xlabel('Parameter')
    utility_ax.set_yticks([])
    utility_ax.set_ylabel('Utility')
    figure.subplots_adjust(hspace=0)

    return figure


def df_dict_filter(df, d):
    """
    returns a copy of filtered df based on key/value columns providided in dictionary, d and a string for a label name
    :param df: dataframe to filter
    :param d: dictionary of key/value column filters
    :return: dataframe
    """
    ddf = df.copy()
    c = []
    for k, v in d.items():
        c.append(ddf[k] == v)
    # AND all the filters
    if len(c) > 0:
        ddf = ddf[reduce(np.logical_and, c)]

    return ddf


def get_saturation_bounds(df, x_col, column, threshold):
    """
    Get the saturation bounds with respect to x for a given dataframe
    :param df: dataframe
    :param col: column to calculate saturation
    :param threshold: threshold value
    :param x: column to provide bounds with respect to
    :return: list of x_col pairs
    """
    df = df.copy()
    # get boolean of saturated
    df['saturated'] = df[column] > ((2 ** 16) * threshold)
    # label groups of True with increasing number
    label, num_features = ndimage_label(df['saturated'])
    df['sat_label'] = label
    # group by the labels and take the first and last points
    result = df.loc[df['sat_label'] != 0].groupby('sat_label')[x_col].agg(
        ['first', 'last']).drop_duplicates()
    return result.values.tolist()


def generate_spectra_plot(
        data: pd.DataFrame,
        varied_col_name: str,
        x_col_name: str,
        y_col_name: str,
        y_col_groups: Dict,
        line_col_name: str,
        baseline_col_names: List[str] = None,
        baseline_col_groups: Dict = None,
        baseline_colours: List[str] = None,
        saturation: Dict = None,
        filter_key_value: Dict = None,
        labels: Dict[str, str] = None,
        units: Dict[str, str] = None,
        title: str = None,
        x_label: str = None,
        y_label: str = None,
        figsize: Tuple[float, float] = (11, 8.5),
        fig_args: Dict = None,
        ax_args: Dict = None,
        ax_settings: List = None,
        reverse_x_axis=False,
) -> plt.figure():
    """
    creates the raw spectra plots, which are usually x (nm) vs transmission or some other spectra property, colored by
    varied parameter. This plot has a line for each position which traces out what would normally be a scatter plot. The
    number of subplots is equal to the length of y_col_groups, x_col_names and y_col_names.
    :param data: the dataframe to get data from
    :param varied_col_name: the varied parameter to use as a color
    :param x_col_name: the x axis of the plot, lengths of x_col_names, y_col_names and y_col_groups must be equal
    :param y_col_name: the y axis column name used for each line, lengths of x_col_names, y_col_names and y_col_groups must be equal
    :param y_col_groups: the y axis column key/value filter pairs
    :param line_col_name: the column which determines the grouping of the data for each line, usually position number
    :param baseline_col_names: names of columns to plot independently of the y data
    :param baseline_col_groups: grouping filters for baseline columns
    :param baseline_colours: colours of baseline colours in order
    :param saturation: optional saturation information provide dictionary of {column: <>, threshold: <>, 'x_col': <>}
    :param filter_key_value: column name, column value pair to filter the whole dataset on e.g. {'position': 0}
    :param labels: a dictionary to map column headers to axis labels
    :param units: a dictionary to map column headers to units
    :param title: Figure title
    :param x_label: x label title
    :param y_label: y label title
    :param figsize: size of the figure
    :param fig_args: args for the figure
    :param ax_args: args for the axes
    :param ax_settings: call and set axes methods set_ylim, hline etc.
    :param reverse_x_axis: if true, reverse the direction of the x axis
    :return: a plt.figure() object
    """

    # Convert the kwargs
    fig_args = proc_passed_kwargs(fig_args)
    ax_args = proc_passed_kwargs(ax_args)

    # Check labels
    if labels is None:
        labels = {}

    if x_label is not None:
        labels[x_col_name] = x_label
    if y_label is not None:
        labels[y_col_name] = y_label

    # Generate the formatted labels
    formatted_labels = format_labels(
        keys=[x_col_name, y_col_name],
        labels=labels,
        label_units=units,
        dictionary=True
    )

    # Create the figure
    figure = plt.figure(
        figsize=figsize,
        **fig_args,
    )

    # Get the color values
    baseline_defaults = ['royalblue', 'tomato']
    data['colour_column'] = [tuple(x) for x in values_to_color_list(
        values=data[varied_col_name],
        color_map_name='viridis'
    )]

    # Filter the whole dataset
    data = df_dict_filter(data, filter_key_value)

    ax: plt.Axes = plt.subplot2grid((1, 20), (0, 0), colspan=19, fig=figure)

    # add title to axis
    add_title_to_axes(ax, title)

    # Filter data and create label
    f_data = df_dict_filter(data, y_col_groups)
    f_data = f_data.sort_values(by=[x_col_name])

    # warn on emtpy dataframe
    if f_data.empty:
        logger.warning(f"y_col_name dataframe is empty, no data will be plotted, check y_col_groups: `{y_col_groups}`")

    # Add baseline lines if required
    if baseline_col_names is not None:
        if baseline_colours is None:
            baseline_colours = [baseline_defaults[x % 2] for x in range(len(baseline_col_names))]

        # Add the baselines
        for b_col, b_colour in zip(baseline_col_names, baseline_colours):
            b_data = df_dict_filter(data, baseline_col_groups)

            # warn on emtpy dataframe
            if b_data.empty:
                logger.warning(
                    f"baseline dataframe is empty, no baselines will be plotted, check baseline_col_groups: `{baseline_col_groups}`")

            # group for plotting
            grps = b_data.groupby(line_col_name)
            # plot all the baselines
            for i, (ix, group) in enumerate(grps):
                label = b_col
                if 'baseline' not in b_col:
                    label = 'blank_slide'

                # don't duplicate legend entries
                if i > 0:
                    label = f"_{label}"

                ax.plot(
                    group[x_col_name],
                    group[b_col],
                    color=b_colour,
                    label=label,
                    zorder=10,
                    **ax_args
                )

    # Set the axes method options
    if ax_settings is not None:
        for setting in ax_settings:
            for name, dd in setting.items():
                args = dd.get('args', [])
                kwargs = dd.get('kwargs', {})
                getattr(ax, name)(*args, **kwargs)

    # Add saturation if required
    if saturation is not None:
        sat = get_saturation_bounds(f_data, x_col_name, saturation['column'], saturation['threshold'])
        i = 0
        for start, end in sat:
            if i == 0:
                ax.fill_between(
                    [start, end],
                    saturation["y_limit"],
                    color='lightgray',
                    edgecolor=None,
                    linewidth=0,
                    label="Spect. saturated region"
                )
                i = 1
            else:
                ax.fill_between(
                    [start, end],
                    saturation["y_limit"],
                    color='lightgray',
                    edgecolor=None,
                    linewidth=0,
                )

    ax.legend()

    # Plot the data
    for ix, group in f_data.groupby(line_col_name):
        c = group['colour_column'].tolist()[0]
        ax.plot(
            group[x_col_name],
            group[y_col_name],
            color=c,
            label=labels.get(y_col_name, y_col_name),
            **ax_args
        )

    # add title and labels
    figure.suptitle(formatted_labels.get(title, title))
    ax.set_xlabel(formatted_labels.get(x_label, x_label))
    ax.set_ylabel(formatted_labels.get(y_label, y_label))

    xmin, xmax = at.calc_plot_min_max_from_range(data[x_col_name], buffer=0)
    if reverse_x_axis:
        ax.set_xlim(xmax, xmin)
    else:
        ax.set_xlim(xmin, xmax)

    formatted_labels_dict = format_labels(
        keys=[varied_col_name],
        labels=labels,
        label_units=units,
        dictionary=True
    )

    add_colorbar_to_figure(figure=figure,
                           num_cols=20,
                           num_rows=1,
                           position=20,
                           dataframe=data,
                           column_name=varied_col_name,
                           label=formatted_labels_dict[varied_col_name],
                           x_pos_inner=0.25,
                           y_pos_inner=0,
                           width=0.5,
                           height=1,
                           label_position='right',
                           )

    return figure


def add_title_to_axes(
        axes,
        text
):
    """
    Add a title to a passed matplotlib axes.
    :param axes: The axes to add the title to.
    :param text: The text to add as the title.
    :return: None.
    """

    # Add the title to the axes
    axes.text(
        0.02,
        0.96,
        text,
        fontsize=9,
        horizontalalignment='left',
        verticalalignment='top',
        transform=axes.transAxes,
        alpha=0.5
    )


def scatter_plot(data: pd.DataFrame,
                 x_axis_col: str,
                 y_axis_col: str,
                 color_by_col: str,
                 x_axis_label: str,
                 y_axis_label: str,
                 title: str,
                 color_by_col_label: str = None,
                 sample: int = None,
                 edge_buffer: float = 0.1,
                 dpi: int = 100,
                 figsize: tuple = (11, 8.5)):
    # Create a figure object
    fig = plt.figure(figsize=figsize, dpi=dpi)  # figsize, dpi args

    # Create figure axes
    axes: plt.Axes = plt.subplot2grid((1, 20), (0, 0), colspan=19, fig=fig)

    # Get the total number of possible mol ratios for the colormap
    viridis = cm.get_cmap('viridis', len(data[color_by_col].unique()))

    # Colour by position
    color = viridis(data[color_by_col])

    axes.scatter(
        data[x_axis_col],
        data[y_axis_col],
        c=color
    )

    # Set axes labels
    axes.set_ylabel(str(y_axis_label).capitalize())
    axes.set_xlabel(str(x_axis_label).capitalize())

    # Get x limits
    xmin, xmax = at.calc_plot_min_max_from_range(data[x_axis_col], buffer=edge_buffer)
    axes.set_xlim(xmin, xmax)

    # Get y limits
    ymin, ymax = at.calc_plot_min_max_from_range(data[y_axis_col], buffer=edge_buffer)
    axes.set_ylim(ymin, ymax)

    plt.title(title)

    plt.grid(True)

    if color_by_col_label is None:
        add_colorbar_to_figure(figure=fig,
                               num_cols=20,
                               num_rows=1,
                               position=20,
                               dataframe=data,
                               column_name=color_by_col,
                               label=str(color_by_col).capitalize(),
                               x_pos_inner=0.25,
                               y_pos_inner=0.0,
                               width=0.5,
                               height=1,
                               label_position='right',
                               )
    else:
        add_colorbar_to_figure(figure=fig,
                               num_cols=20,
                               num_rows=1,
                               position=20,
                               dataframe=data,
                               column_name=color_by_col,
                               label=color_by_col_label,
                               x_pos_inner=0.25,
                               y_pos_inner=0.0,
                               width=0.5,
                               height=1,
                               label_position='right',
                               )

    return fig


def normal_distribution_plot(data: pd.DataFrame,
                             title: str,
                             col: str,
                             col_label: str,
                             sample: int = None,
                             dpi: int = 100,
                             figsize: tuple = (11, 8.5)):
    # Create a figure object
    figure = plt.figure(figsize=figsize, dpi=dpi)  # figsize, dpi args

    # Create figure axes
    axes: plt.Axes = plt.subplot2grid((1, 20), (0, 0), colspan=19, fig=figure)

    mean = np.mean(data[col])
    std = np.std(data[col])

    plt.grid()

    # fill area 3
    pos3 = mean + 3 * std
    plt.plot([pos3, pos3], [0.0, stats.norm.pdf(pos3, mean, std)], color='black')
    neg3 = mean - 3 * std
    plt.plot([neg3, neg3], [0.0, stats.norm.pdf(neg3, mean, std)], color='black')
    ptx = np.linspace(pos3, neg3, 100)
    pty = stats.norm.pdf(ptx, mean, std)
    axes.fill_between(ptx, pty, color='#89bedc', alpha='1.0')

    # fill area 2
    pos2 = mean + 2 * std
    plt.plot([pos2, pos2], [0.0, stats.norm.pdf(pos2, mean, std)], color='black')
    neg2 = mean - 2 * std
    plt.plot([neg2, neg2], [0.0, stats.norm.pdf(neg2, mean, std)], color='black')
    ptx = np.linspace(pos2, neg2, 100)
    pty = stats.norm.pdf(ptx, mean, std)
    axes.fill_between(ptx, pty, color='#539ecd', alpha='1.0')

    # fill area 1
    pos1 = mean + std
    plt.plot([pos1, pos1], [0.0, stats.norm.pdf(pos1, mean, std)], color='black')
    neg1 = mean - std
    plt.plot([neg1, neg1], [0.0, stats.norm.pdf(neg1, mean, std)], color='black')
    ptx = np.linspace(pos1, neg1, 100)
    pty = stats.norm.pdf(ptx, mean, std)
    axes.fill_between(ptx, pty, color='#0b559f', alpha='1.0')

    # Top line
    x = np.linspace(neg3, pos3, 100)
    y = stats.norm.pdf(x, mean, std)
    axes.plot(x, y, color='black')

    # Custom legend
    std1 = mpatches.Patch(color='#0b559f', label='0-1 STD from Mean')
    std2 = mpatches.Patch(color='#539ecd', label='1-2 STDs from Mean')
    std3 = mpatches.Patch(color='#89bedc', label='2-3 STDs from Mean')
    plt.legend(handles=[std1, std2, std3])

    # Vertical mean line
    axes.axvline(x=mean,
                 color="black",
                 linestyle="--")

    axes.text(mean,
              max(y) * 1.1,
              f'   Mean: {mean}   \n   STD: {std}   \n   Observations: {len(data[col])}   ',
              horizontalalignment='right',
              verticalalignment='center')

    plt.title(title)

    plt.xlabel(str(col_label).capitalize())
    plt.ylabel('Probability Density')

    plt.ylim(0, max(y) * 1.2)

    return figure


def beta_distribution_plot(data: pd.DataFrame,
                           title: str,
                           col: str,
                           col_label: str,
                           sample: int = None,
                           dpi: int = 100,
                           figsize: tuple = (11, 8.5)):
    # Create a figure object
    figure = plt.figure(figsize=figsize, dpi=dpi)  # figsize, dpi args

    # Create figure axes
    axes: plt.Axes = plt.subplot2grid((1, 20), (0, 0), colspan=19, fig=figure)

    alpha, beta, loc, scale = stats.beta.fit(data=data[col], floc=0)

    x = np.arange(0.01, 1, 0.01)
    y = stats.beta.pdf(x, alpha, beta)

    axes.fill_between(x, y, color='#0b559f', alpha='1.0')
    axes.plot(x, y, color="black")

    axes.text(0.5,
              max(y) * 1.1,
              f'   Alpha: {alpha}   \n   Beta: {beta}   \n   Observations: {len(data[col])}   ',
              horizontalalignment='center',
              verticalalignment='center')

    plt.grid()

    plt.title(title)

    plt.xlabel(str(col_label).capitalize())
    plt.ylabel('Probability Density')

    plt.ylim(0, max(y) * 1.2)

    return figure


def plot_image(image_path,
               title='',
               dpi: int = 100,
               figsize: tuple = (11, 8.5),
               **annotations):
    # Read in the image
    img = mpimg.imread(image_path)

    bottom_fig = img.shape[1]

    # Create a figure object
    figure = plt.figure(figsize=figsize, dpi=dpi)  # figsize, dpi args

    # Create figure axes
    axes: plt.Axes = plt.subplot2grid((1, 20), (0, 0), colspan=19, fig=figure)

    axes.imshow(img)

    text_list = []
    for key, value in annotations.items():
        text = f"{key}: {value}"
        text_list.append(text)

    combined_text = "\n".join(text_list)

    axes.annotate(combined_text,
                  (0, 0),
                  (0, -20),
                  xycoords='axes fraction',
                  textcoords='offset points',
                  va='top'
                  )

    plt.title(title)

    plt.xticks([])
    plt.yticks([])

    return figure


def plot_dual_image(
        image_1_path,
        image_2_path,
        image_1_title=None,
        image_2_title=None,
        title='',
        dpi: int = 200,
        figsize: tuple = (20, 8.5),
        **annotations):
    # Read in the image
    img1 = mpimg.imread(image_1_path)
    img2 = mpimg.imread(image_2_path)

    figure, axes = plt.subplots(nrows=1, ncols=2, figsize=figsize, dpi=dpi)

    axes[0].imshow(img1)
    axes[0].set_title(image_1_title)
    axes[0].set_xticks([])
    axes[0].set_yticks([])

    axes[1].imshow(img2)
    axes[1].set_title(image_2_title)
    text_list = []
    for key, value in annotations.items():
        text = f"{key}: {value}"
        text_list.append(text)

    combined_text = "\n".join(text_list)

    axes[1].annotate(combined_text,
                     (0, 0),
                     (0, -20),
                     xycoords='axes fraction',
                     textcoords='offset points',
                     va='top'
                     )
    axes[1].set_xticks([])
    axes[1].set_yticks([])

    figure.suptitle(title, fontsize=16)

    return figure


def generate_spectra_plot_validation(
        data: pd.DataFrame,
        x_col_name: str,
        y_col_name: str,
        baseline_col_names: List[str] = None,
        baseline_colours: List[str] = None,
        labels: Dict[str, str] = None,
        units: Dict[str, str] = None,
        title: str = None,
        x_label: str = None,
        y_label: str = None,
        figsize: Tuple[float, float] = (11, 8.5),
        fig_args: Dict = None,
        ax_args: Dict = None,
        ax_settings: List = None,
        fail_indecies: list = None,
) -> plt.figure():
    """
    creates the raw spectra plots, which are usually x (nm) vs transmission or some other spectra property, colored by
    varied parameter. This plot has a line for each position which traces out what would normally be a scatter plot. The
    number of subplots is equal to the length of y_col_groups, x_col_names and y_col_names.
    :param data: the dataframe to get data from
    :param x_col_name: the x axis of the plot, lengths of x_col_names, y_col_names and y_col_groups must be equal
    :param y_col_name: the y axis column name used for each line, lengths of x_col_names, y_col_names and y_col_groups must be equal
    :param baseline_col_names: names of columns to plot independently of the y data
    :param baseline_colours: colours of baseline colours in order
    :param labels: a dictionary to map column headers to axis labels
    :param units: a dictionary to map column headers to units
    :param title: Figure title
    :param x_label: x label title
    :param y_label: y label title
    :param figsize: size of the figure
    :param fig_args: args for the figure
    :param ax_args: args for the axes
    :param ax_settings: call and set axes methods set_ylim, hline etc.
    :return: a plt.figure() object
    """

    # Convert the kwargs
    fig_args = proc_passed_kwargs(fig_args)
    ax_args = proc_passed_kwargs(ax_args)

    # Check labels
    if labels is None:
        labels = {}

    if x_label is not None:
        labels[x_col_name] = x_label
    if y_label is not None:
        labels[y_col_name] = y_label

    # Generate the formatted labels
    formatted_labels = format_labels(
        keys=[x_col_name, y_col_name],
        labels=labels,
        label_units=units,
        dictionary=True
    )

    # Create the figure
    figure = plt.figure(
        figsize=figsize,
        **fig_args,
    )

    # Get the color values
    baseline_defaults = ['royalblue', 'tomato']

    ax: plt.Axes = plt.subplot2grid((1, 20), (0, 0), colspan=19, fig=figure)

    # Add baseline lines if required
    if baseline_col_names is not None:
        if baseline_colours is None:
            baseline_colours = [baseline_defaults[x % 2] for x in range(len(baseline_col_names))]

        # Add the baselines
        for b_col, b_colour in zip(baseline_col_names, baseline_colours):
            # take lowest line_col_name to plot baselines by default

            label = b_col
            if 'baseline' not in b_col:
                label = 'blank_slide'

            ax.plot(
                data[x_col_name],
                data[b_col],
                color=b_colour,
                label=label,
                **ax_args
            )

    # Set the axes method options
    if ax_settings is not None:
        for setting in ax_settings:
            for name, dd in setting.items():
                args = dd.get('args', [])
                kwargs = dd.get('kwargs', {})
                if name in ['set_xlabel', 'set_ylabel']:
                    for arg in args:
                        arg = formatted_labels.get(arg, arg)
                else:
                    getattr(ax, name)(*args, **kwargs)

    # Add saturation if required
    if fail_indecies is not None:
        i = 0
        for start, stop in fail_indecies:
            if i == 0:
                ax.fill_between(
                    [data[x_col_name][start], data[x_col_name][stop]],
                    2 ** 16,
                    color='mistyrose',
                    edgecolor=None,
                    linewidth=0,
                    label="Failure region"
                )
                i = 1
            else:
                ax.fill_between(
                    [data[x_col_name][start], data[x_col_name][stop]],
                    2 ** 16,
                    color='mistyrose',
                    edgecolor=None,
                    linewidth=0,
                )

    # Plot the data
    ax.plot(
        data[x_col_name],
        data[y_col_name],
        label=y_col_name,
        color='forestgreen',
        **ax_args
    )

    ax.legend()

    # add title and labels
    figure.suptitle(formatted_labels.get(title, title))
    ax.set_xlabel(formatted_labels.get(x_label, x_label))
    ax.set_ylabel(formatted_labels.get(y_label, y_label))

    return figure


def conductance_plot_v2(
        conductivity_data,
        title,
        varied_param,
        edge_buffer: float = 0.1,
        dpi: int = 100,
        figsize: Tuple[float, float] = (11, 8.5)):
    # Create a figure object
    figure = plt.figure(figsize=figsize, dpi=dpi)  # figsize, dpi args

    # Create figure axes
    axes: plt.Axes = plt.subplot2grid((1, 20), (0, 0), colspan=19, fig=figure)

    plt.title(title)
    plt.grid(True)

    # Set axes labels
    axes.set_ylabel("Potential (Volts)")
    axes.set_xlabel("Current (Amps)")

    # Remove saturated
    valid_df = cond.remove_saturated_points(conductivity_data)

    if len(valid_df) <= cond.number_valid_observations_lm_threshold:

        # Get x limits
        xmin, xmax = at.calc_plot_min_max_from_range(conductivity_data["Current"], buffer=edge_buffer)
        axes.set_xlim(xmin, xmax)

        # Get y limits
        ymin, ymax = at.calc_plot_min_max_from_range(conductivity_data["Voltage"], buffer=edge_buffer)
        axes.set_ylim(ymin, ymax)

        viridis = cm.get_cmap('viridis', len(conductivity_data[varied_param].unique()))

        # Plot invalid points
        axes.scatter(
            conductivity_data['Current'],
            conductivity_data['Voltage'],
            c=conductivity_data[varied_param],
            cmap=viridis,
            s=80,
            facecolors='none',
            edgecolors='red'
        )

        try:
            axes.scatter(
                valid_df['Current'],
                valid_df['Voltage'],
                c=valid_df[varied_param],
                cmap=viridis,
                s=80,
                facecolors='none',
                edgecolors='black'
            )
        except:
            pass

        add_colorbar_to_figure(figure=figure,
                               num_cols=20,
                               num_rows=1,
                               position=20,
                               dataframe=conductivity_data,
                               column_name=varied_param,
                               label=varied_param,
                               x_pos_inner=0.25,
                               y_pos_inner=0.0,
                               width=0.5,
                               height=1,
                               label_position='right',
                               )

        return figure

    else:
        # Get x limits
        xmin, xmax = at.calc_plot_min_max_from_range(valid_df["Current"], buffer=edge_buffer)
        axes.set_xlim(xmin, xmax)

        # Get y limits
        ymin, ymax = at.calc_plot_min_max_from_range(valid_df["Voltage"], buffer=edge_buffer)
        axes.set_ylim(ymin, ymax)

        r2, slope, intercept = cond.linear_regression_RANSAC(data=valid_df, x_axis='Current', y_axis='Voltage')

        if slope <= 0:

            viridis = cm.get_cmap('viridis', len(valid_df[varied_param].unique()))

            # Plot invalid points
            axes.scatter(
                valid_df['Current'],
                valid_df['Voltage'],
                c=valid_df[varied_param],
                cmap=viridis,
                s=80,
                facecolors='none',
                edgecolors='red'
            )

            add_colorbar_to_figure(figure=figure,
                                   num_cols=20,
                                   num_rows=1,
                                   position=20,
                                   dataframe=valid_df,
                                   column_name=varied_param,
                                   label=varied_param,
                                   x_pos_inner=0.25,
                                   y_pos_inner=0.0,
                                   width=0.5,
                                   height=1,
                                   label_position='right',
                                   )

            return figure

        else:

            viridis = cm.get_cmap('viridis', len(valid_df[varied_param].unique()))

            # Plot invalid points
            axes.scatter(
                valid_df['Current'],
                valid_df['Voltage'],
                c=valid_df[varied_param],
                cmap=viridis
            )

            add_colorbar_to_figure(figure=figure,
                                   num_cols=20,
                                   num_rows=1,
                                   position=20,
                                   dataframe=valid_df,
                                   column_name=varied_param,
                                   label=varied_param,
                                   x_pos_inner=0.25,
                                   y_pos_inner=0.0,
                                   width=0.5,
                                   height=1,
                                   label_position='right',
                                   )

            # Plot linear models
            valid_df['y_i'] = valid_df['Current'] * slope + intercept

            axes.plot(
                valid_df['Current'],
                valid_df['y_i'],
                c='black'
            )

            return figure


def conductance_plot(conductivity_data,
                     varied_param,
                     title,
                     edge_buffer: float = 0.1,
                     dpi: int = 100,
                     figsize: Tuple[float, float] = (11, 8.5),
                     individual_pos=False):
    # Create a figure object
    figure = plt.figure(figsize=figsize, dpi=dpi)  # figsize, dpi args

    # Create figure axes
    axes: plt.Axes = plt.subplot2grid((1, 20), (0, 0), colspan=19, fig=figure)

    outlier_df = cond.flag_outliers(conductivity_data)

    varied_col = varied_param[list(varied_param.keys())[0]]['col_name']
    varied_unit = varied_param[list(varied_param.keys())[0]]['units']

    filtered_df = outlier_df.filter(['sample', 'Current', 'Voltage', 'outlier', varied_col])

    valid_df = filtered_df[filtered_df['outlier'] == 0]

    # Get the total number of possible mol ratios for the colormap
    viridis = cm.get_cmap('viridis', len(valid_df[varied_col].unique()))

    # Plot valid points
    axes.scatter(
        valid_df['Current'],
        valid_df['Voltage'],
        c=valid_df[varied_col],
        cmap=viridis,
        alpha=0.6
    )

    # Set axes labels
    axes.set_ylabel("Potential (Volts)")
    axes.set_xlabel("Current (Amps)")

    # Get x limits
    xmin, xmax = at.calc_plot_min_max_from_range(valid_df["Current"], buffer=edge_buffer)
    axes.set_xlim(xmin, xmax)

    # Get y limits
    ymin, ymax = at.calc_plot_min_max_from_range(valid_df["Voltage"], buffer=edge_buffer)
    axes.set_ylim(ymin, ymax)

    text_list = []

    text = "Measurements:"
    text_list.append(text)

    for sample in valid_df['sample'].unique():

        sample_df = valid_df[valid_df['sample'] == sample]

        r2, slope, intercept = cond.linear_regression_RANSAC(data=sample_df, x_axis='Current', y_axis='Voltage')

        if individual_pos:
            text = f'R2: {r2}, Conductance: {(1 / slope) * 10 ** 9} nS'
            text_list.append(text)

        if r2 == 0 and slope == 0 and intercept == 0:
            logger.warning(f"Conductivity for sample {sample} is 0.")

            # Plot invalid points
            axes.scatter(
                sample_df['Current'],
                sample_df['Voltage'],
                s=80,
                facecolors='none',
                edgecolors='red'
            )

            text = f"Not enough points to generate conductance for sample {sample} - red circle"
            text_list.append(text)

        else:

            if slope < 0:

                # Plot invalid points
                axes.scatter(
                    sample_df['Current'],
                    sample_df['Voltage'],
                    s=80,
                    facecolors='none',
                    edgecolors='black'
                )

                text = f"Negative slope recorded for sample {sample} - black circle"
                text_list.append(text)

            else:

                # Plot linear models
                sample_df['y_i'] = sample_df['Current'] * slope + intercept

                axes.plot(
                    sample_df['Current'],
                    sample_df['y_i'],
                    c='black'
                )

    combined_text = "\n".join(text_list)
    axes.annotate(combined_text,
                  (0, 0),
                  (0, -50),
                  xycoords='axes fraction',
                  textcoords='offset points',
                  va='top'
                  )

    plt.title(title)

    figure.set_figheight(8.5 + len(text_list) * 1)

    plt.grid(True)

    add_colorbar_to_figure(figure=figure,
                           num_cols=20,
                           num_rows=1,
                           position=20,
                           dataframe=valid_df,
                           column_name=varied_col,
                           label=f"{str(varied_col)} ({varied_unit})",
                           x_pos_inner=0.25,
                           y_pos_inner=0.0,
                           width=0.5,
                           height=1,
                           label_position='right',
                           )

    return figure


if __name__ == '__main__':
    processing_param_dir = '/Users/teddyhaley/PycharmProjects/data_processing/test_data/results_data/2019-08-22_10-47-11/processing_parameters.json'
    with open(processing_param_dir, 'r') as json_file:
        paramater_dict = json.load(json_file)

    conductivity_dir = paramater_dict['raw_data']['conductivity']
    conductivity_df = pd.read_csv(conductivity_dir)

    sample_dir = paramater_dict['aggregate_data']['sample']
    sample_df = pd.read_csv(sample_dir)

    combined_df = pd.merge(conductivity_df, sample_df, on="sample", how='inner')

    title = "something"

    fig = conductance_plot_v2(
        conductivity_data=combined_df,
        title=title,
        varied_param='sample'
    )
    plt.show()

    # data_good = {
    #     'Current': [0, 1, 2, 3, 4, 5, 6, 7, 8],
    #     'Voltage': [0, 1, 2, 3, 4, 5, 6, 7, 8],
    #     'Position': [0, 1, 2, 3, 4, 5, 6, 7, 8],
    #     'Conductance': [2.912655e-10, 3.245884e-10, 3.249483e-10, 2.997544e-10, 2.903846e-10, 2.663400e-10,
    #                     2.229549e-10, 2.663400e-10, 2.229549e-10],
    #     'R2': [1, 1, 1, 0.99, 0.90, 0.80, 0.5, 0.2, 0]
    # }
    #
    # df_good = pd.DataFrame(data_good, columns=data_good.keys())
    #
    # fig = scatter_plot(data=df_good,
    #                    x_axis_col="Current",
    #                    y_axis_col="Voltage",
    #                    color_by_col="Position",
    #                    x_axis_label="x",
    #                    y_axis_label="y",
    #                    title="Something")

    # normal_distribution_plot(data=df_good,
    #                          col="Conductance",
    #                          col_label="Conductance")

    # beta_distribution_plot(data=df_good,
    #                        col="R2",
    #                        col_label="R - Squared")

    # image_path = "/Users/teddyhaley/Desktop/2019-08-16_15-19-19/sample_000/spincoated_slide_after_annealing1.jpg"
    # plot_image(image_path=image_path, crack=5, dewet=5)
    #
    # plt.show()
    #
    # measured_params_file = "/Users/teddyhaley/Desktop/2019-08-22_10-47-11_measured_params.json"
    # varied_params_file = "/Users/teddyhaley/Desktop/2019-08-22_10-47-11_varied_params.json"
    # conductivity_file = "/Users/teddyhaley/Desktop/conductivity_df.csv"
    #
    # with open(measured_params_file, 'r') as f:
    #     measured_params = json.load(f)
    #
    # with open(varied_params_file, 'r') as f:
    #     varied_params = json.load(f)
    #
    # conductivity_df = pd.read_csv(conductivity_file)
    #
    # # pprint(measured_params)
    # # print('\n')
    # # pprint(varied_params)
    # # print(conductivity_df.columns)
    #
    # for key, value in varied_params.items():
    #     param = {key: value}
    #
    # conductance_plot(conductivity_data=conductivity_df,
    #                  title="Test",
    #                  varied_param=param,
    #                  figsize=(11, 8.5 + 0.25 * len(conductivity_df['sample'].unique()))
    #                  )
    # plt.show()

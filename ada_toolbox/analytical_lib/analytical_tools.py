"""
Data analysis tools.
"""
from scipy import stats, mean, std
import pandas as pd
import numpy as np
from typing import List, Dict, Tuple
from scipy import spatial, constants, mean
from scipy.stats import linregress, sem, t
from scipy.interpolate import griddata
from scipy.optimize import minimize
import matplotlib.tri as mtri
import lmfit
import pathlib
import imageio
import os
import logging

logger = logging.getLogger(__name__)


def expand_df_view():
    """
    Expands the view of dataframes in the console so that large outputs can be
    viewed.
    :return: None
    """

    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)


def voronoi_finite_polygons_2d(vor, radius=None):
    """
    Reconstruct infinite voronoi regions in a 2D diagram to finite
    regions.
    Parameters
    ----------
    vor : Voronoi
        Input diagram
    radius : float, optional
        Distance to 'points at infinity'.
    Returns
    -------
    regions : list of tuples
        Indices of vertices in each revised Voronoi regions.
    vertices : list of tuples
        Coordinates for revised Voronoi vertices. Same as coordinates
        of input vertices, with 'points at infinity' appended to the
        end.
    """

    if vor.points.shape[1] != 2:
        raise ValueError("Requires 2D input")

    new_regions = []
    new_vertices = vor.vertices.tolist()

    center = vor.points.mean(axis=0)
    if radius is None:
        radius = vor.points.ptp().max() * 2

    # Construct a map containing all ridges for a given point
    all_ridges = {}
    for (p1, p2), (v1, v2) in zip(vor.ridge_points, vor.ridge_vertices):
        all_ridges.setdefault(p1, []).append((p2, v1, v2))
        all_ridges.setdefault(p2, []).append((p1, v1, v2))

    # Reconstruct infinite regions
    for p1, region in enumerate(vor.point_region):
        vertices = vor.regions[region]

        if all(v >= 0 for v in vertices):
            # finite region
            new_regions.append(vertices)
            continue

        # reconstruct a non-finite region
        ridges = all_ridges[p1]
        new_region = [v for v in vertices if v >= 0]

        for p2, v1, v2 in ridges:
            if v2 < 0:
                v1, v2 = v2, v1
            if v1 >= 0:
                # finite ridge: already in the region
                continue

            # Compute the missing endpoint of an infinite ridge

            t = vor.points[p2] - vor.points[p1]  # tangent
            t /= np.linalg.norm(t)
            n = np.array([-t[1], t[0]])  # normal

            midpoint = vor.points[[p1, p2]].mean(axis=0)
            direction = np.sign(np.dot(midpoint - center, n)) * n
            far_point = vor.vertices[v2] + direction * radius

            new_region.append(len(new_vertices))
            new_vertices.append(far_point.tolist())

        # sort region counterclockwise
        vs = np.asarray([new_vertices[v] for v in new_region])
        c = vs.mean(axis=0)
        angles = np.arctan2(vs[:, 1] - c[1], vs[:, 0] - c[0])
        new_region = np.array(new_region)[np.argsort(angles)]

        # finish
        new_regions.append(new_region.tolist())

    return new_regions, np.asarray(new_vertices)


def filter_array_with_iqr(
        array,
        iqr_factor: float = 1.5,
) -> np.ndarray:
    """
    Filter a given array using 1.5 the IQR from Q1 and Q3.
    see https://www.youtube.com/watch?v=FRlTh5HQORA
    :param array: Array to filter
    :param iqr_factor: The multiple of IQR away from the outer Q values.
    :return: Filtered array
    """

    # If the array is 0 or 1, return the array without any filter.
    if len(array) == 0 or len(array) == 1:
        return array

    # Calculate the filtering bounds
    lower_bound, upper_bound = calculate_outlier_bounds_from_iqr(
        array=array,
        iqr_factor=iqr_factor,
    )

    # Filter the array
    f_array = array
    f_array = f_array[f_array > lower_bound]
    f_array = f_array[f_array < upper_bound]

    # If the array no longer has any data in it, return the original array
    if len(f_array) == 0:
        return array

    return f_array


def calculate_outlier_bounds_from_iqr(
        array,
        iqr_factor: float = 1.5,
) -> Tuple[float, float]:
    """
    Determines outlier bounds for a given array of values using 1.5 the
    interquartile range from below Q1, and above Q2.
    see https://www.youtube.com/watch?v=FRlTh5HQORA
    :param array: Array of which to calculate the bounds.
    :param iqr_factor: The multiple of IQR away from the outer Q values.
    :return: lower bound, upper bound.
    """

    # Calculate Q values
    q1, q2, q3 = calculate_q_values(array)

    # Calculate the interquartile range
    iqr = q3 - q1

    # Calculate bounds
    lower_bound = q1 - iqr_factor * iqr
    upper_bound = q3 + iqr_factor * iqr

    return lower_bound, upper_bound


def calculate_q_values(
        array
) -> Tuple[float, float, float]:
    """
    Calculate and return the Q1, Q2, and Q3 values for a given array. For more
    information on these values, see https://www.youtube.com/watch?v=FRlTh5HQORA
    :param array: The array of which to calculate the values
    :return: Q1, Q2, Q3
    """

    q1 = np.quantile(array, .25)
    q2 = np.quantile(array, .50)
    q3 = np.quantile(array, .75)

    return q1, q2, q3


def normalize_array(
        array,
) -> np.ndarray or None:
    """
    Rescales a numpy array from 0 to 1.
    :param array: Array to normalize.
    :return: Normalized array.
    """

    # Define the array bounds
    array_min = min(array)
    array_max = max(array)

    # If the array is completely flat:
    if array_min == array_max:

        # special case if its all 0
        if array_min == 0:
            return array

        # special case if it is flat
        else:
            return np.ones(len(array))

    return (array - min(array)) / (max(array) - min(array))


def normalize_dataframe(
        data: pd.DataFrame,
        return_bounds: bool = False,
):
    """
    Normalizes the values of a dataframe along the columns to be between 0 and
    1.
    :param data: The dataframe to be normalized.
    :param return_bounds: Should the min/max of the dataframe be returned to
    allow for rescaling of data.
    :return: Normalized dataframe.
    """

    # Get dataframe bounds
    data_min = data.min()
    data_max = data.max()

    # Scale
    data_scaled = (data - data_min) / (data_max - data_min)

    # return based on bounds bool
    if return_bounds:
        return data_scaled, data_min, data_max
    else:
        return data_scaled


def get_plotting_bounds(
        array,
        buffer: float = 0.05,
) -> Tuple[float, float]:
    """
    For a given array of data, generates what the proper min and max values of
    an axis should be to plot
    :param array: The array of which to calculate the plotting bounds
    :param buffer: The additional space around the data.
    :return: Tuple, min and max bounds
    """

    # Get the shape of the array
    array_min = min(array)
    array_max = max(array)
    array_range = array_max - array_min

    # If the array did not span any area, just use 1.
    if array_range == 0:
        array_range = 1

    # Calculate the plotting bounds
    plot_min = array_min - buffer * array_range
    plot_max = array_max + buffer * array_range

    return plot_min, plot_max


def generate_voroni_regions(
        data: pd.DataFrame,
        ax_0: str,
        ax_1: str,
):
    """
    Generates the points required to describe the Voroni polygons around the
    x, y data points passed.
    :param data: Pandas dataframe containing the x-y data.
    :param ax_0: Column name of the first axis of data.
    :param ax_1: Column name of the second axis of data.
    :return: Polygon points.
    """

    # Downselect the data
    voroni_data = data[[ax_0, ax_1]]

    # Normalize the passed data
    data_norm, data_min, data_max = normalize_dataframe(
        voroni_data,
        return_bounds=True
    )

    # Convert the two passed columns into a ndarray.
    voroni_array = data_norm[[ax_0, ax_1]].values

    # Compute the Voroni tesselation
    voroni_tesselation = spatial.Voronoi(voroni_array)

    # Convert into coordinates
    regions, vertices = voronoi_finite_polygons_2d(voroni_tesselation)

    # Rescale back into original data scale
    vertices_scaled_ax_0 = vertices.T[0] * \
                           (data_max[ax_0] - data_min[ax_0]) + data_min[ax_0]
    vertices_scaled_ax_1 = vertices.T[1] * \
                           (data_max[ax_1] - data_min[ax_1]) + data_min[ax_1]
    vertices_scaled = np.array([vertices_scaled_ax_0, vertices_scaled_ax_1]).T

    # Expand for each region
    region_bounds = [vertices_scaled[region] for region in regions]

    # Return
    return region_bounds


def running_max_of_array(
        array
):
    """
    Creates an array of the running maximum of the passed array. For example,
    the array [1, 2, 1] will become [1, 2, 2].
    :param array: Array to calculate the running max of.
    :return: Array of running max.
    """

    max_array = np.maximum.accumulate(array)

    return max_array


def sobol_sequence_expander(
        array
):
    """
    Every possible slice of a Sobol sequence is in itself a Sobol sequence. This
    function takes a passed sequence of values that have been sampled with
    Sobol, and returns a 2D array of every possible slice of that Sobol
    sequence.
    :param array: Sobol sequence to slice.
    :return: 2D array of Sobol sequences.
    """

    # Get the length of the array
    array_len = len(array)

    # Create an array in which to store shorter arrays
    slices = np.empty(shape=(0, array_len))

    # For all possible slices
    for size in range(array_len):
        # Get all the slices
        size_slices = sobol_sequence_slicer(
            array=array,
            slice_size=size,
        )

        # Fill the empty spaces with nan
        nans = np.zeros(shape=(size_slices.shape[0], array_len - size - 1)) + np.nan
        nan_filled = np.append(size_slices, nans, axis=1)

        # Store
        slices = np.append(slices, nan_filled, axis=0)

    return slices


def sobol_sequence_slicer(
        array,
        slice_size,
):
    """
    For a given array, return all slices of length slice_size for all positions
    in the array.
    :param array: Array to slice.
    :param slice_size: Size of slice.
    :return: 2D array of slices.
    """

    # Make an array to store the slices in
    result_array = np.empty(shape=(0, slice_size + 1))

    # Get the length of the array
    array_len = len(array)

    # Start position for slice
    start = 0

    while start + slice_size < array_len:
        # Get slice
        slice = array[start:start + slice_size + 1]

        # Store slice
        result_array = np.append(result_array, [slice], axis=0)

        # Increment the start position
        start += 1

    # Return the result
    return result_array


def wavelength_nm_to_energy_ev(wavelength_nm):
    """
    Converts wavelength (in nm) to energy (in eV).
    :param wavelength_nm: The object (either iterable or non-iterable) to
    be converted into energy (eV).
    :return:
    """

    # If the passed object is a list, convert it to a numpy array.
    if isinstance(wavelength_nm, list):
        wavelength_nm = np.array(wavelength_nm)

    # Convert the wavelength from nanometer to meters.
    wavelength_m = wavelength_nm * 1E-9

    # Convert to frequency (in Hz)
    frequency_hz = constants.physical_constants['speed of light in vacuum'][0] / wavelength_m

    # Convert to energy (in eV)
    energy_ev = constants.physical_constants['Planck constant in eV s'][0] * frequency_hz
    return energy_ev


def calc_plot_min_max_from_range(range, buffer=0.1):
    """
    helper function for plotting
    :param range: range of the data
    :param buffer: desired buffer for edges of plot
    :return: min and max values for plot
    """
    range_min = min(range)
    range_max = max(range)
    range_span = range_max - range_min
    plot_min = range_min - buffer * range_span
    plot_max = range_max + buffer * range_span

    return plot_min, plot_max


def calc_absorbance_from_t_and_r(t_data: pd.DataFrame, r_data: pd.DataFrame):
    """
    calculates the absorbance data based on transmission and reflection data
    :param t_data: t_data dataframe
    :param r_data: r_data dataframe
    :return: a_data: absorbance dataframe
    """
    a_data = pd.DataFrame()
    a_data['x_(nm)'] = t_data['x_(nm)']
    a_data['calibrated_signal'] = 1 - t_data['calibrated_signal'] - r_data['calibrated_signal']
    a_data['calibrated_signal_smoothed'] = 1 - t_data['calibrated_signal_smoothed'] - r_data[
        'calibrated_signal_smoothed']
    return a_data


def linear_regression(df, x_axis='x_axis', y_axis='y_axis'):
    slope, intercept, r_val, p_val, std_err = linregress(df[x_axis], df[y_axis])
    return {
        "slope": slope,
        "intercept": intercept,
        "r_val": r_val,
        "p_val": p_val,
        "std_err": std_err,
    }


def normal_distribution_confidence_interval(df: pd.DataFrame,
                                            data_col: str,
                                            groupby_col: str = None,
                                            confidence=0.95):
    """
    For a given dataframe, find the confidence interval for a normal distribution.
    :param df: Dataframe of data
    :param data_col: Column of the dataframe to subset.
    :param groupby_col: Another column to group by.
    :param confidence: Percent confidence interval assuming normal distribution.
    :return: low and high bound.
    """

    if groupby_col is None:

        data = df[data_col]
        n = len(data)
        m = mean(data)
        std_err = stats.sem(data)
        h = std_err * stats.t.ppf((1 + confidence) / 2, n - 1)
        low_bound = m - h
        high_bound = m + h

        return low_bound, high_bound

    else:
        df_group = df[[data_col, groupby_col]].groupby(groupby_col).agg(['mean', 'count', 'sem'])
        df_group['h'] = df_group[data_col]['sem'] * stats.t.ppf((1 + confidence) / 2, df_group[data_col]['count'] - 1)
        low_bound = df_group[data_col]['mean'] - df_group['h']
        high_bound = df_group[data_col]['mean'] + df_group['h']

        return low_bound, high_bound


def one_sided_cumulative_density_function(data: list,
                                          value: float):
    """
    Relative to a normal distribution, is the probability that 'value' will take a value less than or equal to the mean.
    :param data: Array of data to form distribution
    :param value: Value relative to the mean
    :return: probability
    """
    population_mean = mean(data)
    population_std = std(data)

    # Calculate the standard deviations the value is from the mean of the provided
    sample_std = abs(population_mean - value) / population_std  # Z-score
    probability = stats.norm.cdf(sample_std)

    return probability


def calculate_film_absorbance(R_0, T_0, R_1, T_1, x_nm):
    # TODO add docstring and symbol definitions and optics math reference

    R_g = (R_0 ** 2 - 2 * R_0 - T_0 ** 2 - 1 + np.sqrt(
        (R_0 ** 2 - 2 * R_0 - T_0 ** 2 - 1) ** 2 + 4 * R_0 * (R_0 - 2))) / \
          (2 * (R_0 - 2))

    T_g = 1 - R_g

    A_g = 1 - ((- (1 - R_g) ** 2 + np.sqrt((1 - R_g) ** 4 + 4 * T_0 ** 2 * R_g ** 2)) /
               (2 * T_0 * R_g ** 2))

    R_f = (- T_g ** 2 * R_1 ** 2 + 2 * T_g ** 2 * R_1 + T_g ** 2 - 2 * R_g * T_1 ** 2 + T_g * (R_1 - 1) * np.sqrt(
        T_g ** 2 * R_1 ** 2 - 2 * T_g ** 2 * R_1 + T_g ** 2 + 4 * R_g * T_1 ** 2)) / \
          (2 * (- T_g ** 2 * R_1 + 2 * T_g ** 2 - R_g * T_1 ** 2))

    T_f = 1 - R_f

    A_f = (- T_g * (1 - R_f) * (1 - A_g) + np.sqrt(
        (T_g * (1 - R_f) * (1 - A_g)) ** 2 + 4 * T_1 ** 2 * (1 - A_g) ** 2 * R_g * R_f)) / \
          (2 * T_1 * (1 - A_g) ** 2 * R_g * R_f)

    log_A_f = - np.log10(A_f)

    energy_ev = wavelength_nm_to_energy_ev(x_nm)

    df = pd.DataFrame(
        data={
            'x_(nm)': x_nm,
            'energy_(eV)': energy_ev,
            'A_f': A_f,
            'log_A_f': log_A_f,
            'A_g': A_g
        }
    )

    return df


def mols_from_volume(volume, conc_mg_ml, mw_g_mol):
    mass_mg = volume * conc_mg_ml
    mol = (mass_mg / 1000) / mw_g_mol
    return mol


def mols_from_mg(mass, conc_mg_ml, mw_g_mol, density_g_ml):
    mass_g = mass / 1000
    vol_ml = mass_g / density_g_ml
    mol = mols_from_volume(vol_ml, conc_mg_ml, mw_g_mol)
    return mol


def add_gaussian_region(
        name: str,
        lower_bound: float,
        upper_bound: float
):
    """
    Add a new region (in eV) to fit gaussians to in the UV-Vis spectra.
    :param name: Label for fitting region.
    :param lower_bound: Lower fit bound (eV)
    :param upper_bound: Upper fit bound (eV)
    :return: None
    """

    # Append the new gaussian bound
    return {
        'name': name,
        'bounds_ev': [upper_bound, lower_bound],
        'init_gaussians': []
    }


def pseudofret_bounds(df, lower_bound=350, upper_bound=600):
    # filter wavelengths
    ix = (df['x_(nm)'] >= lower_bound) & (df['x_(nm)'] <= upper_bound)
    return df.loc[ix, :].copy()


def pseudofret_normalise(df):
    df = pseudofret_bounds(df)
    # offset positive by the minimum
    df.loc[:, "log_A_f_mean"] = df["log_A_f_mean"]- df["log_A_f_mean"].min()

    # divide by integral
    integral = np.trapz(y=df['log_A_f_mean'], x=df["x_(nm)"])
    df.loc[:, "log_A_f_mean"] = (df["log_A_f_mean"] / integral).copy()

    return df.sort_values(by="x_(nm)").reset_index(drop=True)


def psuedofret_smoothing(df, window=20):
    _df = df.rolling(window, win_type='hann', center=True).mean()

    # remove nan values from smoothing
    _df = _df.dropna().sort_values(by="x_(nm)").reset_index(drop=True)

    return _df


def generate_pseudofret_baseline(files, baseline_type, output_filepath="", plot=False):
    """ creates a baseline given processed spectroscopy files """
    raw_norm_dfs = []
    dfs = []
    for i, f in enumerate(files):
        _df = pd.read_csv(f, index_col=False)
        # cut down dataframe
        _df = pseudofret_prep(_df)
        # filter high frequency components
        _filtered_df = psuedofret_smoothing(_df)
        # normalise to target wavelengths and by area
        _norm_df = pseudofret_normalise(_filtered_df)
        _raw_norm_df = pseudofret_normalise(_df)

        dfs.append(_norm_df)
        raw_norm_dfs.append(_raw_norm_df)
        if plot:
            from matplotlib import pyplot as plt
            plt.plot(_raw_norm_df["x_(nm)"], _raw_norm_df["log_A_f_mean"], label=f"raw_{i}")
            plt.plot(_norm_df["x_(nm)"], _norm_df["log_A_f_mean"], label=f"filtered_{i}")

    def combine_dfs(dataframes):
        df = pd.concat(dataframes)
        df = df.groupby(["x_(nm)"], as_index=False).mean()
        df = df.sort_values(by="x_(nm)").reset_index(drop=True)
        return df

    df = combine_dfs(dfs)

    if plot:
        from matplotlib import pyplot as plt
        plt.plot(df["x_(nm)"], df["log_A_f_mean"], label="combined")
        plt.legend(loc='lower right')
        plt.xlabel("x_(nm)")
        plt.ylabel("log_A_f_mean")
        plt.show()

    df.to_csv(os.path.join(output_filepath, f"baselines", f"{baseline_type}_baseline.csv"), index=False)


def pseudofret_prep(df):
    # keep only target wavelengths
    ix = (df['blank'] == False)
    _df = df.loc[ix, ["x_(nm)", "position", "log_A_f"]].copy()
    before_shape = _df.shape
    _df = _df.drop_duplicates().copy()
    if before_shape[0] != (2 * _df.shape[0]):
        raise ValueError(f"de-duplicated data should have half the rows, got: before {before_shape[0]} after {_df.shape[0]}_df.shape[0]")
    _df = _df.sort_values(by="x_(nm)").reset_index(drop=True)

    # take the mean of all positions
    _df = _df.groupby(["x_(nm)"], as_index=False)["x_(nm)", "log_A_f"].mean()
    _df.rename(columns=dict(log_A_f="log_A_f_mean"), inplace=True)

    return _df


def pseudofret(spectra_df, red_baseline=None, green_baseline=None, blue_baseline=None, plot=False):

    def col_norm(x):  # normalize data by maximum for fitting
        return (x - x.min()) / (x - x.min()).max()

    def linear_combination(x, r, g, b):
        return ((1 - x[0] - x[1]) * r) + (x[0] * g) + (x[1] * b)

    def min_linear_combination(x, f, r, g, b):
        comb = linear_combination(x, r, g, b)
        delta = (comb - f) ** 2
        return delta['log_A_f_mean'].sum()

    # read in the baselines
    baselines = {}
    for baseline, colour in zip([red_baseline, green_baseline, blue_baseline], ['red', 'green', 'blue']):
        if baseline is None:
            p = os.path.join(pathlib.Path(__file__).parent, "baselines", f"{colour}_baseline.csv")
            _df = pd.read_csv(p, index_col=False)
            baselines[colour] = _df

    # prepare the target spectra
    prep_df = pseudofret_prep(spectra_df)
    filt_df = psuedofret_smoothing(prep_df)
    norm_df = pseudofret_normalise(filt_df)

    # calculate integral on filtered and cut but not normalised
    int_df = pseudofret_bounds(filt_df)
    try:
        integral = np.trapz(y=int_df['log_A_f_mean'], x=int_df["x_(nm)"])
    except:
        logger.exception(f"PSEUDOFRET: failed to calculate integral, assigning 0")
        integral = 0

    # set low integrals to 0, likely blank slides
    if integral < 0:
        logger.info(f"PSEUDOFRET: integral < 0: {integral}")
        integral = 0

    # check lengths
    for _, b in baselines.items():
        if norm_df.shape != b.shape:
            raise ValueError(f"baseline shape must match input df shape: {norm_df.shape} != {b.shape}")

    try:
        # constrains
        cons = list()
        cons.append(dict(type='ineq', fun=lambda x: 1 - x[0] - x[1]))
        cons.append(dict(type='ineq', fun=lambda x: x[0]))
        cons.append(dict(type='ineq', fun=lambda x: x[1]))

        result = minimize(min_linear_combination,  # function
                 x0=np.array([0.5, 0.5]),  # initial guess
                 args=(norm_df, baselines['red'], baselines['green'], baselines['blue']),
                 method="SLSQP",
                 bounds=((0, 1), (0, 1)),
                 constraints=cons,
                 options={'maxiter': 500000, 'ftol': 1e-9}
                 )
        score = min_linear_combination(result.x, norm_df, baselines['red'], baselines['green'], baselines['blue'])
        logger.debug(f"PSEUDOFRET: minimise result: score={score} at {result.x} sum of x={sum(result.x)}")
    except:
        logger.exception(f"failed to minimise function, returning 0")
        return 0

    if plot:
        linear_comb = linear_combination(result.x, baselines['red']["log_A_f_mean"], baselines['green']["log_A_f_mean"], baselines['blue']["log_A_f_mean"])
        from matplotlib import pyplot as plt
        plt.plot(baselines['red']["x_(nm)"], linear_comb, label="min_linear_combination", linestyle='--', color='purple')
        for c in ['red', 'green', 'blue']:
            plt.plot(baselines[c]["x_(nm)"], (baselines[c]["log_A_f_mean"]), label=c, color=c)
        plt.plot(norm_df["x_(nm)"], (norm_df["log_A_f_mean"]), label="filtered", color='black')

        plt.legend(loc='upper right')
        plt.xlabel("x_(nm)")
        plt.ylabel("Normalised log_A_f_mean")
        plt.title(f"Combination of: score={round(score, 8)} - red={round(1 - result.x[0] - result.x[1], 3)} green={round(result.x[0], 3)} blue={round(result.x[1], 3)}")
        plt.show()

    final_score = integral * score
    logger.info(f"PSEUDOFRET: integral * interaction = final_score: {integral} * {score} = {final_score}")

    return final_score


def add_gaussian_to_region(
        gaussian_bounds_ev: list,
        region_name: str,
        a: float,
        b: float,
        c: float,
        b_min=-np.inf,
        b_max=np.inf
):
    """
    Add a gaussian to a UV-Vis gaussian fitting region.
    :param region_name: Label of fitting region to add gaussian to.
    :param a: Initial height of gaussian (Absorbance units)
    :param b: Initial position of gaussian (eV)
    :param c: Initial width of gaussian (eV)
    :return: None
    """

    # Add gaussian to region
    for region in gaussian_bounds_ev:
        if region['name'] == region_name:
            region['init_gaussians'].append(
                {
                    'a': a,
                    'b': b,
                    'c': c,
                    'b_min': b_min,
                    'b_max': b_max,
                }
            )
    return gaussian_bounds_ev


def get_fit_params(gaus_count, gaussian):
    fit_params = lmfit.Parameters()

    for param in ['a', 'b', 'c']:

        param_min = -np.inf
        param_max = np.inf

        # If amplitude, set min to 0
        if param == 'a':
            param_min = 0
            param_max = 5

        # If position, don't let the position be outside the region.
        if param == 'b':
            param_min = gaussian['b_min']
            param_max = gaussian['b_max']

        # If a width param, don't let it be negative
        if param == 'c':
            param_min = 0

        # Add the fit parameter
        fit_params.add(
            name=f'{param}_{gaus_count}',
            value=gaussian[param],
            min=param_min,
            max=param_max,
        )

    return fit_params


def gaussian(
        a,
        b,
        c,
        x
):
    """
    Calculate the value of a gaussian, for a given range.
    :param a: The amplitude of the gaussian (in absorbance).
    :param b: The position of the gaussian (in electron volts).
    :param c: The width of the gaussian (in electron volts).
    :param x: The range over which the gaussian should be calculated.
    :return: The value of the gaussian over the passed range.
    """

    # Convert the passed range to a numpy array.
    x_array = np.array(x)

    # Calculate the value of the gaussian.
    gaussian = a * np.exp(- ((x_array - b) ** 2) / (2 * c ** 2))

    # Return the value of the gaussian.
    return gaussian


def sum_of_gaussians(
        a_list,
        b_list,
        c_list,
        x
):
    """
    Calculate the sum of gaussians, over a given range, for a list of
    gaussian shape parameters.
    :param a_list: list of gaussian heights (in absorbance).
    :param b_list: list of gaussian positions (in electron volts).
    :param c_list: list of gaussian widths (in electron volts).
    :param x: Range over which to calculate the gaussian.
    :return: The value of the sum over the gaussians over the passed range.
    """

    # Create a list of zeros the length of the passed range.
    y = np.zeros(len(x))

    # For each gaussian
    for a, b, c in zip(a_list, b_list, c_list):
        # Calculate the value of the gaussian and add it to the y axis.
        y += gaussian(a, b, c, x)

    # Return the sum of all the gaussians.
    return y


def gaussian_cost(
        params,
        x=None,
        y=None,
        num_gaussians=1
):
    """
    For a given gaussian region, for a given guess for the gaussians,
    calculate the cost of the current guess.
    :param params: The lmfit parameters object containing the gaussian shape
    parameters.
    :param x: The gaussian region x values.
    :param y: The experimental values for the gaussian region.
    :return: The cost of the current guess over the gaussian region.
    """

    # Create empty lists to fill with the current gaussian fir parameters.
    a_list = []
    b_list = []
    c_list = []

    # For all the gaussians in the current gaussian region
    for gaus_count in range(num_gaussians):
        # Append the current values to the lists of gaussian parameters.
        a_list.append(params[f'a_{gaus_count}'].value)
        b_list.append(params[f'b_{gaus_count}'].value)
        c_list.append(params[f'c_{gaus_count}'].value)

    # Calculate the sum of gaussians over the gaussian region.
    gaussian_sum = sum_of_gaussians(a_list=a_list, b_list=b_list, c_list=c_list, x=x)

    # Calculate the cost
    cost = y - gaussian_sum

    # Return the cost of the current guess.
    return cost


def lmfitminimize(func, fitparams, kws):
    """
    performs a lmfit minimize with the given inputs
    :param func: cost function
    :param fitparams: parameters to fit
    :param kws: keyword args for cost function
    :return: dictionary of fitted params
    """

    fitting = lmfit.minimize(
        func,
        params=fitparams,
        kws=kws)

    return fitting.params.valuesdict()


def area_under_gaussian(
        a,
        c
):
    """
    Calculate the area under the gaussian from -inf to inf.
    :param a: The height of the gaussian (in absorbance).
    :param c: The width of the gaussian (in electron volts).
    :return:
    """

    # Calculate the area under the gaussian.
    area = a * c * np.sqrt(2 * np.pi)

    # Return the area under the gaussian.
    return area


def images_to_gif(
        path_to_frames: str,
        path_to_outout: str,
        gif_name: str = 'generate_gif.gif',
        framerate: int = 10,
) -> None:
    """
    Generate a gif image from a series of still images.
    :param path_to_frames: Path to folder with frames to compile.
    :param path_to_outout: Path to location for gif output.
    :param gif_name: Name to give gif.
    :param framerate: Framerate to use for gif.
    :return:
    """

    # Get an ordered list of the frame names
    files = sorted(os.listdir(path_to_frames))

    # Create a holder for the gif frames
    image = []

    # For each file
    for file in files:

        # If the file is an image
        if file.endswith('.png'):
            # Add to growing gif file
            image.append(imageio.imread(path_to_frames + f'/{file}'))

    # Calculate the time for each frame
    frame_dwell = 1 / framerate

    # Save the gif to the specified directory
    imageio.mimsave(
        path_to_outout + f'/{gif_name}',
        image,
        duration=frame_dwell,
    )


def interpoloate_3d_data(
        data: pd.DataFrame,
        x0: str,
        x1: str,
        z: str,
        interp_mode: str = 'linear',
        density: int = 1000,
        buffer: float = 0.1,
):
    """
    For a given 2D dataset, interpolate the data for contour plotting.
    :param data: A dataframe containing the x0, x1, and z data.
    :param x0: Dataframe col name of x0 data.
    :param x1: Dataframe col name of x1 data.
    :param z: Dataframe col name of z data.
    :param interp_mode: The interpolation mode to be used. 'cubic' and 'linear'
    are supported.
    :param buffer: The additional spacing around the data to perform the
    interpolation.
    :return: Tuple of x0, x1, and z interpolated.
    """

    # Select the data from the dataframe
    x0_data = data[x0]
    x1_data = data[x1]
    z_data = data[z]

    # Get the bounds
    x0_min, x0_max = get_plotting_bounds(x0_data, buffer=buffer)
    x1_min, x1_max = get_plotting_bounds(x1_data, buffer=buffer)

    # Create the grid values for the interpolation sampling
    x0_i = np.linspace(x0_min, x0_max, density)
    x1_i = np.linspace(x1_min, x1_max, density)

    # Perform the interpolation
    z_i = griddata(
        points=(x0_data, x1_data),  # TODO turn this into an array?
        values=z_data,
        xi=(x0_i[None, :], x1_i[:, None]),
        method=interp_mode
    )

    # Return the data
    return (x0_i, x1_i, z_i)


def form_triangulation_object(
        data: pd.DataFrame,
        x0: str,
        x1: str,
):
    """
    Using the matplotlib plotting tools, form a triangulation object
    :param data: Dataframe with data.
    :param x0: x0 data positions.
    :param x1: x1 data positions.
    :return: triangulation object
    """

    # Select the data
    x0_data = data[x0]
    x1_data = data[x1]

    # Form the triangulation object
    triangles = mtri.Triangulation(
        x=x0_data,
        y=x1_data,
    )

    # Return the object
    return triangles


def interpolate_triangulation(
        data: pd.DataFrame,
        x0: str,
        x1: str,
        z: str,
        interp_mode: str = 'linear',
        density: int = 1000,
        buffer: float = 0.1,
):
    """
    For a given 2D dataset, interpolate the data for contour plotting using
    matplotlibs triangulation module.
    :param data: A dataframe containing the x0, x1, and z data.
    :param x0: Dataframe col name of x0 data.
    :param x1: Dataframe col name of x1 data.
    :param z: Dataframe col name of z data.
    :param interp_mode: The interpolation mode to be used.
    :param buffer: The additional spacing around the data to perform the
    interpolation.
    :return: Tuple of x0, x1, and z interpolated.
    """

    # Get the triangulation
    triangles = form_triangulation_object(
        data=data,
        x0=x0,
        x1=x1,
    )

    # Form the interpolation function
    if interp_mode == 'linear':
        interp_func = mtri.LinearTriInterpolator(
            triangles,
            data[z]
        )
    else:
        interp_func = mtri.CubicTriInterpolator(
            triangles,
            data[z],
            kind=interp_mode
        )

    # Get the bounds on the data including the buffer
    x0_min, x0_max = get_plotting_bounds(data[x0], buffer=buffer)
    x1_min, x1_max = get_plotting_bounds(data[x1], buffer=buffer)

    # Make the sampling array
    x0i, x1i = np.meshgrid(
        np.linspace(x0_min, x0_max, density),
        np.linspace(x1_min, x1_max, density),
    )

    # Sample the interpolation function
    zi = interp_func(x0i, x1i)

    # Return
    return x0i, x1i, zi


def optimizer_modelling(
        emulator,
        optimizer,
):
    pass


if __name__ == "__main__":
    df = pd.DataFrame(np.random.randint(0, 100, size=(100, 4)), columns=list('ABCD'))
    df['E'] = np.random.randint(0, 4, size=len(df))

    print(df.head())

    normal_distribution_confidence_interval(df=df, data_col="A", groupby_col='E')

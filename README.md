# Toolbox

A collection of generic data manipulation and plotting tools for scientists. Please do not alter or add to the structure of this repository.

Please note that this repository is shared under the CC BY-NC-SA 4.0 license (share-alike, non-commercial use), and is available to the public.

## Guidelines
The aim of this repository is to contain data analysis and plotting functions for generic use. It's important that this repository is usable by those who are not familiar.
 
import math
from scipy.spatial.distance import euclidean
from itertools import cycle, islice
import plotly.graph_objs as go
from plotly import subplots
import plotly_express as px
from plotly.offline import plot
from plotly.subplots import make_subplots
import colorlover as cl

from sklearn.linear_model import RANSACRegressor
import numpy as np
import pandas as pd
import copy
from PIL import Image


def comparison_plot(
        plot_df,
        group_by_col,
        x_axis_column,
        title=None
):

    plot_df = copy.copy(plot_df)
    plot_df = plot_df.sort_values(by=[x_axis_column])

    cols = list(plot_df.columns)
    cols.remove(group_by_col)
    cols.remove(x_axis_column)

    groups = plot_df[group_by_col].unique()

    fig = subplots.make_subplots(rows=len(cols), cols=1, print_grid=False)

    traces = []

    for col in cols:

        group_traces = {}
        group_traces[col] = {}

        for group in groups:
            group_df = plot_df[plot_df[group_by_col] == group]

            trace = go.Scatter(mode='lines+markers',
                               name=f"{col} - {group}",
                               x=group_df[x_axis_column],
                               y=group_df[col],
                               hoverinfo='all',
                               marker=dict(
                                size=16
                                   )
                               )
            group_traces[col][group] = trace

        traces.append(group_traces)

    i = 1

    for group_trace in traces:

        for param, group_dict in group_trace.items():

            for key, val in group_dict.items():

                fig.append_trace(val, i, 1)
                fig['layout'][f"yaxis{i}"].update(title=param)

        i += 1
    fig['layout'][f'xaxis{len(cols)}'].update(title=x_axis_column)
    # fig['layout']['showlegend'] = False
    fig['layout']['title'] = go.layout.Title(text=title)

    fig.update_layout(
        template='plotly_white',
        height=650 * len(cols),
    )

    return fig


def progress_plot(
        plot_df,
        y_axis_columns,
        x_axis_column,
        labels: dict = {},
        title=None
):
    """
    creates a scatter plot with a shared x-axis (typically sample_number), and split y axis
    :param plot_df: the dataframe to plot
    :param y_axis_columns: column names the parameters
    :param x_axis_column: column name of the x_axis
    :param labels: dictionary of column names to axis labels / trace labels
    :param title: plot title
    :return:
    """
    if len(labels) == 0:
        for col in y_axis_columns:
            labels[col] = col
        labels[x_axis_column] = x_axis_column

    plot_df = copy.copy(plot_df)
    trace_list = []

    for col in y_axis_columns:
        trace = go.Scatter(mode='lines+markers', name=labels[col], x=plot_df[x_axis_column], y=plot_df[col],
                           hoverinfo='all',
                           marker=dict(
                               size=16
                           ))
        trace_list.append(trace)

    fig = subplots.make_subplots(rows=len(y_axis_columns), cols=1, print_grid=False)
    i = 1

    for trace in trace_list:
        fig.append_trace(trace, i, 1)
        fig['layout'][f"yaxis{i}"].update(title=labels[y_axis_columns[i - 1]])
        i += 1
    fig['layout'][f'xaxis{len(y_axis_columns)}'].update(title=labels[x_axis_column])
    fig['layout']['showlegend'] = False
    fig['layout']['title'] = go.layout.Title(text=title)

    fig.update_layout(
        template='plotly_white',
        height=250 * len(y_axis_columns),
    )

    return fig


def sample_dispense_accuracy_plot(
        plot_df,
        x_axis_column,
        y_axis_column,
        colour_column,
        reference_column,
        group_col,
        labels: dict = {},
        title="Sample Dispense Accuracy "
):
    """
    Creates a per sample area plot of dispense data
    :param plot_df: the dataframe to plot
    :param x_axis_column: label of the x axis column
    :param y_axis_column: label of the y axis column
    :param colour_column: label of the colour (will be stacked)
    :param reference_column: label of the total reference value
    :param group_col: label of the column to iterate over and make subplots
    :param labels:
    :param title:
    :return:
    """

    if len(labels) == 0:
        for col in plot_df[colour_column].unique():
            labels[col] = col
        labels[x_axis_column] = x_axis_column

    fig = go.Figure()

    # generate colours
    cs = cl.scales['11']['div']['Spectral']
    cs = cs[7:]
    if len(plot_df[colour_column].unique()) > 4:
        cs = cl.interp(cs, len(plot_df[colour_column].unique()))

    num_subplots = len(plot_df[group_col].unique())
    subplot_titles = []
    for name, _ in plot_df.groupby(group_col):
        subplot_titles.append(name)

    fig = make_subplots(rows=num_subplots, cols=1, subplot_titles=subplot_titles, vertical_spacing=0.05, shared_xaxes=False)

    for i, (name, df) in enumerate(plot_df.groupby(group_col)):
        row = i + 1
        temp_fig = px.area(df, x=x_axis_column, y=y_axis_column, color=colour_column, template='plotly_white', color_discrete_sequence=cs,)
        # extract the plotly express
        for j, t in enumerate(temp_fig.data):
            t['showlegend'] = (row == 1)
            t['line']['width'] = 0
            fig.add_trace(t, row=row, col=1)

        # add the total requested
        fig.add_trace(go.Scatter(
            x=df.loc[df[colour_column] == 1, x_axis_column],
            y=df.loc[df[colour_column] == 1, reference_column],
            mode='markers',
            marker=dict(
                color='Black',
                symbol="line-ew",
                size=20,
                line=dict(width=2, color='Black',),
            ),
            showlegend=(row == 1),
            name='Requested', ),
            row=row, col=1,
            )

        # add total dispensed
        total_dispensed = df.groupby(x_axis_column)[y_axis_column].sum()
        fig.add_trace(go.Scatter(
            x=total_dispensed.index.values,
            y=total_dispensed.values,
            mode='markers',
            marker=dict(
                color='darkgrey',
                opacity=0.01,
            ),
            showlegend=(row == 1),
            name='Dispensed', ),
            row=row, col=1,
        )

    fig.update_layout(hovermode='x', )

    # set axis parameters
    fig.update_xaxes(title_text="Sample (#)", row=num_subplots, col=1)  # last only
    fig.update_yaxes(title_text="Dispensed (mL)")
    fig.update_layout(template='plotly_white', title=title)

    return fig


def dispense_accuracy_plot(
    plot_df,
    key_column,
    x_axis_column,
    y_axis_column,
    labels: dict = {},
    title = None
    ):
    """
    Creates a dashed y = x line, scatter for each x vs 'key' filtered y, and a RANSAC best fit line
    :param plot_df: the dataframe to plot
    :param key_column: data to filter over
    :param x_axis_column: column name of the x_axis
    :param y_axis_column: column name
    :param labels: dictionary of column names to axis labels / trace labels
    :param title: plot title
    :return:
    """
    if len(labels) == 0:
        for col in plot_df[key_column].unique():
            labels[col] = col
        labels[x_axis_column] = x_axis_column

    fig = go.Figure()

    # get max x value for plot scaling
    max_x = max(plot_df[x_axis_column])
    max_x = max_x * 1.05  # pad 5 %

    # plot y = x
    fig.add_trace(go.Scatter(x=[0, max_x], y=[0, max_x], mode='lines', line=dict(color='Black', width=0.5), name='Requested', ))

    # colours
    cs = cl.scales['11']['div']['Spectral']
    len_keys = max([len(plot_df[key_column].unique()), 11])
    if len_keys > 11:
        cs = cl.interp(cs, len_keys)
    # reorder to start at a certain spot in the colour scale
    cs = cs[(len_keys // 2) + (len_keys // 6):] + cs[:(len_keys // 2) + (len_keys // 6)]

    for i, (ix, group) in enumerate(plot_df.groupby(key_column)):
        x = group.loc[:, x_axis_column].values
        y = group.loc[:, y_axis_column].values

        # generate model
        model = RANSACRegressor()
        model.fit(x.reshape(-1, 1), y.reshape(-1, 1))
        sample = np.linspace(0, max_x, 1000)
        line = model.predict(sample.reshape(-1, 1))
        line = np.ndarray.flatten(line)

        # add scatter points
        fig.add_trace(go.Scatter(x=x, y=y, mode='markers',
                                 marker=dict(color=cs[i], opacity=1, size=6, line=dict(width=0.5,color='DarkSlateGrey',),),
                                 name=f'Dispensed: {ix}', legendgroup=ix,))

        # add best fit line
        fig.add_trace(go.Scatter(x=sample, y=line, mode='lines',
                                 line=dict(color=cs[i],),
                                 name=f'Model: {ix}', legendgroup=ix,))

        # add calibrated line
        fig.add_trace(go.Scatter(x=line, y=sample, mode='lines',
                                 line=dict(color=cs[i], dash='dash'),
                                 name=f'Calibrated: {ix}', legendgroup=ix,))

    # set axis parameters
    fig.update_xaxes(title_text="Requested (uL)", range=[0, max_x], rangemode="nonnegative")
    fig.update_yaxes(title_text="Dispensed (mL)", range=[0, max_x], rangemode="nonnegative")
    fig.update_layout(template='plotly_white', title=title)

    return fig


def sample_dispense_columns(
        plot_df,
        plot_obj,
        x_axis_column,
        y_axis_column,
        colour_column,
        labels: dict = {},
        title="Sample Dispense Amounts "
):
    """
    Creates a per sample area plot of dispense data
    :param plot_df: the dataframe to plot
    :param x_axis_column: label of the x axis column
    :param y_axis_column: label of the y axis column
    :param colour_column: label of the colour (will be stacked)
    :param labels:
    :param title:
    :return:
    """

    samples = []
    costs = {}

    for sample_num in plot_df[x_axis_column].unique():
        for chemical in plot_df[colour_column].unique():
            sample = {
                'sample': sample_num,
                'chemical': chemical,
                'target_volume': 0,
                'dispensed': 0,
            }
            samples.append(sample)

    for index, row in plot_df.iterrows():
        sample_num = row[x_axis_column]
        chemical = row[colour_column]
        target_volume = row['target_volume']
        dispensed = row['dispensed']
        sample = next(item for item in samples if item[x_axis_column] == sample_num and item[colour_column] == chemical)
        sample['target_volume'] += target_volume
        sample['dispensed'] += dispensed

    if plot_obj is not None:
        for obj in plot_obj['cost_objective'].unique():
            costs[obj] = ':.5f'

        for index, row in plot_obj.iterrows():
            sample_num = row[x_axis_column]
            cost_name = row['cost_objective']
            cost_value = row['cost_value']
            for sample in samples:
                if sample[x_axis_column] == sample_num:
                    sample[cost_name] = cost_value

    reduced_df = pd.DataFrame(samples)

    if len(labels) == 0:
        for col in reduced_df[colour_column].unique():
            labels[col] = col
        labels[x_axis_column] = x_axis_column

    # generate colours
    cs = cl.scales['11']['div']['Spectral']
    cs = cs[7:]
    if len(reduced_df[colour_column].unique()) > 4:
        cs = cl.interp(cs, len(reduced_df[colour_column].unique()))

    fig = px.bar(reduced_df, x=x_axis_column, y=y_axis_column, hover_data=costs, color=colour_column, color_discrete_sequence=cs)

    # set axis parameters
    fig.update_xaxes(title_text="Sample (#)")  # last only
    fig.update_yaxes(title_text="Dispensed (mL)")
    fig.update_layout(template='plotly_white', title=title)

    return fig


def optimisation_progress_by_parameter_plot(
        plot_df,
        x_axis_col='sample',
        y_axis_col='parameter',
        requested_col='requested',
        realised_col='realised',
        relative_error_col='rel_error',
        absolute_error_col='abs_error',
        labels: dict = {},
        title="Optimization Progress by Parameter"
):
    """
    Creates an optimisation plot by sample

    :param plot_df:
    :param x_axis_col:
    :param y_axis_col:
    :param requested_col:
    :param realised_col:
    :param relative_error_col:
    :param absolute_error_col:
    :param labels:
    :param title:
    :return:
    """

    # get the x params
    params = plot_df[y_axis_col].unique().tolist()

    # measure are all other columns
    measures = []
    column_filters = [x_axis_col, y_axis_col, requested_col, realised_col, relative_error_col, absolute_error_col]
    for p in params:
        column_filters.extend([s for s in plot_df.columns if p in s])
    measures = [col for col in plot_df.columns if col not in column_filters]

    # make 2 * number of parameter subplots + measurement subplots
    num_subplots = len(params) * 2 + len(measures)

    # plot titles
    plot_titles = params.copy()
    for i, p in enumerate(params):
        plot_titles.insert(1 + i * 2, f"{labels.get(p, p)} error")

    plot_titles = measures.copy() + plot_titles.copy()

    # specs for shared y
    specs = []
    for i in range(0, len(params) * 2):
        if i % 2 != 0:
            specs.insert(i, [{"secondary_y": True}])
        else:
            specs.insert(i, [{}])
    specs = [[{}] for i in range(len(measures))] + specs

    fig = make_subplots(
        rows=num_subplots, cols=1,
        vertical_spacing=0.3 / num_subplots,
        specs=specs,
        subplot_titles=plot_titles,
    )

    # colours
    cs = cl.interp(cl.scales['11']['div']['Spectral'], 13)
    req_colour = cs[0]
    real_colour = cs[-4]
    measure_colours = cl.interp(cl.scales['11']['div']['Spectral'][9:], len(measures) + 1)

    # plot measurements
    for i, measure in enumerate(measures):
        _df = plot_df.groupby([x_axis_col, measure], as_index=False).first()

        # measure
        fig.add_trace(
            go.Scatter(x=_df[x_axis_col], y=_df[measure].values,
                       mode='lines+markers', marker=dict(symbol="circle", color=measure_colours[i], size=5,
                                                         line=dict(width=0.5, color='darkslategrey', ), ),
                       name=measure, showlegend=(i == 0)),
            row=i + 1, col=1, )

    row_offset = len(measures)

    for i, param in enumerate(params):
        _df = plot_df[plot_df[y_axis_col] == param].copy()
        # requested
        fig.add_trace(go.Scatter(x=_df[x_axis_col], y=_df[requested_col].values,
                                 mode='lines+markers', marker=dict(symbol="line-ew", color=req_colour, size=20,
                                                                   line=dict(width=4, color=req_colour, ), ),
                                 name='Requested', showlegend=(i == 0)),
                      row=row_offset + 1 + (i * 2), col=1,

                      )

        # requested
        fig.add_trace(go.Scatter(x=_df[x_axis_col], y=_df[realised_col].values,
                                 mode='lines+markers', marker=dict(symbol="line-ew", color=real_colour, size=20,
                                                                   line=dict(width=4, color=real_colour, ), ),
                                 name='Realized', showlegend=(i == 0)),
                      row=row_offset + 1 + (i * 2), col=1
                      )

        # relative error
        fig.add_trace(go.Scatter(x=_df[x_axis_col], y=abs(_df[relative_error_col].values * 100),
                                 mode='lines', line=dict(dash="longdash", color='DarkGrey', width=1),
                                 name='Relative Error', showlegend=(i == 0)),
                      row=row_offset + 2 + (i * 2), col=1, secondary_y=True,
                      )

        # absolute error
        fig.add_trace(go.Scatter(x=_df[x_axis_col], y=_df[absolute_error_col].values,
                                 mode='lines', line=dict(dash="solid", color='Black', width=1),
                                 name='Absolute Error', showlegend=(i == 0)),
                      row=row_offset + 2 + (i * 2), col=1, secondary_y=False,
                      )

        # axes titles
        fig.update_yaxes(title_text="Relative Error (%)", row=row_offset + 2 + (i * 2), col=1, secondary_y=True)
        fig.update_yaxes(title_text="Absolute Error", row=row_offset + 2 + (i * 2), col=1, secondary_y=False)

    fig.update_layout(hovermode='x', )

    # set axis parameters
    fig.update_xaxes(title_text="Sample (#)", row=num_subplots, col=1)  # last only
    fig.update_layout(template='plotly_white', title=title)
    fig.update_layout(height=num_subplots * 400)

    return fig


def optimisation_parameter_distance_plot(
        plot_df,
        index_col='sample',
        pivot_col='parameter',
        value_col='realised',
        colour_col='measurement_mean',
        title="Optimisation Distances",
):
    """
    :param plot_df:
    :param index_col:
    :param pivot_col:
    :param value_col:
    :param colour_col:
    :param title:
    :return:
    """

    # get parameter columns
    cols = plot_df[pivot_col].unique().tolist()

    # pivot the dataframe
    _pdf = plot_df.pivot(index=index_col, columns=pivot_col, values=value_col).reset_index(index_col)
    _pdf = _pdf.merge(plot_df[[index_col, colour_col]].drop_duplicates(), on=[index_col], how='inner')
    _pdf = _pdf.sort_values(by=index_col)

    def calc_dist_from_max(df):
        c_max = -1
        df['dist_from_max'] = 0
        for i, (ix, row) in enumerate(df.iterrows()):
            meas = row[colour_col]
            if i == 0:
                c_max = meas
                c_max_points = row[cols].values
                continue
            # calculate distance from current max
            dist = abs(euclidean(row[cols].values, c_max_points))
            df.loc[ix, 'dist_from_max'] = dist
            # update current max if greater
            if meas > c_max:
                c_max = meas
                c_max_points = row[cols].values
        return df.copy()

    _pdf = calc_dist_from_max(_pdf)

    # setup subplots and titles
    plot_titles = ['Euclidean Distance from Max Measurement', 'Distance Distribution']
    num_subplots = 2
    fig = make_subplots(
        rows=num_subplots, cols=1,
        vertical_spacing=0.3 / num_subplots,
        subplot_titles=plot_titles,
    )

    # do 5 sets of lambda values
    all_traces = []
    buttons = []
    for lamb in range(1, 6):
        vals = list(range(1, lamb + 1))
        _pdf['lambda_set'] = list(islice(cycle(vals), _pdf.shape[0]))

        # colours
        c1 = cl.scales[str(len(vals) + 2)]['qual']['Set2'][1:]
        c2 = cl.scales[str(len(vals) + 2)]['qual']['Set2'][1:]

        for i, (lambda_set, _df) in enumerate(_pdf.groupby('lambda_set')):
            _df = _df.copy()
            vis = False
            if lamb == 1:
                vis = True
            # Distance from max plot
            name = f"{lamb} Lambda Value(s), set: {lambda_set}"
            fig.add_trace(go.Scatter(x=_df[index_col].values, y=_df['dist_from_max'].values,
                                     mode='markers+lines',
                                     marker=dict(symbol="circle-dot", color=c1[i], size=8,
                                                 line=dict(width=1, color=c1[i], ), ),
                                     name=name, legendgroup=name, visible=vis), row=1, col=1)
            fig.update_xaxes(title_text="Sample (#)", row=1, col=1)
            fig.update_yaxes(title_text="Distance (units)", row=1, col=1)

            # Probability Histogram
            _df['rel_distance_from_max'] = 100 * (_df.dist_from_max / math.sqrt(len(cols)))

            h = go.Histogram(x=_df['rel_distance_from_max'],
                             xbins=dict(start=0, end=100.0, size=2.5),
                             marker=dict(color=c2[i]),
                             autobinx=False,
                             autobiny=False,
                             name=name,
                             legendgroup=name,
                             visible=vis,
                             histnorm='probability'
                             )
            fig.add_trace(h, row=2, col=1)
        # for the buttons
        d = dict(label=f"{lamb}", method="update",
                 args=[{"visible": all_traces + [True, ] * 2 * len(vals)}, {"title": f"{lamb} Lambda Value(s)"}])
        for b in buttons:
            b['args'][0]['visible'].extend([False, ] * 2 * len(vals))
        buttons.append(d)
        all_traces.extend([False, ] * 2 * len(vals))

        # Button
    fig.update_layout(
        updatemenus=[
            go.layout.Updatemenu(
                type="buttons",
                showactive=True,
                direction="right",
                active=0,
                x=0.57,
                y=1.2,
                buttons=buttons
            )
        ])
    fig.update_layout(
        annotations=[
            go.layout.Annotation(text="Lambda values:", x=0.35, y=1.15, showarrow=False),
        ])

    # for histograms
    fig.update_layout(barmode='overlay')
    # Reduce opacity to see both histograms
    fig.update_traces(opacity=0.5)

    fig.update_xaxes(title_text="Relative Distance (%)", row=2, col=1)
    fig.update_yaxes(title_text="Probability", row=2, col=1)

    # set axis parameters
    fig.update_layout(template='plotly_white', title=title)
    fig.update_layout(height=num_subplots * 400)
    fig.update_layout(showlegend=True, hovermode='x')

    return fig


def optimisation_parameter_corner_plot(
        plot_df,
        index_col='sample',
        pivot_col='parameter',
        value_col='realised',
        colour_col='measurement_mean',
        title="Corner Plot: Optimisation Parameters",
        labels = None,
):
    """
    Pivots the parameter dataframe to make corner plot

    :param plot_df:
    :param index_col:
    :param pivot_col:
    :param value_col:
    :param colour_col:
    :param title:
    :return:
    """

    # pivot the dataframe
    _pdf = plot_df.pivot(index=index_col, columns=pivot_col, values=value_col).reset_index(index_col)
    _pdf = _pdf.merge(plot_df[[index_col, colour_col]].drop_duplicates(), on=[index_col], how='inner')

    # corner plot
    fig = px.scatter_matrix(_pdf, color=colour_col, height=900, color_continuous_scale=px.colors.sequential.Viridis, labels=labels)
    fig.update_traces()

    # plot formatting
    fig.update_layout(
        template='plotly_white',
        dragmode='select',
        height=950,
        hovermode='closest',
        title=title)

    # layout formatting
    fig.update_traces(
        marker=dict(size=10, line_color='white', line_width=0.5),
        showupperhalf=False,
        diagonal_visible=False,
    )

    return fig


def optimisation_parameter_parallel_coordinates_plot(
        plot_df,
        index_col='sample',
        pivot_col='parameter',
        value_col='realised',
        colour_col='measurement_mean',
        title="Parallel Coordinates",
        labels=None,
):
    """
    Pivots the parameter dataframe to make corner plot

    :param plot_df:
    :param index_col:
    :param pivot_col:
    :param value_col:
    :param colour_col:
    :param title:
    :return:
    """

    # pivot the dataframe
    _pdf = plot_df.pivot(index=index_col, columns=pivot_col, values=value_col).reset_index(index_col)
    _pdf = _pdf.merge(plot_df[[index_col, colour_col]].drop_duplicates(), on=[index_col], how='inner')

    # corner plot
    fig = px.parallel_coordinates(_pdf, color=colour_col, height=900, color_continuous_scale=px.colors.sequential.Plasma, labels=labels)
    fig.update_traces()

    # plot formatting
    fig.update_layout(
        template='plotly_white',
        dragmode='select',
        height=950,
        hovermode='closest',
        title=title)

    return fig


def corner_plot(data_frame,
                dimensions,
                color,
                title,
                labels,
                hover_data,
                color_continuous_scale=px.colors.sequential.Viridis):
    fig = px.scatter_matrix(data_frame=data_frame,
                            dimensions=dimensions,
                            color=color,
                            color_continuous_scale=color_continuous_scale,
                            title=title,
                            labels=labels,
                            hover_data=hover_data)

    fig.update_layout(
        template='plotly_white',
        autosize=True,
        height=200 * len(dimensions),
    )

    fig.update_xaxes(automargin=True)

    return fig


def parallel_coordinates(data_frame,
                         dimensions,
                         color,
                         labels,
                         title,
                         color_continuous_scale=px.colors.sequential.Viridis):

    fig = px.parallel_coordinates(data_frame=data_frame,
                                  dimensions=dimensions,
                                  color=color,
                                  color_continuous_scale=color_continuous_scale,
                                  labels=labels,
                                  title=title)

    fig.update_yaxes(automargin=True)

    fig.update_layout(
        template='plotly_white',
        autosize=False,
        width=250 * len(dimensions),
    )

    return fig


def plot_image(image_path,
               campaign=None,
               sample=None,
               **defects):
    image = Image.open(image_path)
    x, y = image._size

    fig = go.Figure()

    fig.update_layout(
        title_text=f"Image: {campaign}, {sample}",
        images=[go.layout.Image(
            x=0,
            sizex=x,
            y=y,
            sizey=y,
            xref="x",
            yref="y",
            opacity=1.0,
            layer="below",
            sizing="stretch",
            source=image)]
    )

    # Configure axes
    fig.update_xaxes(
        visible=False,
        range=[0, x]
    )

    fig.update_yaxes(
        visible=False,
        range=[0, y],
        scaleanchor="x"
    )

    i = 0
    annotations = []
    try:
        for key, value in defects.items():
            i = i + 1
            annotation = go.layout.Annotation(
                showarrow=False,
                x=0,
                y=(y + (-80*i)),
                text=f"{key}: {value}",
                xanchor="left",
                xshift=10,
                font=dict(
                    family="Courier New, monospace",
                    size=20,
                    color="red"
                )
            )
            annotations.append(annotation)

    except:
        pass

    fig.update_layout(
        showlegend=False,
        annotations=annotations
    )

    return fig


def plot_sensor_data(df, sample_num=None, title=None, subsample=False):

    if subsample:
        ix = [True, False, False, False]*int(np.ceil(len(df['sample'])/4))
        while len(ix) < len(df['sample']):
            ix.append(False)
        while len(ix) > len(df['sample']):
            del ix[-1]
        df = df[ix]

    sensors = pd.unique(df['component'])
    sensor_num = len(sensors)

    fig = make_subplots(sensor_num, 1)

    if sample_num is not None:
        df = df[df['sample'] == sample_num]
        time_key = 'time_stamp_rel'
        marker_size = None
        color = None
        mode = 'lines+markers'
    else:
        time_key = 'time_stamp_abs'
        marker_size = 2
        color = df['sample']
        mode = 'markers'

    for enum, sensor in enumerate(sensors):
        df_sensor = df[df['component'] == sensor]
        df_sensor.index = range(len(df_sensor.index))
        name = ''
        for word in sensor.split("_"):
            name += str.capitalize(word) + ' '

        hovertext = []

        for num, action in enumerate(df_sensor['action']):
            text = f"Action: {action}<br>Time: {df_sensor['date_time'][num][11:]}<br>Sample: {df_sensor['sample'][num]}"
            hovertext.append(text)

        fig.append_trace(go.Scattergl(name=name,
                                      showlegend=False,
                                      x=df_sensor[time_key],
                                      y=df_sensor['sensor_reading'],
                                      mode=mode,
                                      marker=dict(
                                          size=marker_size,
                                          showscale=False
                                      ),
                                      hovertext=hovertext), row=enum+1, col=1)

        fig.update_xaxes(title_text='Time (s)', row=enum+1, col=1)
        fig.update_xaxes(range=[0, np.max(df[time_key])], row=enum+1, col=1)

        if sensor == 'annealing_oven':
            name = f'Temperature (°C)'

        fig.update_yaxes(title_text=name, row=enum+1, col=1)
        fig.update_yaxes(range=[0, np.max(df_sensor['sensor_reading'])+20], row=enum + 1, col=1)

    fig.update_layout(template='plotly_white', title=title, height=300*sensor_num)

    return fig


def _generate_random_data():
    varied_columns = ['A', 'B', 'C', 'D']

    varied_labels = {
        'A': 'Cobalt',
        'B': 'Acid',
        'C': 'Zinc',
        'D': 'Annealing'
    }

    varied_units = {
        'A': 'n/n',
        'B': 'mM',
        'C': 'mg',
        'D': 's'
    }
    data = pd.DataFrame(
        np.random.rand(500, 4) * 15,
        columns=varied_columns
    )

    data['measured_param'] = data['A'] * 2 + data['C'] * 2

    return data, varied_columns, varied_labels, varied_units


if __name__ == "__main__":

    df = '/Users/alexproskurin/PycharmProjects/toolbox/sample_data/sensor_data.csv'

    df = pd.read_csv(df)

    fig = plot_sensor_data(df, title='lol', subsample=True)

    fig.show()

    print('done')
    # data, varied_columns, varied_labels, varied_units = _generate_random_data()
    # image_path = '/Users/teddyhaley/Desktop/2019-08-16_15-19-19/sample_000/spincoated_slide_after_annealing1.jpg'
    #
    # print(data)
    # print(varied_columns[0:3])
    # print(varied_columns[3])
    #
    # fig1 = progress_plot(data, varied_columns[0:3], varied_columns[3])
    # plot(fig1)
    #
    # fig2 = parallel_coordinates(data_frame=data,
    #                             dimensions=varied_columns,
    #                             color='measured_param',
    #                             labels=varied_labels,
    #                             title="Test")
    # plot(fig2)
    #
    # fig3 = corner_plot(data_frame=data,
    #                    dimensions=varied_columns,
    #                    color='measured_param',
    #                    labels=varied_labels,
    #                    hover_data=['measured_param'],
    #                    title="Test")
    # plot(fig3)
    #
    # # plot_image(image_path, campaign="campaign 1", sample="sample 1", crack=1, dewetting=2)

from setuptools import find_packages, setup

NAME = 'ada_toolbox'
AUTHOR = ''
URL = 'https://gitlab.com/ada-chem/toolbox'
CLASSIFIERS = [
    "Programming Language :: Python :: 3",
    "Operating System :: OS Independent",
]
INSTALL_REQUIRES = [
    'plotly',
    'matplotlib',
    'numpy',
    'pandas',
    'colorlover',
    'sklearn',
    'typing',
    'imageio',
    'lmfit',
    'scipy',
    'bayesian-optimization',
    'coloredlogs',
    'pillow',
    'opencv-python',
    'slackclient',
    'certifi',
    'requests',
]

EXCLUDE_PACKAGES = [
]

# with open('LICENSE.md') as f:
#     LICENSE = f.read()
# TODO: license file is not cross platform, need to fix encoding
LICENSE = ""

about = {}
with open('ada_toolbox/__about__.py') as fp:
    exec(fp.read(), about)

with open("README.md", "r") as fh:
    DESCRIPTION = fh.read()

# find packages and prefix them with the main package name
PACKAGES = find_packages(exclude=EXCLUDE_PACKAGES)

setup(
    name=NAME,
    version=about["__version__"],
    description=DESCRIPTION,
    author=AUTHOR,
    install_requires=INSTALL_REQUIRES,
    packages=PACKAGES,
    url=URL,
    license=LICENSE,
    classifiers=CLASSIFIERS,
    package_data={
        'ada_toolbox': ['analytical_lib/baselines/*.csv']
    },
    include_package_data=True

)

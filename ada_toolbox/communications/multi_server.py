import socket
import os
# from _thread import *
import threading

ServerSocket = socket.socket()
host = '127.0.0.1'
port = 1235
ThreadCount = 0
try:
    ServerSocket.bind((host, port))
except socket.error as e:
    print(str(e))

print('Waiting for a Connection..')
ServerSocket.listen(5)


def threaded_client(connection):
    connection.send(str.encode('Welcome to the Server'))
    while True:
        data = connection.recv(2048)
        reply = 'Server Says: ' + data.decode('utf-8')
        if not data:
            break
        connection.sendall(str.encode(reply))
    connection.close()

thread_list = []
while True:
    try:
        Client, address = ServerSocket.accept()
        print('Connected to: ' + address[0] + ':' + str(address[1]))
        _t = threading.Thread(target=threaded_client, args=(Client,))
        thread_list.append(_t)
        _t.start()
        print('Thread Number: ' + str(len(thread_list)))
    except Exception as e:
        print(e)
        ServerSocket.close()
        break
ServerSocket.close()

from ada_toolbox.data_analysis_lib.validation_thresholds import number_valid_observations_lm_threshold, \
    r2_score_threshold, lm_slope_threshold
from ada_toolbox.data_analysis_lib import conductivity_analysis as cond
import logging
import pandas as pd

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

data_good = {
    'Current': [0, 1, 2, 3],
    'Voltage': [0, 1, 2, 3]
}

df_good = pd.DataFrame(data_good, columns=data_good.keys())

data_bad_current = {
    'Current': [0, 1, 2, 1001],
    'Voltage': [0, 1, 2, 3]
}

df_bad_current = pd.DataFrame(data_bad_current, columns=data_bad_current.keys())

data_bad_voltage = {
    'Current': [0, 1, 2, 3],
    'Voltage': [0, 1, 2, 1001]
}

df_bad_voltage = pd.DataFrame(data_bad_voltage, columns=data_bad_voltage.keys())

data_bad_number_observations = {
    'Current': [0, 1],
    'Voltage': [0, 1001]
}

df_bad_number_observations = pd.DataFrame(data_bad_number_observations, columns=data_bad_number_observations.keys())

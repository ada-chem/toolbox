
Feature request template.
```
- Make sure the title has good glance value.
```
## Requested by: \<NAME HERE PLEASE\>

## Problem/Current behaviour
```
The problem that is not fully addressed by the current behaviour of the system.
e.g. The robot currently does not make tea or coffee.
```

## Desired feature
```
The feature that will address the problem.
e.g.
	The robot should be able to make tea.
	The robot will prompt the user whether they would like tea.
	Additionally, the robot will ask what type of tea they would like.
```

### Related information/context
```
Any other information that you'd like to share that may provide more insight to the feature.
e.g. Maybe also consider making coffee?
```

## Feature impact
```
How will this make your life better?
What does this feature enable you to do that you could not before?
e.g.
	The robot will be able to prepare experiment-grade standardized cups of tea.
```

## Feature scope
```
What parts of the system does this feature affect?
Which group of users does this feature affect?
```
### Affected system components
- System part A
- System part B
- System part C

### Affected groups
- Group $\Phi$
- Group $\Psi$
- Group $\Omega$

## Possible Solution (optional)
```
- Suggest some ideas for the feature.
- If you do suggest a possible solution, please provide a detailed description of the change or addition you are proposing.
e.g.
	The robot will prompt a user if they would like tea, and if so, what kind of tea.
	Upon receiving an answer, the robot will then use its arm to:
		1. Place a pre-loaded tea-bag into an empty cup.
		2. Fill the cup with hot water.
		3. Hand the cup to the user.
		4. Return to its previous position before the tea-prompt.
```

## Possible Implementation (optional)
```
- Reference a commit or pseudocode here if you have a potential solution.
```

import socket
import pickle
import time
import sys
import logging
from contextlib import contextmanager

logger = logging.getLogger(__name__)


class TCPIPClient:
    def __init__(self, address, packet_length_kb=16, header_length=30, timeout=5):
        # have to set atttributes by __dict__ because of overridden __setattr__
        self.__dict__["address"] = address
        # self.address = address
        self.__dict__["sock"] = None
        # self.sock = None
        self.__dict__["timeout"] = timeout
        # self.timeout = timeout
        self.__dict__["packet_length"] = packet_length_kb * 1024
        # self.packet_length = packet_length_kb * 1024
        self.__dict__["logger"] = logger.getChild(self.__class__.__name__)
        # self.logger = logger.getChild(self.__class__.__name__)
        self.__dict__["header_length"] = header_length
        # self.header_length = header_length
        self.__dict__["mute"] = False
        # self.mute = False

    def __getattr__(self, item):
        return self.__getattribute__(item)

    @contextmanager
    def open_connection(self):
        self.__dict__["sock"] = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # default protocals
        self.sock.settimeout(self.timeout)
        self.sock.connect(self.address)
        yield
        self.sock.close()

    def send_message_await_reply(self, message, silent=True):
        if not self.mute:
            self.logger.info(f'connection to server at {self.address}')
        if self.sock is None:
            raise Exception('Socket not created')

        message = bytes(message, 'utf-8')
        self.sock.sendall(message)

        data = b""
        header = self.sock.recv(self.header_length)
        data_length = int(pickle.loads(header))

        start_time = time.time()
        while True:
            packet = self.sock.recv(self.packet_length)
            data += packet
            if not silent or data_length > 100000:
                done = int(50 * len(data) / data_length)
                elapsed_time = time.time() - start_time
                if data_length < 1024:
                    display_length = data_length
                    display_data = len(data)
                    display_units = 'bytes'
                else:
                    display_length = data_length / 1024
                    display_data = len(data) / 1024
                    display_units = 'kilobytes'
                if not self.mute:
                    if elapsed_time != 0:
                        sys.stdout.write("\rdownload progress [%s%s] %.0f out of %.0f %s (%.0f kb /s)" % ('=' * done, ' ' * (50 - done), display_data, display_length, display_units, (self.packet_length / 1024) / elapsed_time))
                    else:
                        sys.stdout.write("\rdownload progress [%s%s] %.0f out of %.0f %s (? kb /s)" % ('=' * done, ' ' * (50 - done), display_data, display_length, display_units))
                    sys.stdout.flush()
                start_time = time.time()
            if not packet:
                break
        result = pickle.loads(data)

        return result


class TCPIPServer:

    def __init__(self, address, packet_length_kb=16):
        self.address = address
        self.sock = None
        self.packet_length = packet_length_kb * 1024
        self.reply = b''
        self.buffer = b''
        self.logger = logger.getChild(self.__class__.__name__)

        self.logger.info(f'creating server at {self.address}')
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # default protocols
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(self.address)
        # Listen for incoming connections, use 5 here so that we can listen to multiple clients simultaneously.
        self.sock.listen(5)

    @contextmanager
    def get_message_return_reply(self):

        connection, client_address = self.sock.accept()
        self.logger.info(f'accepting connection from {client_address}')
        self.buffer = b''
        while True:
            data = connection.recv(self.packet_length)
            self.buffer += data
            if len(data) < self.packet_length:
                break
        yield
        ret_header = pickle.dumps(str(len(self.reply)).zfill(20))
        connection.sendall(ret_header + self.reply)
        self.sock.close()
        self.reply = b''
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # default protocols
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(self.address)
        # Listen for incoming connections
        self.sock.listen(1)

    def get_message(self, connection, client_address):
        self.logger.info(f'accepting connection from {client_address}')
        buffer = b''
        while True:
            data = connection.recv(self.packet_length)
            buffer += data
            if len(data) < self.packet_length:
                break
        return buffer

    def return_reply(self, connection, reply):
        ret_header = pickle.dumps(str(len(reply)).zfill(20))
        connection.sendall(ret_header + reply)
        connection.close()


if __name__ == "__main__":
    test = TCPIPClient(address='home')

    with test.open_connection():
        test.test()

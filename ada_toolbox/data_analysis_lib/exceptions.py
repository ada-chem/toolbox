"""
This file contains custom exceptions that we can later act upon
"""


class DataOutOfBounds(Exception):
    pass

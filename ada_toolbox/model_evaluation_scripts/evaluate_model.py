from ada_toolbox.data_analysis_lib.validation_tools import _blur_detection
import pandas as pd
import os
import csv

desktop = os.path.join(os.path.join(os.path.expanduser('~')), 'Desktop')
ratings = pd.read_csv(os.path.join(desktop, "results.csv"))
images_directory = os.path.join(desktop, 'images')
files = os.listdir(images_directory)
csv_files = [file for file in ratings['file']]

ratings['sobel_max'] = 0
ratings['avg_sobel'] = 0
ratings['delta_sobel'] = 0

ratings.set_index("file")

for file in files:
    if file in csv_files:
        print(file)
        sobel_max, avg_sobel, delta_sobel = _blur_detection(os.path.join(images_directory, file))

        blur = ratings.loc[ratings['file'] == file]['blurry'].values[0]
        print(blur)

        print(ratings.loc[file])

        # row = [file, blur, sobel_max, avg_sobel, delta_sobel]
        # header = ['file', 'blur', 'sobel_max', 'avg_sobel', 'delta_sobel']
        #
        # if not os.path.isfile(os.path.join(desktop, "modified_results.csv")):
        #     with open(os.path.join(desktop, "modified_results.csv"), 'a+') as csvFile:
        #         writer = csv.writer(csvFile)
        #         writer.writerow(header)
        #
        # with open(os.path.join(desktop, "modified_results.csv"), 'a+') as csvFile:
        #     writer = csv.writer(csvFile)
        #     writer.writerow(row)

import sys
# Imaging validation thresholds
blur_threshold = 0.005
brightness_threshold = 0.3

# Conductivity validation thresholds
number_valid_observations_lm_threshold = 4
min_current_pA = 50
r2_score_threshold = 0.97
outlier_threshold_sd = 2
lm_slope_threshold = 0
max_voltage = 10
min_voltage = -10
min_current = -20
max_current = 20


# Spectrometry thresholds
# dark baseline
bound_lo_dark = 0
bound_up_dark = 0.1 * 2 ** 16
tol_lo_dark = 0
tol_up_dark = 0

# bright baseline
bound_lo_bright = 0
bound_up_bright = 2 ** 16
tol_lo_bright = 0
tol_up_bright = 0

# sample signal
tolerance = 0

# calibrated transmission and reflection
bound_lo_calibrated = 0
bound_up_calibrated = 1
ignore_range_t_r = [[0, 250], [500, 600]]
tolerance_cal_t_r_lo = 0
tolerance_cal_t_r_up = 0

# calculated absorbance
ignore_range_absorp = [[0, 250], [500, 600]]
bound_absorp_lo = 0
bound_absorp_up = sys.maxsize
tolerance_cal_t_r_lo = 0.1


"""
A series of examples on how to use the matplotlib_tools.py
"""

from ada_toolbox.matplotlib_lib import matplotlib_tools as pt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


def _generate_random_data(
        length: int = 500,
):
    varied_columns = ['A', 'B', 'C', 'D']

    labels = {
        'A': 'Cobalt',
        'B': 'Acid',
        'C': 'Zinc',
        'D': 'Annealing',
        'measured_param': 'Measured Param',
        'sample': 'Sample Number'
    }

    units = {
        'A': 'n/n',
        'B': 'mM',
        'C': 'mg',
        'D': 's',
        'measured_param': 'units',
        'sample': 'count'
    }
    data = pd.DataFrame(
        np.random.rand(length, 4) * 15,
        columns=varied_columns
    )

    data['measured_param'] = data['A'] * 2 + data['C'] * 2
    data['sample'] = np.arange(length)

    return data, varied_columns, labels, units


def values_to_color_list_example():
    # Generate random values
    values = np.random.rand(10) * 3

    # Get color map
    color_map = pt.values_to_color_list(
        values=values,
        color_map_name='viridis'
    )

    print(color_map)

    print('OK')


def create_corner_plot_example():
    data, columns, labels, units = _generate_random_data()

    pt.create_corner_plot(
        data=data,
        varied_col_names=columns,
        measured_name='measured_param',
        labels=labels,
        units=units,
    )

    plt.show()

    print('OK')


def create_parallel_coordinate_plot_example():
    data, columns, labels, units = _generate_random_data()

    pt.create_parallel_coordinate_plot(
        data=data,
        varied_col_names=columns,
        measured_name='measured_param',
        labels=labels,
        units=units,
    )

    plt.show()

    print('OK')


def create_parameter_progress_plot_example():
    data, columns, labels, units = _generate_random_data()

    pt.create_parameter_progress_plot(
        data=data,
        varied_col_names=columns,
        measured_name='measured_param',
        x_axis_column='sample',

        labels=labels,
        units=units,
    )

    plt.show()

    print('OK')


def create_voroni_plot_example():
    data, columns, labels, units = _generate_random_data(500)

    pt.create_voroni_plot(
        data=data,
        ax_0='A',
        ax_1='B',
        measured_name='measured_param',
        labels=labels,
        units=units,
        poly_line_args={
            'edgecolor': 'white',
            'linewidth': 1,
            'alpha': 0.3,
        },
        scatter_args={
            'color': 'white',
            's': 7,
        }
    )

    plt.show()

    print('OK')


def create_styled_line():
    figure = plt.figure()
    ax = figure.add_subplot(1, 1, 1)

    x = np.arange(10)
    y = x ** 2

    pt.plot_styled_line(
        ax=ax,
        x=x,
        y=y
    )

    plt.show()

    print('OK')


def add_color_bar_to_axis_example():
    figure = plt.figure()
    ax = figure.add_subplot(1, 15, 1)

    pt.add_colorbar_to_axis(
        ax,
    )

    plt.show()

    print('OK')

def create_contour_plot_example():

    data, columns, labels, units = _generate_random_data(50)
    figure = plt.figure()
    ax = figure.add_subplot(1, 1, 1)

    pt.create_contour_plot(
        data=data,
        ax_x0='A',
        ax_x1='B',
        ax_z='measured_param',
        ax=ax,
    )
    plt.show()

if __name__ == "__main__":
    # values_to_color_list_example()
    # create_corner_plot_example()
    create_parameter_progress_plot_example()
    # create_parallel_coordinate_plot_example()
    # create_voroni_plot_example()
    # create_styled_line()
    # add_color_bar_to_axis_example()
    # create_contour_plot_example()

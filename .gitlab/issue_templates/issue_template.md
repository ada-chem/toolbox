Issue reporting template.
```
- Make sure the title has good glance value.
```

## Expected Behaviour
```
- What should happen
```

## Current Behaviour
```
- What actually happens instead
```

## Possible Solution (optional)
```
- Suggest a fix/reason for the bug. If not sure, leave this empty.
- If you do suggest a possible solution, please provide a detailed description of the change or addition you are proposing.
```

## Steps to Reproduce
```
- Provide an unambiguous set of steps to reproduce this behaviour.
- Include any custom code, if relevant and not already in repo.
```
1.
2.
3.
4.

## Context (Environment)
```
- Some information to help us diagnose the issue.
- What were you trying to accomplish?
```

## Possible Implementation (optional)
```
- Reference a commit here, if you have a potential solution.
```
